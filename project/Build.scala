import sbt._

object ApplicationBuild extends Build {

  val appName         = "ztm-rt-monitor"
  val appVersion      = "1.0-SNAPSHOT"
  val slick           = "com.typesafe.slick" %% "slick" % "1.0.0"
  val posgresDriver   = "postgresql" % "postgresql" % "8.4-701.jdbc4"

  val appDependencies = Seq(
    slick,
    posgresDriver
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here

  )

}
