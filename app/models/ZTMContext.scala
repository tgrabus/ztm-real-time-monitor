package models

import repository._
import scala.slick.driver.PostgresDriver.simple._
import models.schema._
import play.api.Play.current
import schema.Line
import schema.LinePoints
import schema.LineStops
import schema.Point
import schema.Schedule
import schema.Stop
import services._
import org.joda.time.DateTime
import collection.mutable
import java.sql.{Time, Timestamp}
import java.util.Date
import java.io.FileWriter
import akka.event.slf4j.Logger
import play.api.libs.json.Json
import scala.collection.mutable._
import models.schema.Point
import models.schema.ActualDeparture
import models.schema.LinePoints
import models.schema.Transport
import models.schema.VehiclePosition
import models.schema.Schedule
import models.schema.Line
import models.schema.Stop
import models.schema.LineStops

/**
 * User: Tomasz
 * Date: 30.05.13
 */

class ZTMContext {
  private val config = play.api.Play.configuration

  import scala.slick.session.{Database, Session}

  val database = Database.forURL(
    url = config.getString("db.postgres.url").get,
    driver = config.getString("db.postgres.driver").get,
    user = config.getString("db.postgres.user").get,
    password = config.getString("db.postgres.password").get)



  def CreateTables = {
    database withSession { implicit session:Session =>
      (Schedules.ddl ++
        Departures.ddl ++
        Lines.ddl ++
        Points.ddl ++
        Stops.ddl ++
        LinesPoints.ddl ++
        LinesStops.ddl ++
        Transports.ddl ++
        VehiclePositions.ddl ++
        ActualDepartures.ddl
        ).create
    }
  }
  def DropTables = {
    database withSession { implicit session:Session =>
      (Schedules.ddl ++
        Departures.ddl ++
        Lines.ddl ++
        Points.ddl ++
        Stops.ddl ++
        LinesPoints.ddl ++
        LinesStops.ddl ++
        Transports.ddl ++
        VehiclePositions.ddl ++
        ActualDepartures.ddl)drop
    }
  }
  def InsertData = {
    DropTables
    CreateTables
    val lines = LinesParser.getLines
    val stops =  services.StopsParser.parseStops
    var missingMap:HashMap[String, String] = HashMap[String, String]()
    var incompleteLines:HashMap[String, String] = HashMap[String, String]()

    database withSession { implicit session:Session =>
      
      val stopRepo = new StopRepository
      val pointRepo = new PointRepository
      val lineRepo = new LineRepository

      val lineStopRepo = new LineStopRepository
      val scheduleRepo = new ScheduleRepository
      val departureRepo = new DepartureRepository


      for(s <- stops) {
    	  val newPoint = new Point(pointRepo.generateId, s.position.latitude.toFloat, s.position.longtitude.toFloat)
    	  val newStop = new Stop(s.id, s.name, newPoint.id)
    	  pointRepo.insert(newPoint)
    	  stopRepo.insert(newStop)
      }

      for(line <- lines) {
        val newline = new Line(lineRepo.generateId, line.name, line.direction, LineInserter.getLineType(line.name))
        lineRepo.insert(newline)

        for(stop <- line.stops) {
          if(stop.id > 0) {	          
	          try {
	            val stopFromDB = stopRepo.getById(stop.id)
	          } catch {
	            case nse : NoSuchElementException => {
	              try {
	            	  val simlar:Stop = stopRepo.getSimilarStopByName(stop.name)
	            	  stopRepo.insert(new Stop(stop.id, stop.name, simlar.pointId))
	              } catch {
	                case n : NoSuchElementException => {
	                  val txt = missingMap.getOrElse(stop.id + ";" + stop.name, "")
		              if("" == txt) {
		                missingMap(stop.id.toString + ";" + stop.name) = (line.name + " " + line.direction + ", ")
		              } else {
		                missingMap(stop.id.toString + ";" + stop.name) = (txt + line.name + " " + line.direction + ", ")
		              }
		              incompleteLines(line.name + ";" + line.direction) = ""
	                }
	              }
	            }
	          }
	          val time = java.sql.Timestamp.valueOf("2013-01-01 00:00:00")
            val x = lineStopRepo.generateId
	          val newlineStop = new LineStops(x, stop.id, newline.id, false)

	          try {
	        	  lineStopRepo.insert(newlineStop)
              for(sche <- stop.schedule)    {
                val scheduleId = scheduleRepo.generateId
                val scheduleType = sche.typeNameArg;
                var typeInt = 0;

                if(scheduleType == "DniPowszednie")
                  typeInt = 1;
                if(scheduleType == "Soboty")
                  typeInt = 2;
                if(scheduleType == "NiedzieleSwieta")
                  typeInt = 3;


                val newschedule = new Schedule(scheduleId, time, time, "", x, typeInt)
                scheduleRepo.insert(newschedule)

                var hour = 0;
                for(dep <- sche.schedule) {
                         for(minute <- dep) {
                           var min = minute.substring(0,2)
                           var len = minute.length;
                           var info = minute.substring(2, len)
                           var dt = new DateTime(2013, 11, 1, hour, min.toInt);
                           var dep = new Departure(departureRepo.generateId, new java.sql.Timestamp(dt.getMillis), info, scheduleId)
                           departureRepo.insert(dep)
                         }
                  hour = hour + 1;
                }
              }

	          } catch {
	          	case pse : Exception => {
	          	  println("Cannot insert line: " + newlineStop.lineId + " stop: " + newlineStop.stopId)
	          	}
	          }
	        }
	      }
      }
    }
    writeMissingStops(missingMap)
    writeIncompleteLines(incompleteLines)
  }
  
  /*def InsertActualDepartures = {

    database withSession { implicit session:Session =>
      ActualDepartures.ddl.drop
      ActualDepartures.ddl.create
      val stopRepo = new StopRepository
      val lineRepo = new LineRepository
      val repo = new ActualDepartureRepository
      val stops = stopRepo.getAll

      for(stop <- stops)    {
        val current = ActualScheduleParser.parseObjects(stop.id.toString);
        for(a <- current){
          var s = a.time.toString();
          var time = java.sql.Time.valueOf("00:00:00");
          var arriving = 0;
          try {
            if(s.contains("za")) {
              s = s.replace("za ","");
              s = s.replace(" min","");
              arriving = Integer.parseInt(s);
              s = "00:" + s + ":00";
              time = java.sql.Time.valueOf(s);
            } else {
                if(a.time!=null) time = java.sql.Time.valueOf(a.time + ":00");
                arriving = -1;
            }
          } catch {
            case ex : IllegalArgumentException => {
              print("Nieprawidłowy czas " + a.time + "\n")
            }
          }
          try {
          val line = lineRepo.getByNameAndDirection(a.line, a.direction);
          val ad = new ActualDeparture(repo.generateId, stop.id, line.id, time, arriving, a.line, a.direction);
          repo.insert(ad)

          } catch {
            case ex : NoSuchElementException => {
              print("[" + stop.name + " " + stop.id + "] Nie znaleziono lini: " + a.line + " " + a.direction + "\n")
              val ad = new ActualDeparture(repo.generateId, stop.id, 1, time, arriving, a.line, a.direction);
              repo.insert(ad)
            }
          }
        }
    }
    }
  }*/
  def getPositionOfStop(stopName : String) : models.schema.Point =
  {
    database withSession { implicit session:Session =>
      val stopRepository = new StopRepository
      val pointRepo = new PointRepository
      //find by ZTMNumer, not only name
      val stop = stopRepository.getStopByName(stopName).get
      if(stop!=null) {
        val pointId = stop.pointId
        val point = pointRepo.getById(pointId)
        point
      }
      null
    }
  }




  def insertVehiclePositions(lines: List[Line], vehicles:List[models.parser.ActualVehiclePosition],
                             pointsRepo: PointRepository, vehiclesRepo: VehiclePositionsRepository,
                             transportRepo: TransportRepostory) {
    val lineIds = prepareMap(lines)
    database withSession { implicit session:Session =>
      for(vehicle <- vehicles) {
        if (lineIds.contains(vehicle.lineId)) {
          val pointId = pointsRepo.generateId
          pointsRepo.insert(new Point(pointId, vehicle.point.lat.toFloat, vehicle.point.long.toFloat))
          val vehicleId = vehiclesRepo.generateId
          var time = vehicle.time.split(" ")
          time = time(1).split(":")
          var dt = new DateTime(2013, 11, 1, time(0).toInt, time(1).toInt, time(2).toInt);

          val transport = transportRepo.getById(vehicle.vehicleId.toInt).getOrElse(None)
          if(None.equals(transport)) {
            transportRepo.insert(new Transport(vehicle.vehicleId.toInt, "", "", 0,0))
          }
          vehiclesRepo.insert(new VehiclePosition(vehicleId, new java.sql.Timestamp(dt.getMillis), lineIds(vehicle.lineId), vehicle.vehicleId.toInt, pointId))
        }
      }
    }
  }

  def prepareMap(lines: List[Line]) = {
    val map = new mutable.HashMap[String, Int]()
    for(line <- lines) {
      val key = line.name + ";" + line.direction
      map(key) = line.id
    }
    map
  }

  def searchRoute(from : String, destination : String, date : Int): (List[Line], List[Timestamp]) = {
    //znajdz przystanek from
    //znajdz przystanek to
    //pobierz linie jezdzace miedzy tymi przystankami
    //pobierz godziny przyjazdow

    database withSession { implicit session:Session =>

      val stopRepo = new StopRepository
      val startStop = stopRepo.getStopByName(from).get
      val endStop = stopRepo.getStopByName(destination).get

      if(startStop != null && endStop != null) {
        val startlines = for {
          ls <- startStop.lineStops
          l <- ls.line
        } yield l

        val endlines = for {
          ls <- endStop.lineStops
          l <- ls.line
        } yield l

        val lines = for {
          sl <- startlines
          el <- endlines
          if sl.id === el.id
        } yield sl



        var closests  : List[Timestamp] = Nil

       // for (s <- schedules) {
       //   var ts = new Timestamp(date)
       //   val closest = s.departures.filter(_.departureTime >= ts).list.map(_.departureTime).mapConserve(t => new Date(t.getTime)).min
         // ts = new Timestamp(closest.getTime)
        //  closests = closests.::(ts)
     //   }

        return (lines.list, closests)

      }
      return null
    }
  }
  
  private def writeMissingStops(stops:HashMap[String, String]) {
    val missingStopsFile = "missing.csv"
    try {
      new java.io.File(missingStopsFile).delete()
    } catch {
      case ex:Exception => print(ex)
    }
    val fw = new FileWriter(missingStopsFile, true)
    for((key, value) <- stops) {
      fw.write(key + ";" + value + "\n")
    }
    fw.close()
  }

  private def writeIncompleteLines(lines:HashMap[String, String]) {
    val missingStopsFile = "incomplete.csv"
    try {
      new java.io.File(missingStopsFile).delete()
    } catch {
      case ex:Exception => print(ex)
    }
    val fw = new FileWriter(missingStopsFile, true)
    for((key, value) <- lines) {
      fw.write(key + "\n")
    }
    fw.close()
  }

  def writeSchemaCreationScriptToFile() {
    val createFileName = "create.sql"
    try {
      new java.io.File(createFileName).delete()
    } catch {
      case ex:Exception => print(ex)
    }
    val fw = new FileWriter(createFileName, true)
    Lines.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    Points.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    LinesPoints.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    Stops.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    Schedules.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    Departures.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    LinesStops.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    Transports.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    VehiclePositions.ddl.createStatements.foreach(p => fw.write(p + ";\n"))
    fw.close()
  }


  def writeInsertsToFIle = {
    val insertFile = "ins.sql"
    try {
      new java.io.File(insertFile).delete()
    } catch {
      case ex:Exception => print(ex)
    }



    var inserts = new mutable.StringBuilder("")

    database withSession { implicit session:Session =>
      val stopRepo = new StopRepository
      val pointRepo = new PointRepository
      val lineRepo = new LineRepository
      val linePointRepo = new LinePointRepository
      val lineStopRepo = new LineStopRepository
      val scheduleRepo = new ScheduleRepository
      val departureRepo = new DepartureRepository
      val transportRepo = new TransportRepostory
      val vehiclePositionRepo = new VehiclePositionsRepository

      for(line <- lineRepo.getAll.toList) {
        inserts.append("insert into \"public\".\"" + Lines.tableName + "\" values (" + line.id + ", \'" + line.name + "\', \'" + line.direction + "\', " + line.lineKind + ");\n")
      }

      for(point <- pointRepo.getAll.toList) {
        inserts.append("insert into \"public\".\"" + Points.tableName + "\" values (" + point.id + ", " + point.latitude + ", " + point.longitude + ");\n")
      }

      for(lp <- linePointRepo.getAll.toList) {
        inserts.append("insert into \"public\".\"" + LinesPoints.tableName + "\" values (" + lp.pointId + ", " + lp.lineId + ", false);\n")
      }

      for(stop <- stopRepo.getAll.toList){
        inserts.append("insert into \"public\".\"" + Stops.tableName + "\" values (" + stop.id + ", \'" + stop.name + "\', " + stop.pointId + ");\n")
      }

      for(schedule <- scheduleRepo.getAll.toList) {
        inserts append "insert into \"public\".\"" + Schedules.tableName + "\" values (" + schedule.id + ", \'" + schedule.activeFrom + "\', \'" + schedule.activeTo + "\');\n"
      }

      for(ls <- lineStopRepo.getAll.toList) {
        inserts.append("insert into \"public\".\"" + LinesStops.tableName + "\" values (" + ls.stopId + ", " +  ls.lineId + ", " + ls.onDemand + ");\n")
      }

      for(transport <- transportRepo.getAll.toList) {
        inserts.append("insert into \"public\".\"" + Transports.tableName + "\" (" + transport.id + ", \'\', \'\', 0,0);\n")
      }

      for(vp <- vehiclePositionRepo.getAll.toList) {
        inserts.append("insert into \"public\".\"" + VehiclePositions.tableName + "\" (" + vp.id + ", \'" + vp.time + "\'," + vp.lineId + ", " + vp.vehicleId + ", " + vp.pointId + ");\n")
      }
    }
    val fw = new FileWriter(insertFile, true)
    fw.write(inserts.toString())
    fw.close()
  }

}