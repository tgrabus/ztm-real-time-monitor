package models.repository
import scala.slick.driver.PostgresDriver.simple._
import Database.threadLocalSession
import models.schema.Line

/**
 * User: Tomasz
 * Date: 25.01.14
 */
trait LineTrait {

  def getDistinctLines(implicit session : Session) : List[Line]

  def getDirectionsForLine(lineName : String)(implicit session: Session) : List[String]

}
