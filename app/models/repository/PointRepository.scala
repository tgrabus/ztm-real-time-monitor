package models.repository

import models.schema._
import scala.slick.driver.PostgresDriver.simple._
/**
 * User: Tomasz
 * Date: 15.06.13
 */
class PointRepository extends GenericRepository[Point]  with RepositoryWithId {

  def getById(id: Int)(implicit session: Session) = {
    Option(Query(Points).filter(_.id === id).first)
  }

  def getAll(implicit session: Session) = {
    Query(Points).list
  }

  def insert(entity: Point)(implicit session: Session) {
    Points.insert(entity)
  }

  def delete(entity: Point)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: Point)(implicit session: Session) {}

  def generateId(implicit session: Session): Int = {
    if (0 == lastId) {
      val tmp = Query(Points.map(_.id).max).first
      lastId = tmp.getOrElse(0)
    }
    lastId = lastId + 1
    lastId
  }
}