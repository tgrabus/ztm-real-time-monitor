package models.repository

import scala.slick.driver.PostgresDriver.simple._
import models.schema._

/**
 * User: Tomasz
 * Date: 18.06.13
 */
class LinePointRepository extends GenericRepository[LinePoints] {
  def getById(id: Int)(implicit session: Session) = ???

  def getAll(implicit session: Session) = {
    Query(LinesPoints).list()
  }

  def insert(entity: LinePoints)(implicit session: Session) {
    LinesPoints.insert(entity)
  }

  def delete(entity: LinePoints)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: LinePoints)(implicit session: Session) {}

  def getByLineId(id: Int)(implicit  session: Session) = {
    Query(LinesPoints).filter(_.lineId === id).list()
  }
}
