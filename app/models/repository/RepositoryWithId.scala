package models.repository

import scala.slick.driver.PostgresDriver.simple._

/**
 * User: Kuczi
 */
trait RepositoryWithId {
  protected var lastId = 0
  def generateId(implicit session: Session) : Int
}
