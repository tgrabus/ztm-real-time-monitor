package models.repository

import models.schema._
import scala.slick.driver.PostgresDriver.simple._
import collection.mutable
import scala.slick.jdbc.{GetResult, StaticQuery}
//import scala.slick.jdbc.GetResult
/**
 * User: Tomasz
 * Date: 15.06.13
 */
class StopRepository extends GenericRepository[Stop]  with RepositoryWithId {

  def getById(id: Int)(implicit session: Session) = {
    Option(Query(Stops).filter(_.id === id).first())
  }
  def getStopByName(name : String)(implicit session: Session) = {
    Option(Query(Stops).filter(_.name === name).first())
  }
  def getStopsByName(name : String)(implicit session: Session) = {
    Option(Query(Stops).filter(_.name === name).list())
  }
  def getAll(implicit session: Session) : List[models.schema.Stop] = {
    Query(Stops).list()
  }

  def insert(entity: Stop)(implicit session: Session) {
    Stops.insert(entity)
  }

  def delete(entity: Stop)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: Stop)(implicit session: Session) {}

  def generateId(implicit session: Session): Int = {
    if (0 == lastId) {
      val tmp = Query(Stops.map(_.id).max).first
      lastId = tmp.getOrElse(0)
    }
    lastId = lastId + 1
    lastId
  }
  
  def getSimilarStopByName(name : String)(implicit session: Session) : Stop= {
	  val s = for {
	    cs <- Stops if cs.name like name
	  } yield (cs)
 
	  s.first
  }
  implicit val getSupplierResult = GetResult(r => models.schema.Stop(r.<<, r.<<, r.<<))
  def getNearestStops(lat : Float, long : Float) (implicit session: Session) : List[Stop] = {
    val list = StaticQuery.queryNA[Stop]("select mytable.\"id\" as id, mytable.\"name\" as name, mytable.\"pointId\" as pointId " +
      "from ( SELECT \n  \"Stops\".id, \n  \"Stops\".\"pointId\", \n  \"Points\".latitude, \n  \"Points\".longitutde, \n " +
      " \"Stops\".name,\n  distance("+lat.toString +", "  + long.toString +", \"Points\".latitude, \"Points\".longitutde) as dist\nFROM \n " +
      " public.\"Points\", \n  public.\"Stops\"\nWHERE \n  \"Points\".id = \"Stops\".\"pointId\" and\n " +
      " \"Points\".latitude != 54.3345\n  order by dist asc ) mytable where dist< 0.118901880928419").list()
    list
  }

}


/*

 CREATE OR REPLACE FUNCTION distance(lat1 FLOAT, lon1 FLOAT, lat2 FLOAT, lon2 FLOAT) RETURNS FLOAT AS $$
DECLARE
    x float = 69.1 * (lat2 - lat1);
    y float = 69.1 * (lon2 - lon1) * cos(lat1 / 57.3);
BEGIN
    RETURN sqrt(x * x + y * y);
END
$$ LANGUAGE plpgsql;

*/
