package models.repository

import models.schema.{ Schedule, Schedules}
import scala.slick.driver.PostgresDriver.simple._
/**
 * User: Tomasz
 * Date: 18.06.13
 */
class ScheduleRepository extends GenericRepository[Schedule]  with RepositoryWithId {
  def getById(id: Int)(implicit session: Session) = ???

  def getAll(implicit session: Session) = {
    Query(Schedules).list()
  }

  def insert(entity: Schedule)(implicit session: Session) {
    Schedules.insert(entity)
  }
  def getByLineStopId(id: Int)(implicit  session: Session) = {
    Query(Schedules).filter(_.idLineStop === id).list()
  }
  
  def getByLineStopIdAndType(id: Int, scheduleType: Int)(implicit  session: Session) = {
    val q = Query(Schedules).filter(_.idLineStop === id)
    q.filter(_.scheduleType === scheduleType).list()
  }
  
  def delete(entity: Schedule)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: Schedule)(implicit session: Session) {}

  def generateId(implicit session: Session): Int = {
    if (0 == lastId) {
      val tmp = Query(Schedules.map(_.id).max).first
      lastId = tmp.getOrElse(0)
    }
    lastId = lastId + 1
    lastId
  }
}
