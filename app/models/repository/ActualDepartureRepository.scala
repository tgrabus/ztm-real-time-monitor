package models.repository

import scala.slick.driver.PostgresDriver.simple._
import models.schema._

/**
 * User: Mateusz
 * Date: 10.11.2013
 */
class ActualDepartureRepository extends GenericRepository[ActualDeparture] with RepositoryWithId {
  def getById(id: Int)(implicit session: Session) = ???

  def getAll(implicit session: Session) = {
    Query(ActualDepartures).list
  }

  def insert(entity: ActualDeparture)(implicit session: Session) {
    ActualDepartures.insert(entity)
  }

  def delete(entity: ActualDeparture)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {

  }

  def update(entity: ActualDeparture)(implicit session: Session) {}



  def getActualFromStop(stop:Int)(implicit session: Session) = {
    Query(ActualDepartures).filter(_.stopId === stop).list
  }

  def generateId(implicit session: Session): Int = {
    if (0 == lastId) {
      val tmp = Query(ActualDepartures.map(_.id).max).first
      lastId = tmp.getOrElse(0)
    }
    lastId = lastId + 1
    lastId
  }
}
