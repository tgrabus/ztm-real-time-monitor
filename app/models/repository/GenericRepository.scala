package models.repository

import scala.slick.driver.PostgresDriver.simple._
import Database.threadLocalSession
import models.schema._

/**
 * User: Tomasz
 * Date: 15.06.13
 */
trait GenericRepository[T] {

  def getById(id : Int)(implicit session : Session) : Option[T]

  def getAll(implicit session : Session) : List[T]

  def insert(entity : T)(implicit session : Session)

  def delete(entity : T)(implicit session : Session)

  def deleteById(id : Int)(implicit session : Session)

  def update(entity : T)(implicit session : Session)
}
