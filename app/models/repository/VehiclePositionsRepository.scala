package models.repository

import models.schema._
import scala.slick.driver.PostgresDriver.simple._
import java.sql.Timestamp
import org.joda.time._

/**
 * User: Kuczi
 */
class VehiclePositionsRepository extends GenericRepository[VehiclePosition] with RepositoryWithId {
  def getById(id: Int)(implicit session: Session): Option[VehiclePosition] = ???

  def getAll(implicit session: Session) = {
    Query(VehiclePositions).list()
  }

  def delete(entity: VehiclePosition)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: VehiclePosition)(implicit session: Session) {}

  def generateId(implicit session: Session): Int = {
    if (0 == lastId) {
      val tmp = Query(VehiclePositions.map(_.id).max).first
      lastId = tmp.getOrElse(0)
    }
    lastId = lastId + 1
    lastId
  }

  def insert(entity: VehiclePosition)(implicit session: Session) {
    VehiclePositions.insert(entity)
  }

  def getVehiclesOnLineById(id:Int)(implicit session: Session) = {

    val now = new DateTime(DateTimeZone.forID("Europe/Warsaw"))
    val tse = now.minusSeconds(20)
    val start = new Timestamp(1,1,1, tse.getHourOfDay(), tse.getMinuteOfHour, tse.getSecondOfMinute(),1)
    val end = new Timestamp(1,1,1,  now.getHourOfDay(), now.getMinuteOfHour(), now.getSecondOfMinute(), 1)
    Query(VehiclePositions).filter(_.lineId === id).filter(_.time >= start).filter(_.time <= end).list()
  }


  def getAllVehiclesOnLines (implicit session: Session) = {
    val now = new DateTime(DateTimeZone.forID("Europe/Warsaw"))
    val tse = now.minusSeconds(20)
    val start = new Timestamp(1,1,1, tse.getHourOfDay(), tse.getMinuteOfHour, tse.getSecondOfMinute(),1)
    val end = new Timestamp(1,1,1,  now.getHourOfDay(), now.getMinuteOfHour(), now.getSecondOfMinute(), 1)
    Query(VehiclePositions).filter(_.time >= start).filter(_.time <= end).list()
  }
}
