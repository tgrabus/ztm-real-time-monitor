package models.repository

import models.schema.{Transport,Transports}
import scala.slick.driver.PostgresDriver.simple._

/**
 * User: Kuczi
 */
class TransportRepostory  extends GenericRepository[Transport]{
  def getById(id: Int)(implicit session: Session): Option[Transport] = {
    try {
      Option(Query(Transports).filter(_.id === id).first())
    }
    catch {
      case nse: NoSuchElementException => None
    }
  }

  def getAll(implicit session: Session): List[Transport] = {
    Query(Transports).list()
  }

  def insert(entity: Transport)(implicit session: Session) {
    Transports.insert(entity)
  }

  def delete(entity: Transport)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: Transport)(implicit session: Session) {}

}
