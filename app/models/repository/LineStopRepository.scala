package models.repository

import models.schema._
import scala.slick.driver.PostgresDriver.simple._

/**
 * User: Tomasz
 * Date: 18.06.13
 */
class LineStopRepository extends GenericRepository[LineStops] with RepositoryWithId {
  def getById(id: Int)(implicit session: Session) = ???

  def getAll(implicit session: Session) = {
    Query(LinesStops).list()
  }

  def insert(entity: LineStops)(implicit session: Session) {
    LinesStops.insert(entity)
  }

  def delete(entity: LineStops)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: LineStops)(implicit session: Session) {}

  def getByLineId(id: Int)(implicit  session: Session) = {
    Query(LinesStops).filter(_.lineId === id).list()
  }

  def getByStopId(id: Int)(implicit  session: Session) = {
    Query(LinesStops).filter(_.stopId === id).list()
  }


  def getByLineIdAndStopId(lineid: Int, stopid : Int)(implicit  session: Session) = {
    Query(LinesStops).filter(_.lineId === lineid).filter(_.stopId === stopid).first()
  }

  def generateId(implicit session: Session): Int = {
    if (0 == lastId) {
      val tmp = Query(Lines.map(_.id).max).first
      lastId = tmp.getOrElse(0)
    }
    lastId = lastId + 1
    lastId
  }
}
