package models.repository


import scala.slick.driver.PostgresDriver.simple._
import models.schema._
/**
 * User: Tomasz
 * Date: 15.06.13
 */
class LineRepository extends GenericRepository[Line] with RepositoryWithId with LineTrait {

  def getById(id: Int)(implicit session: Session) = {
    Option(Query(Lines).filter(_.id === id).first)
  }

  def getAll(implicit session: Session) = {
    Query(Lines).list
  }

  def insert(entity: Line)(implicit session: Session) {
    Lines.insert(entity)
  }

  def delete(entity: Line)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: Line)(implicit session: Session) {}

  def generateId(implicit session: Session): Int = {
    if (0 == lastId) {
      val tmp = Query(Lines.map(_.id).max).first
      lastId = tmp.getOrElse(0)
    }
    lastId = lastId + 1
    lastId
  }

  def getByName(name:String)(implicit session: Session) = {
    Query(Lines).filter(_.name === name).list
  }

  def getByNameAndDirection(name:String, direction:String) (implicit session: Session):Line = {
    Query(Lines).filter(_.name === name).filter(_.direction === direction).first
  }

  def getDistinctLines(implicit session: Session) : List[Line] = {
    val lines = getAll.groupBy(_.name)
    val result = lines.map(x => x._2).map(_.head).toList
    result
  }

  def getDirectionsForLine(lineName : String)(implicit session: Session) : List[String] = {
    getByName(lineName).map(_.direction)
  }

  def getLineStopsForDirection(lineName : String, direction : String)(implicit session: Session) : List[Stop] = {
    val lineRepo = new LineRepository
    val stopRepo = new StopRepository
    val lsRepo = new LineStopRepository

    controllers.Application.context.database.withSession{implicit session:Session =>
      val l = lineRepo.getByNameAndDirection(lineName, direction)

      var stops = List[Stop]()

      for(s <- lsRepo.getByLineId(l.id).toList) {
        val stop = stopRepo.getById(s.stopId).get

        stops = stops :+ stop
      }

      stops
    }
  }
}
