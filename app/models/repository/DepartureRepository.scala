package models.repository

import models.schema.{Departure, Departures}
import scala.slick.driver.PostgresDriver.simple._

/**
 * User: Tomasz
 * Date: 18.06.13
 */
class DepartureRepository extends GenericRepository[Departure] with RepositoryWithId {
  def getById(id: Int)(implicit session: Session) = ???

  def getAll(implicit session: Session) = {
    Query(Departures).list()
  }
  def getByScheduleId(id: Int)(implicit  session: Session) = {
    Query(Departures).filter(_.scheduleId === id).list()
  }
  def insert(entity: Departure)(implicit session: Session) {
    Departures.insert(entity)
  }

  def delete(entity: Departure)(implicit session: Session) {}

  def deleteById(id: Int)(implicit session: Session) {}

  def update(entity: Departure)(implicit session: Session) {}

  def generateId(implicit session: Session): Int = {
    if (0 == lastId) {
      val tmp = Query(Departures.map(_.id).max).first
      lastId = tmp.getOrElse(0)
    }
    lastId = lastId + 1
    lastId
  }
}
