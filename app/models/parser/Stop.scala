package models.parser
import play.api.libs.json._

/**
 * Created with IntelliJ IDEA.
 * User: Kuczi
 * Date: 07.04.13
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */

object StopType extends Enumeration{
  val BusStop = Value(1,"Bus")
  val TramStop = Value(2, "Tram")
}

/**
 *
 * @param name
 * @param position
 */
class Stop(val id:Int, val name:String, var position : Point, var schedule : Array[ScheduleEntry] ) {
    def nameStop = name
    def positionStop = position
    var directionStop = ""
    var scheduleStop = schedule

    def setDirection(direction: String)
    {
          directionStop = direction
    }
}
object Stop {
  implicit object StopFormat extends Format[Stop] {
    def reads(json: JsValue) = {
      new JsSuccess(
        new Stop((json \ "id").as[Int],
          (json \ "name").as[String],
          (json \ "position").as[Point],
          null
        )
      )
    }

    def writes(input: Stop): JsValue = {
      Json.obj("id" -> input.id,
        "name" -> input.name,
        "position" -> input.position)
    }
  }
}