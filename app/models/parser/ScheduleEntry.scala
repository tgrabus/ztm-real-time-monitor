package models.parser

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable

class ScheduleEntry(var line:String, var typeNameArg: String)
{
  def lineSchedule = line
  def typeName = typeNameArg

  var schedule = new ArrayBuffer[ArrayBuffer[String]];

  for(i <- 1 to 25)
    {
    schedule +=  new ArrayBuffer[String]
  }

  def AddValue(hour:Int, minute:String)
  {
    schedule(hour) += minute
  }
}
