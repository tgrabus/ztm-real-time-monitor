/*
* Modified by grabus
*
*
* */

package models.parser

import play.api.libs.json._
import scala.reflect.api.Position

abstract class Transport {
  def id: Int
  def associatedLine: Line
  def position: Point
}

class Bus(busId: Int, busAssociatedLine: Line, busPosition: Point) extends Transport {
  def id = busId
  def associatedLine = busAssociatedLine
  def position = busPosition
}

class Tram(tramId: Int, tramAssociatedLine: Line, tramPosition: Point) extends Transport {
  def id = tramId
  def associatedLine = tramAssociatedLine
  def position = tramPosition
}

object Tram {
  implicit object TramFormat extends Format[Tram] {
    def reads(json: JsValue) = {
      new JsSuccess(
        new Tram(
            (json \ "id").as[Int],
            new Line(0, (json \ "linia").as[String], (json \ "kierunek").as[String],List[models.parser.Stop](), List[Point]()),
            new Point((json \ "pozycja" \ "szerokosc").as[Double], (json \ "pozycja" \ "dlugosc").as[Double])
            )
        )
    }
    
    def writes(input: Tram): JsValue = {
      Json.obj("id" -> input.id,
          "linia" -> input.associatedLine.name,
          "kierunek" -> input.associatedLine.direction,
          "pojazd" -> input.getClass().getSimpleName,
          "pozycja" -> Json.obj(
        		  "dlugosc" -> input.position.longtitude,
        		  "szerokosc" -> input.position.latitude)
        		  )
    }
  }
}

object Bus {
  implicit object BusFormat extends Format[Bus] {
    def reads(json: JsValue) = {
      new JsSuccess(
        new Bus(
            (json \ "id").as[Int],
            new Line(0, (json \ "linia").as[String], (json \ "kierunek").as[String],List[models.parser.Stop](), List[Point]()),
            new Point((json \ "pozycja" \ "szerokosc").as[Double], (json \ "pozycja" \ "dlugosc").as[Double])
            )
        )
    }
    
    def writes(input: Bus): JsValue = {
      Json.obj("id" -> input.id,
          "linia" -> input.associatedLine.name,
          "kierunek" -> input.associatedLine.direction,
          "pojazd" -> input.getClass().getSimpleName,
          "pozycja" -> Json.obj(
        		  "dlugosc" -> input.position.longtitude,
        		  "szerokosc" -> input.position.latitude)
        		  )
    }
  }
}

object Transport {
  implicit object TransportFormat extends Format[Transport] {
    def reads(json: JsValue) = {
      new JsSuccess(
        /*new Tram(
            (json \ "id").as[Int],
            new Line(0, (json \ "linia").as[String], (json \ "kierunek").as[String],List[models.parser.Stop](), List[Point]()),
            new Point((json \ "pozycja" \ "szerokosc").as[Double], (json \ "pozycja" \ "dlugosc").as[Double])
            )*/
          null
        )     
    }
    
    def writes(input: Transport): JsValue = {
      Json.obj("id" -> input.id,
          "linia" -> input.associatedLine.name,
          "kierunek" -> input.associatedLine.direction,
          "pojazd" -> input.getClass().getSimpleName,
          "pozycja" -> Json.obj(
        		  "dlugosc" -> input.position.longtitude,
        		  "szerokosc" -> input.position.latitude)
        		  )
    }
  }
}