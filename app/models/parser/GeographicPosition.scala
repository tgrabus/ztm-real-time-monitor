package models.parser

/**
 * Created with IntelliJ IDEA.
 * User: Kuczi
 * Date: 07.04.13
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */
import play.api.libs.json._

class Point (var latitude: Double, var longtitude : Double)    {
    def lat = latitude
    def long = longtitude

    def likeJson: JsObject = {
           val obj = Json.obj(
             "lat" -> latitude,
             "long" -> longtitude
           )
           obj
         }
}

object Point {
  implicit object PointFormat extends Format[Point] {
    def reads(json: JsValue) = {
      new JsSuccess(
        new Point(
          (json \ "lat").as[Double],
          (json \ "_timeToArrive").as[Double]
        )
      )
    }

    def writes(input: Point): JsValue = {
      Json.obj("lat" -> input.lat,
        "long" -> input.long)
    }
  }
}