package models.parser

/**
 *
 * @param name
 * @param direction
 * @param stops
 * @param waypoints geographic coordinates for path drawing
 */
import play.api.libs.json._


class Line(val id : Int,
           val name:String,
           val direction:String,
           var stops:List[models.parser.Stop],
           var waypoints:List[Point]) {

  def this() = this(0, "", "", List(), List());


  def likeJson: JsObject = {
    val obj = Json.obj(
      "id" -> id,
      "nazwa" -> name,
      "kierunek" -> direction,
      "points" -> waypoints.map {
        w => Json.obj(
            "lat" -> w.latitude,
            "long" -> w.longtitude
          )
      }
    )
    obj
  }
}
