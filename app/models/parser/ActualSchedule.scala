package models.parser

import play.api.libs.json._
import org.joda.time._

class ActualScheduleEntry(val lineName:String, val direction:String, val timeToArrive:String)  {
	def timeToArriveInMinutes(date:DateTime) :Int = {
	  if("teraz" == timeToArrive) {
	    0
	  } else if(timeToArrive.contains(":")) {
	    val el = timeToArrive.split(":")
	    val h = el(0).trim.toInt - date.getHourOfDay()
	    val m = el(1).trim.toInt - date.getMinuteOfHour()
	    if(m < 0) {
	      (h - 1) * 60 + m + 60
	    } else {
	      h * 60 + m
	    }
	  } else {
	    var r:String = timeToArrive.replaceAll("za", "")
	    r = r.replaceAll("min", "")
	    r.trim.toInt
	  }
	}
	
	def timeToArriveInMinutes :Int = {
	  timeToArriveInMinutes(DateTime.now())
	}
}

object ActualScheduleEntry {
  implicit object ActualScheduleEntryFormat extends Format[ActualScheduleEntry] {
	  def reads(json: JsValue) = {
	      new JsSuccess(
	        new ActualScheduleEntry(
	          (json \ "line").as[String],
	          (json \ "direction").as[String],
	          (json \ "timeLeft").as[String]
	        )
	      )
    }

    def writes(input: ActualScheduleEntry): JsValue = {
      Json.obj(
		"line" -> input.lineName,
		"direction" -> input.direction,
		"timeLeft" -> input.timeToArrive
		)
    }
  }
}

class ActualSchedule(var entries:List[ActualScheduleEntry]) {
	
}

object ActualSchedule {
  implicit object ActualScheduleFormat extends Format[ActualSchedule] {
	  def reads(json: JsValue) = {
	  var entries = List[ActualScheduleEntry]()
	  //TODO fix deserialization
	  /*for(entry <- (json.asInstanceOf[JsArray]).value) {
	    entries :+ ActualScheduleEntry.t .reads(entry)
	  }*/
      new JsSuccess(new ActualSchedule(entries))
    }

    def writes(input: ActualSchedule): JsValue = {
      Json.toJson(input.entries)
    }
  }
}