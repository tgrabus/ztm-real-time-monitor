package models.parser

import scala.collection.mutable.HashMap
import org.joda.time._
import org.joda.time.format.DateTimeFormat

class Schedule(val departures : HashMap[Int, List[Departure]], val activeFrom:DateTime, val activeTo:DateTime, val onDemand:String, val scheduleType:Int) {
	
  def findClosestDeparture(scheduleType:Int, time:DateTime = DateTime.now()) = {
    var distance = Int.MaxValue
    var closest:Departure = null

    var dl = List[Departure]()
    for((k,v) <- departures) {
      dl = dl ++ v
    }
    val dep = new Departure(DateTime.now(), "")
    for(d <- dl) {
	  val dist = Departure.differenceInMinutes(d, dep)
      if(dist < distance) {
        distance = dist
        closest = d
      }
    }
    closest
  }
  def getCountDepartures() : Int = {
    var i = 0
    departures.foreach( c =>
      c._2.foreach( p =>
        i = i + 1)
    )
    i
  }
  implicit def dateTimeOrdering: Ordering[DateTime] = Ordering.fromLessThan(_ isBefore _)
  def getFirstDepartureFrom(fromTime : DateTime) : DateTime = {
       var listOfDepartures = List[Departure]()
        departures.foreach( c =>
          c._2.foreach( p => {
              listOfDepartures = listOfDepartures :+ p;  }
            )
          )

    var x = listOfDepartures.filter(d => d.time.isAfter(fromTime) || d.time.isEqual(fromTime)).toList
    x = x.sortBy(x=>x.time)
    try {
        val y = x.head.time
        y
    } catch {
      case ex : Exception =>
        fromTime.plusMinutes(160)
    }
  }
  
  def findEalierDeparture(scheduleType:Int, time:DateTime = DateTime.now()) = {
    var distance = Int.MaxValue
    var closest:Departure = null
    val dep = new Departure(DateTime.now(), "")
    var dl = List[Departure]()
    for((k,v) <- departures) {
      dl = dl ++ v
    }
    for(d <- dl) {
        if(time.isAfter(d.time) ) {
        val dist = Departure.differenceInMinutes(d, dep)
        if(dist < distance) {
          closest = d
          distance = dist
        }
      }
    }
    closest
  }
}

object Schedule {
  val NORMAL_SCHEDULE = 1
  val SATURDAY_SCHEDULE = 2
  val HOLIDAY_SCHEDULE = 3
}

class Departure(val time:DateTime, val info:String){
  
}

object Departure {
  def differenceInMinutes(first:Departure, second:Departure) = {
    val h = first.time.getHourOfDay() - second.time.getHourOfDay()
    val m = first.time.getMinuteOfHour() - second.time.getMinuteOfHour()
    if(0 == h) {
      Math.abs(m)
    } else {
      Math.abs(h * 60 + m)
    }
  }
}