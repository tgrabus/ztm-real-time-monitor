package models.parser

import play.api.libs.json._
import play.api.data.format.Formats
import play.api.data.format
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.functional.syntax._
import play.api.libs.json._

/**
 * Created with IntelliJ IDEA.
 * User: Adrian
 * Date: 17.04.13
 * Time: 17:05
 * To change this template use File | Settings | File Templates.
 */
class ArrivingBusEntry(lineNumber :Int, timeToArrive: Int, direction: String) {

    def _lineNumber = lineNumber

    def _timeToArrive = timeToArrive

    def _direction = direction
}

object Formatter{

  implicit val Formatter: Format[ArrivingBusEntry] = new Format[ArrivingBusEntry] {

    def reads(json: JsValue) = {
      new JsSuccess(
        new ArrivingBusEntry(
          (json \ "_lineNumber").as[Int],
          (json \ "_timeToArrive").as[Int],
          (json \ "_direction").as[String]
        )
      )
    }

    def writes(input: ArrivingBusEntry): JsValue = {
      Json.obj("_lineNumber" -> input._lineNumber,
        "_timeToArrive" -> input._timeToArrive,
        "_direction" -> input._direction)
    }

  }
}





