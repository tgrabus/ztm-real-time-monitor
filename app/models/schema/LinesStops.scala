package models.schema
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.session.{Session}

/**
 * User: Tomasz
 * Date: 29.05.13
 */

case class LineStops(id : Int, stopId:Int, lineId:Int, onDemand:Boolean) {
}

object LinesStops extends Table[LineStops]("LinesStops") {
  def id = column[Int]("id", O.PrimaryKey)
  def stopId = column[Int]("stopId")
  def lineId = column[Int]("lineId")
  def onDemand = column[Boolean]("onDemand")

  def * = id ~ stopId ~ lineId ~ onDemand  <> (LineStops, LineStops.unapply _)

  def stop = foreignKey("FK_linesstops_stops", stopId, Stops)(_.id)
  def line = foreignKey("FK_linesstops_points", lineId, Lines)(_.id)

}
