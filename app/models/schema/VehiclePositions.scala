package models.schema

import slick.driver.PostgresDriver.simple._
import scala.slick.session.{Session}
import java.sql.Timestamp

/**
 * User: Kuczi
 */
case class VehiclePosition(id:Int, time:Timestamp, lineId:Int, vehicleId:Int, pointId:Int) {

}

object VehiclePositions extends Table[VehiclePosition]("VehiclePositions") {
  def id = column[Int]("id", O.PrimaryKey)
  def time = column[Timestamp]("time")
  def lineId = column[Int]("line")
  def vehicleId = column[Int]("vehicle_id")
  def pointId = column[Int]("point_id")
  def * = id ~ time ~ lineId ~ vehicleId ~ pointId <> (VehiclePosition, VehiclePosition.unapply _)


  def line = foreignKey("FK_vehiclepositions_lines", lineId, Lines)(_.id)
  def vehicle = foreignKey("FK_vehiclepositions_transports", vehicleId, Transports)(_.id)
  def point = foreignKey("FK_vehiclepositions_points", pointId, Points)(_.id)
}