package models.schema

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.session.{Session}
import scala.slick.jdbc.GetResult

/**
 * User: Tomasz
 * Date: 29.05.13
 */

case class Stop(id:Int, name:String, pointId:Int) {
  def lineStops(implicit session: Session) = (for {
    ls <- LinesStops if ls.stopId === id
  } yield ls)
}


object Stops extends Table[Stop]("Stops") {
  def id = column[Int]("id", O.PrimaryKey)
  def name = column[String]("name")
  def pointId = column[Int]("pointId")
  def * = id ~ name ~ pointId <> (Stop, Stop.unapply _)

  def point = foreignKey("FK_stops_points", pointId, Points)(_.id)



}



