package models.schema

import scala.slick.driver.PostgresDriver.simple._
import scala.slick.session.{Session}

/**
 * User: Tomasz
 * Date: 29.05.13
 */

case class Line(id:Int, name:String, direction:String, lineKind:Int) {
  def lineStops(implicit session: Session) = (for {
      ls <- LinesStops if ls.lineId === id
    } yield ls)

  def linePoints(implicit session: Session) = (for {
    lp <- LinesPoints if lp.lineId === id
  } yield lp)
}

object Lines extends Table[Line]("Lines") {
  def id = column[Int]("id", O.PrimaryKey)
  def name = column[String]("name")
  def direction = column[String]("direction")
  def lineKind = column[Int]("lineKind")

  def * = id ~ name ~ direction ~ lineKind <> (Line, Line.unapply _)
}