package models.schema
import java.sql.Timestamp
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.session.{Session}

/**
 * User: Mateusz
 * Date: 10.11.13
 */

case class ActualDeparture(id : Int, stopId:Int, lineId:Int, departureTime: java.sql.Time, nowArriving : Int, lineName : String, direction : String)

object ActualDepartures extends Table[ActualDeparture]("ActualDeparture") {
  def id = column[Int]("id", O.PrimaryKey)
  def stopId = column[Int]("stopId")
  def lineId = column[Int]("lineId")
  def toArrive = column[Int]("toArrive")

  def lineName = column[String]("lineName")
  def direction = column[String]("direction")

  def departureTime = column[java.sql.Time]("departureTime")

  def * = id ~ stopId ~ lineId ~ departureTime ~ toArrive  ~ lineName ~ direction <> (ActualDeparture, ActualDeparture.unapply _)

  def stop = foreignKey("FK_actualdeparture_stops", stopId, Stops)(_.id)

  def line = foreignKey("FK_actualdeparture_lines", lineId, Lines)(_.id)

}
