package models.schema
import slick.driver.PostgresDriver._
/**
 * User: Tomasz
 * Date: 29.05.13
 */
case class Transport(id:Int, model:String, manufacturer:String, seats:Int, capacity:Int)

object Transports extends Table[Transport]("Transports") {
  def id = column[Int]("id", O.PrimaryKey)
  def model = column[String]("model")
  def manufacturer = column[String]("manufacturer")
  def seats = column[Int]("seats")
  def capacity = column[Int]("capacity")
  def * = id ~ model ~ manufacturer ~ seats ~ capacity <> (Transport, Transport.unapply _)
}