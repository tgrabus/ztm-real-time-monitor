package models.schema
import slick.driver.PostgresDriver._
import java.sql.Timestamp

/**
 * User: Tomasz
 * Date: 29.05.13
 */

case class Departure(id:Int, departureTime:Timestamp, departureInfo:String, scheduleId:Int)

object Departures extends Table[Departure]("Departures") {
  def id = column[Int]("id", O.PrimaryKey)
  def departureTime = column[Timestamp]("departureTime")
  def departureInfo = column[String]("departureInfo")
  def scheduleId = column[Int]("scheduleId")
  def * = id ~ departureTime ~ departureInfo ~ scheduleId <> (Departure, Departure.unapply _)

  def schedule = foreignKey("FK_departures_schedules", scheduleId, Schedules)(_.id)
}