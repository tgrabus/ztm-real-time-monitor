package models.schema
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.session.{Session}
import java.sql.Timestamp
import slick.lifted.PrimaryKey

/**
 * User: Tomasz
 * Date: 29.05.13
 */

case class Schedule(id:Int, activeFrom:Timestamp, activeTo:Timestamp, onDemand : String, idLineStop : Int, scheduleType : Int)  {
  def departures(implicit session: Session) = (for {
    d <- Departures if d.scheduleId === id
  } yield d)
}

object Schedules extends Table[Schedule]("Schedules") {
  def id = column[Int]("id", O.PrimaryKey)
  def activeFrom = column[Timestamp]("activeFrom")
  def activeTo = column[Timestamp]("activeTo")
  def onDemand = column[String]("onDemend")
  def idLineStop = column[Int]("idLineStop")
  def scheduleType = column[Int]("scheduleType")

  def * = id ~ activeFrom ~ activeTo ~ onDemand ~ idLineStop ~ scheduleType <> (Schedule, Schedule.unapply _)

  def linestop = foreignKey("FK_linesstops", idLineStop, LinesStops)(_.id)


}