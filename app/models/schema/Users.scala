package models.schema
import slick.driver.PostgresDriver._
/**
 * User: Tomasz
 * Date: 29.05.13
 */

case class User(id:Int, login:String, passwd:String, settings:String)

object Users extends Table[User]("Users") {
  def id = column[Int]("id", O.PrimaryKey)
  def login = column[String]("login")
  def passwd = column[String]("passwd")
  def settings = column[String]("settings")

  def * = id ~ login ~ passwd ~ settings <> (User, User.unapply _)
}