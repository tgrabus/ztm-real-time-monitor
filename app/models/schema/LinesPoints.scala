package models.schema
import slick.driver.PostgresDriver.simple._
/**
 * User: Tomasz
 * Date: 29.05.13
 */

case class LinePoints(lineId:Int, pointId:Int, isStop:Boolean)

object LinesPoints extends Table[LinePoints]("LinesPoints") {
  def lineId = column[Int]("lineId")
  def pointId = column[Int]("pointId")
  def isStop = column[Boolean]("isStop")
  def * = lineId ~ pointId ~ isStop  <> (LinePoints, LinePoints.unapply _)

  def pk = primaryKey("PK_linespoints", (lineId, pointId))
  def line = foreignKey("FK_linespoints_lines", lineId, Lines)(_.id)
  def point = foreignKey("FK_linespoints_points", pointId, Points)(_.id)
}
