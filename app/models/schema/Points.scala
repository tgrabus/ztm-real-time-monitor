package models.schema
import scala.slick.driver.PostgresDriver.simple._
import scala.slick.session.{Session}

/**
 * User: Tomasz
 * Date: 29.05.13
 */

case class Point(id:Int, latitude:Float, longitude:Float) {
  def linePoints(implicit session: Session) = (for {
    lp <- LinesPoints if lp.pointId === id
  } yield lp)

  def stop(implicit session: Session) = Query(Stops).filter(_.pointId === id).take(1)
}

object Points extends Table[Point]("Points") {
  def id = column[Int]("id", O.PrimaryKey)
  def latitude = column[Float]("latitude")
  def longitude = column[Float]("longitutde")

  def * = id ~ latitude ~ longitude  <> (Point, Point.unapply _)
}
