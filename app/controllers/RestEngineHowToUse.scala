package controllers
import play.api.libs.json._
import play.api.mvc._
import play.api.libs.json.Json._
import models.parser.{Formatter, ArrivingBusEntry}
import Formatter._
import models.parser.ArrivingBusEntry


object RestEngineHowToUse extends Controller {

  def RestEngineHowToUse() =
  Action
  {
    Ok(views.html.RestEngineHowToUse());
  }
}