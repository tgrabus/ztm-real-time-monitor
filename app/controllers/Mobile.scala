package controllers

import play.api.mvc._
import play.api.libs.iteratee.Enumerator
import services.{StopManager, LineManager}


/**
 * User: Tomasz
 * Date: 14.12.13
 */
object Mobile  extends Controller
{
  def getLines = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(LineManager.getLines))
  }

  def getDirectionsForLine(lineName : String) = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(LineManager.getDirectionsForLine(lineName)))
  }

  def getLineStopsForDirection(lineName : String, direction : String) = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(LineManager.getLineStopsForDirection(lineName, direction)))
  }

  def getStops = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(StopManager.getAllStops))
  }
}
