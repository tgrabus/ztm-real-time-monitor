package controllers

import play.api._
import play.api.mvc._
import services._
import models._
import play.api.libs.iteratee.Enumerator
import play.api.libs.json.Json
import play.api.cache.Cache
import play.api.Play.current
import models.parser.ActualSchedule
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat


object Application extends Controller {

  val context = new ZTMContext
  
  def index = Action {
    Ok(views.html.layout1())
  }

  def inserts = Action {
    context.InsertData
    Ok(views.html.layout1())
  }

  def getRoute(stopA: String, stopB : String, time : String) = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")),
      Enumerator(RoutingManager.detRoutesByStopName(stopA, stopB, time)))
  }
  def getRouteEntries(lineId : String, stopA: String, stopB : String, time : String) = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")),
      Enumerator(RoutingManager.getRouteByDetailedSettings(lineId, stopA, stopB, time)))
  }



  def getLineVehicles(name: String) = Action {
    val vehicles = VehicleManager.estimateVehiclePositionsOnLine(name)
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(Json.toJson(vehicles)))
  }
  
  def getLineStopsAndPath(name: String) = Action {
    val line = LineManager.getLine(name)
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(LineConverter.getLineAsJson(line))) 
  }

  def getAdvancedRoute(fromName : String, toName: String, time: String,
            transportation : String, byWalk : String ) = Action {
       val formatter = DateTimeFormat.forPattern("dd-MM-yyyy-HH:mm");
       val dt = formatter.parseDateTime(time);
       val scheduleType = 1
       val results = AdvancedPlanner.getRoutes(fromName, toName,
         dt, scheduleType, transportation.toInt, byWalk.toInt);
        val json = AdvancedPlanner.prepareJson(results, dt)

    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(json))
  }

  def getAllVehicles = Action {
    val vehicles = VehicleManager.getPositionOfAllVehicles()
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(VehicleManager.vehiclesToJson(vehicles)))
  }

  def getActualScheduleForStop(stopId:String) = Action {
    val as = ActualScheduleManager.getStopSchedule(stopId)
    val js = Json.toJson(as)
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(js.toString))
  }

  def getStopNames = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(StopManager.getStopNames))
  }

  def getLinesWithDirections = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(LineManager.getLineNamesAsLabelIdPairs))
  }

  def getLineNames = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(LineManager.getLineNames))
  }
  def getActualArrivingsOnStop(stopId : String) = Action {
    val as = ActualScheduleManager.getStopSchedule(stopId)
    val js = Json.toJson(as)
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(js.toString))
  }

  def test(line : String, stop : String, time : String) = Action
  {
    val formatter = DateTimeFormat.forPattern("dd-MM-yyyy-HH:mm");
    val dt = formatter.parseDateTime(time);
        val sched = SchedulesManager.getSchedule(line.toInt, stop.toInt, 1)
        val times = sched.getFirstDepartureFrom(dt)
    val strtime = formatter.print(times)
     Ok(strtime)
  }

  def getLineNamesByName(name: String) = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(LineManager.getLineNamesByPartOfName(name)))
  }

  def getScheduleOnStop(lineId: String, stopId : String) = Action {
    SimpleResult(ResponseHeader(200, Map(CONTENT_TYPE -> "application/json")), Enumerator(SchedulesManager.getScheduleForLineAndStop(lineId, stopId)))
  }
}