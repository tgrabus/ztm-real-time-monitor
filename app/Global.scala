import models.ZTMContext
import play.api.GlobalSettings

import play.api.Application
import play.api.libs.concurrent.Akka
import scala.concurrent.duration._
import play.api.Play.current
import play.api.libs.concurrent.Execution.Implicits._

object Scheduler {
  def start() {
    Akka.system.scheduler.schedule(60 seconds, 60 minutes) {
      val context = new ZTMContext
      //context.InsertActualDepartures
    }
  }
}
object Global extends GlobalSettings {
  override def onStart(app: Application) {
       //Scheduler.start();
  }
}