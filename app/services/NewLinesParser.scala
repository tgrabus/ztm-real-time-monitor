package services

import models._
import models.parser.{ScheduleEntry, Stop, Line}
import collection.mutable.{ArrayBuffer, HashMap}
import scala.xml.Node


object NewLinesParser {

  def getLines: List[Line] = {
    val content = Helpers.loadSource("http://www.ztm.gda.pl/rozklady/")
    var snippet = Helpers.getSnippet(content.toString(), "<div id=\"content\">", "<div id=\"footer\">")
    snippet = Helpers.removeMarkup(snippet, Array("&apos;", "&nbsp;", "&raquo", "<br>", "&#\\d{1,4}[^;]"))

    snippet = snippet.replace(27.asInstanceOf[Char], ' ')
    snippet = snippet.replace(0x1e.asInstanceOf[Char], ' ')
    snippet = snippet.replaceAll("</P", "</p>") //bledy ztm
    snippet = snippet.replaceAll("<b><i>Latarnia Morska</b><i>", "")


    val xml = scala.xml.XML.loadString(snippet)
    val table = (xml \ "table").head

    var lines: List[Line] = Nil

    var str: String = ""
    table match {
      case table@ <table>{rows@_*}</table> if (table \ "@width" text) == "480" => {
        for (row <- rows) {
          row match {
            case <tr>{columns@_*}</tr> if columns(0).label != "th" =>
              val a = columns(0) \ "a"
              val link = a \ "@href"

              val stops = NewLineStopsParser.getAssociatedLineStops(link.text)

              val line1 = new Line(-1, a.text, stops(0).last.nameStop, stops(0), List())
              val line2 = new Line(-1, a.text, stops(1).last.nameStop, stops(1), List())

              println("---------------------------------------Line: "+ a.text + " successfully parsed.-------------------------------------------");

              lines = lines :+ line1 :+ line2
            case _ =>
          }
        }
      }
      case _ =>
    }
    lines
  }
}

object NewLineStopsParser {

  def getAssociatedLineStops(relativeLink: String): Array[List[Stop]] = {
    val link = "http://www.ztm.gda.pl/rozklady/" + relativeLink;
    val content = Helpers.loadSource(link)
    var snippet = Helpers.getSnippet(content.toString(), "<table width=\"480\" align=\"center\">", "<div id=\"departures-footer\">")

    // fragment moze nie zawierac korzenia wiec trzeba go dodac
    snippet = Helpers.addRootElement(snippet)
    snippet = Helpers.removeMarkup(snippet, Array("&nbsp"))
    val root = scala.xml.XML.loadString(snippet) \ "table"
    val tables = root \ "tr" \\ "table"


    val firstDirectionStops = getLineStops(tables.head, "http://www.ztm.gda.pl/rozklady/")

    for (stop <- firstDirectionStops) {
      stop.setDirection(firstDirectionStops.last.nameStop)
    }

    val secondDirectionStops = getLineStops(tables.last, "http://www.ztm.gda.pl/rozklady/")

    for (stop <- secondDirectionStops) {
      stop.setDirection(secondDirectionStops.last.nameStop)
    }

    Array(firstDirectionStops, secondDirectionStops)
  }

  private def getLineStops(table: Node, baseLink: String): List[Stop] = {
    var str: String = ""
    var stops: List[Stop] = Nil
    var schedule = new Array[ScheduleEntry](3);

    table match {
      case table@ <table>{rows@_*}</table> if (table \ "@class" text) == "route" => {
        for (row <- rows) {
          row match {
            case <tr>{columns@_*}</tr> if columns.head.label != "th" =>
              var stopName: String = ""
              val a = columns.head \\ "a"

              if (a.length != 0) {
                val link = a \ "@href"
                stopName = a.text

                try {
                  schedule = getStopSchedule(baseLink + link, stopName);
                  //println("Stop : " + stopName + ". Successfully parsed. Address: " + baseLink + link)
                }
                catch {
                  case e: Exception => {
                    //println("Error ocurred, during processing stop: " + stopName + ". Address: " + baseLink + link)
                  }
                }
              }
              else {
                stopName = columns.head.text
                schedule = null
              }

              stops = stops :+ new Stop(0, stopName.replaceAll(";", ""), null, schedule)

            case _ => str = "niedziala"
          }
        }
      }
    }

    stops.drop(1)
  }

  private def getStopSchedule(link: String, stopName: String): Array[ScheduleEntry] = {
    val content = Helpers.loadSource(link)

 //   println("dupa")

    var stop_header = Helpers.getSnippet(content.toString(), "<div id=\"departures-header\">", "<div id=\"SIP\">");
    var ztmid = Helpers.getSnippet(stop_header, "<i>", "</i>");
    ztmid = ztmid.replace("<i>", "");
    println("stop info: " + stopName + "to: " + ztmid + " <<");





    var snippet = Helpers.getSnippet(content.toString(), "<div class=\"departures-set\">", "<br class=\"clear\" />")
    snippet = Helpers.addRootElement(snippet)
    snippet = Helpers.removeMarkup(snippet, Array("&nbsp", "&raquo", "<br>"))
    var x = snippet
    var str: String = ""
    var stops: List[Stop] = Nil
    var result = new Array[ScheduleEntry](3)

    result(0) = new ScheduleEntry(stopName, "DniPowszednie")
    result(1) = new ScheduleEntry(stopName, "Soboty")
    result(2) = new ScheduleEntry(stopName, "NiedzieleSwieta")


    var xml = scala.xml.XML.loadString(snippet)


    var schedulesTypes = new Array[Node](3)
    var i: Int = 0

    (xml \ "div").foreach {
      div =>
      //println((div \ "@class").text + "\n")
        schedulesTypes(i) = div
        i += 1
    }

    i = 0
    var lastHour: String = ""
    for (node <- schedulesTypes) {

      if (node != null && (node \ "dif") != null) {
        (node \ "div").foreach {
          div =>
            if ((div \ "@class") != null && (div \ "@class").text == "h") {
              lastHour = div.text

            }
            if ((div \ "@class") != null && (div \ "@class").text == "m") {
              result(i).AddValue(lastHour.toInt, div.text)

            }
        }
      }
      i += 1
    }
    result
  }
}
