package services

import collection.mutable
import play.api.libs.json.Json
import models.repository._
import models.schema.Stop
import models.ZTMContext

object StopManager {
  import scala.slick.session.{Database, Session}

  def getStopNames = {
     var result = List[String]()
    controllers.Application.context.database.withSession { implicit session:Session =>
      val stopRepo = new StopRepository
      val map = new mutable.HashSet[String]()
      for(stop <- stopRepo.getAll.toList) {
        map.add(stop.name)
      }
      result = map.toList
    }
    Json.toJson(result)
  }

  def getAllStops = {

    var stops = List[models.schema.Stop]()
    val context = new ZTMContext

    context.database withSession{ implicit session:Session =>

      val stopRepo = new StopRepository()
      stops = stopRepo.getAll

    }

    val obj = Json.obj("stops" -> stops.map {
      stop => Json.obj(
      "id" -> stop.id,
      "name" -> stop.name
      )
    })

    Json.toJson(obj)
  }
}

