package services
import models.repository;
import org.joda.time.DateTime
import models.repository.{LineStopRepository, LineRepository, PointRepository, StopRepository }
import scala.slick.session.Session
import play.api.cache.Cache
import models.parser.ActualSchedule
import play.api.Play.current
import scala.collection.JavaConversions._
import play.api.libs.json.Json
import org.joda.time.format.DateTimeFormat

object AdvancedPlanner {

  val lineRepo = new LineRepository
  val stopRepo = new StopRepository
  val pointRepo = new PointRepository
  val lineStopRepo = new LineStopRepository

     def getGraph() : RoutingGraph = {
       val graph = Cache.getAs[RoutingGraph]("ztmgraf")
       var g = graph.getOrElse(null)
       if(g != null) {
          return g
       } else
         g = makeGraph();
         Cache.set("ztmgraf", g, 60 * 60 * 24)
         return g;
     }


     def makeGraph() : RoutingGraph = {
       var vertexes = List[Vertex]()
       var edges = List[Edge]()


       controllers.Application.context.database.withSession { implicit session:Session =>
            val lines = lineRepo.getAll


              for( l <- lines) {
                   val stopsOnLine = lineStopRepo.getByLineId(l.id)
                   //println("Line: " + l.name + " " + l.direction)
                   var i = 0;
                   var prevVertex = new Vertex(0, 0, 0, null, "")

                   for(stopOnLine <- stopsOnLine) {

                     var stopVertex = new Vertex(0, 0, 0, null, "")

                     if(vertexes.exists(v => v.stopId == stopOnLine.stopId)) {
                       //println("przystanek juz istnieje")
                       stopVertex = vertexes.filter(x=>x.stopId == stopOnLine.stopId).head
                     } else {
                       //println("dodaje przystanek")
                        val stopInfo = stopRepo.getById(stopOnLine.stopId)
                        val point = pointRepo.getById(stopInfo.get.pointId)

                        stopVertex = new Vertex(stopOnLine.stopId, point.get.latitude, point.get.longitude, null, stopInfo.get.name)
                        //println("New vertex: #" + stopOnLine.id + " " + stopInfo.get.name);
                        vertexes =  stopVertex :: vertexes
                     }
                     if(i == 0) {
                         prevVertex = stopVertex
                     }

                     if(i > 0) {
                            val distance = computeDistance(prevVertex.lat, prevVertex.longt, stopVertex.lat, stopVertex.longt)
                            val newEdge = new Edge(prevVertex, stopVertex, distance.toInt, l.id, l.name, l.direction )
                            val a = SchedulesManager.getSchedule(l.id, prevVertex.stopId, 1).getCountDepartures()
                            val b = SchedulesManager.getSchedule(l.id, stopVertex.stopId, 1).getCountDepartures()
                            val c = a + b
                            println("Pobrano " + c + " odjazdow linii " + l.id)
                            //println(prevVertex.stopId + " -> " + stopVertex.stopId + " " + distance + "m")
                            edges = newEdge :: edges
                       }
                       i = i+ 1;
                       prevVertex = stopVertex;

                   }
              }

         for(vertex <- vertexes) {
           var edgesFromVertex = List[Edge]()

           try {
             edgesFromVertex = edges.filter(e=> e.source.stopId == vertex.stopId).toList
           } catch {
             case ex : Exception =>
              println("z przystanku " + vertex.stopId + " nie ma odjazdow ZTM")
           }

           val stopsByWalk = stopRepo.getNearestStops(vertex.lat.toFloat, vertex.longt.toFloat)
           for(e<- stopsByWalk)  {

             if(!edgesFromVertex.exists(x => x.target.stopId == e.id) && e.id != vertex.stopId) {
               try {
               var routeToVertex = vertexes.find(x=>x.stopId == e.id).get
               val distance = computeDistance(vertex.lat, vertex.longt, routeToVertex.lat, routeToVertex.longt)
               val routeByWalk = new Edge(vertex, routeToVertex, distance.toInt, -1, "pieszo", "-" )
                if(distance < 900.0)
                  edgesFromVertex = routeByWalk :: edgesFromVertex
               } catch {  case ex : Exception =>
                 //println("brak węzła dla przystanku " + e.id )
               }
             }
           }
           vertex.setEdges(edgesFromVertex.toArray)
         }
       }
          println("Zbudowano graf z " + edges.length + " krawędzi.")
          new RoutingGraph(vertexes, edges)
     }

     def computeDistance(lat1 : Double, lon1 : Double, lat2 : Double, lon2 : Double) : Double = {
       val theta = lon1 - lon2;
       var dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
       dist = Math.acos(dist);
       dist = rad2deg(dist);
       dist = dist * 60 * 1.1515;
       dist = dist * 1609.344;
       return dist;
     }

  def deg2rad(deg : Double) : Double = {
    return (deg * Math.PI / 180.0);
  }

  def rad2deg( rad : Double) : Double = {
    return (rad * 180.0 / Math.PI);

  }
     def getRoutes(sourceName : String,
                   destName : String,
                   timeOfAnalyse : org.joda.time.DateTime,
                    typeOfSchedule : Int,
                    typeOfTransportation : Int,
                    metersByWalk : Int) : List [Vertex] = {
          val graph = getGraph();
          var path = List[Vertex]()
          controllers.Application.context.database.withSession { implicit session:Session =>
          val sourceStop = stopRepo.getSimilarStopByName(sourceName).id
          val destinationStop = stopRepo.getSimilarStopByName(destName).id

          path = FindRouteAlgorithms.Dijkstra(graph.getVertexByStopId(sourceStop), graph.getVertexByStopId(destinationStop),
            timeOfAnalyse, typeOfSchedule, typeOfTransportation, metersByWalk).toList

       }
        path
     }

  def prepareJson( list : List[Vertex], time : DateTime ) : String = {

    var avglat = 0.0
    var avglong = 0.0
    var i = 0

    for( v <- list) {
      avglat = avglat + v.lat;
      avglong = avglong  + v.longt
      i = i + 1
    }
    avglat = avglat/i
    avglong = avglong/i
    val formatter = DateTimeFormat.forPattern("dd-MM-yyyy-HH:mm");
    val obj = Json.obj(
      "alat" -> avglat.toString,
      "along" -> avglong.toString,
      "entries" -> list.map {
        entry => Json.obj(
          "time" -> formatter.print(time.plusMinutes(entry.minutesToThisVertex)),
          "lat" -> entry.lat,
          "line" -> entry.bestVertexToStop.lineName,
          "linedir" -> entry.bestVertexToStop.lineDirection,
          "long" -> entry.longt,
          "name" -> entry.name,
          "id" -> entry.stopId
        )
      })
    obj.toString
  }


}



class RoutingGraph ( val stops : List[Vertex], val routes : List[Edge]) {
      def getVertexByStopId( id : Int) : Vertex = {
        stops.find(x=>x.stopId == id).get
      }
}
