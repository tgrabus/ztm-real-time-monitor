package services

import java.net.URL
import io.Source
import java.io.FileNotFoundException
import models._
import collection.mutable._
import parser.{Point, Stop}

/**
 * User: Kuczi
 */
object StopsParser {
  
  def parseStops :List[Stop] = {
    val dataFolderPath = new java.io.File(".").getAbsolutePath.replace(""".""","""""") + "app" + java.io.File.separator + "data" + java.io.File.separator
    val stopDataFileName = "stops.csv"
    
    var stops = List[Stop]()
    var i = 0
    for(entry <- Helpers.getLinesFromFile(dataFolderPath + java.io.File.separator + stopDataFileName)) {
      i = i + 1;
      try {
        stops = stops :+ parseStop(entry)
        
      } catch {
        case iae:IllegalArgumentException => println("Invalid entry! Line: " + i + " " + entry)
      }
    }
    stops
  }
  
  
  @throws [IllegalArgumentException]
  def parseStop(entry:String) :Stop = {
    val elements = entry.split(';')
    try {
    	val id = elements(0).toInt
      var name = elements(1)
      if(name.contains(" I") && !(name== "Jana Pawła II" || name == "Sopot - Władysława IV" ||
        name == "Władysława IV" || name=="Leszczyńskich-Jana Pawła II" || id == 1502)) {
        val x = name.indexOf(" I")
        val todelete = name.substring(x, name.length)
        name = name.replace(todelete, "")
      }

      if(name.contains(" V")) {
        val x = name.indexOf(" V")
        val todelete = name.substring(x, name.length)
        name = name.replace(todelete, "")
      }

    	val long = elements(3).toDouble
    	val lat = elements(2).toDouble
    	val p = new Point(lat, long)
    	new Stop(id, name, p, null)
    } catch {
      case nfe:NumberFormatException => throw new IllegalArgumentException()
    }
  }

}