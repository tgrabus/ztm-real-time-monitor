package services;
import org.joda.time.DateTime;

import java.util.List;


public class FindRouteAlgorithms {
    public static List<Vertex> Dijkstra( Vertex source, Vertex destination, DateTime startTime,  int typeOfSchedule, int typeOfTransportation, int metersByWalk ) {
             List<Vertex> path = Dijkstra.searchPath(source, destination, startTime, typeOfSchedule, typeOfTransportation, metersByWalk );
             return path;
    }
}
