package services

import play.api.cache.Cache
import play.api.Play.current
import models.parser.ActualSchedule
import models.parser.ActualScheduleEntry
import play.api.libs.json.Json

object ActualScheduleManager {
  def getStopSchedule(id:Int) : ActualSchedule= {
    try {
	    val keyPart = "sas"
	    val schedule = Cache.getAs[ActualSchedule](keyPart + id)
	    val s = schedule.getOrElse(null)
	    if(s != null) {
	      s
	    } else {
	      val parsedSchedule = ActualScheduleParser.parse(id)
	      Cache.set(keyPart + id, parsedSchedule, 29) 
	      parsedSchedule
	    }
    } catch {
      case nfe:NumberFormatException => new ActualSchedule(List[ActualScheduleEntry]())
    }
  }
  
  def getStopSchedule(idString:String) :ActualSchedule = {
    try {
      val id = idString.toInt
      getStopSchedule(id)
    } catch {
      case nfe:NumberFormatException => new ActualSchedule(List[ActualScheduleEntry]())
    }
  }
}