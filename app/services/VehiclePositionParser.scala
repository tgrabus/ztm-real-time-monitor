package services

import collection.mutable.ArrayBuffer
import models.parser._
import collection.mutable

/**
 * User: Kuczi
 */
object VehiclePositionParser {
  def loadVehiclePositions :List[ActualVehiclePosition] = {
    val positions = new ArrayBuffer[models.parser.ActualVehiclePosition]()
    for(line <- Helpers.getLinesFromFile(new java.io.File(".").getAbsolutePath.replace(""".""","""""") + "app" + java.io.File.separator + "data" + java.io.File.separator + "vehiclePositions.csv")) {
      val elements = line.replace("\r", "").split(";")
      val lineName = elements(2)
      val direction = elements(6)
      val vehicleNumber = elements(1)
      val long = elements(4).toDouble
      val lat = elements(5).toDouble
      val timestamp = elements(0)
      val point = new models.parser.Point(lat, long)

      positions += new ActualVehiclePosition(timestamp,vehicleNumber,lineName + ";" + direction, point)
    }
    positions.toList
  }
}
