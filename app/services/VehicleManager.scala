package services

import models._
import parser._
import play.api.libs.json._
import repository.{PointRepository, TransportRepostory, LineRepository, VehiclePositionsRepository}
import scala.collection.mutable.ArrayBuffer
import java.lang.IllegalArgumentException
import slick.session.Database.threadLocalSession
import java.util.NoSuchElementException
import scala.collection.parallel._
import play.api.cache.Cache
import play.api.Play.current
import org.joda.time.DateTime
import java.util.Calendar
import org.joda.time.DateTimeConstants

/**
 * User: Kuczi
 */
object VehicleManager {
  def getVehiclesOnLine(name: String) = {
    var vehicles = ArrayBuffer[Transport]()
    val lineNameAndDirection = name.split(":")
    if (lineNameAndDirection.length != 2) {
      vehicles
    } else {
	    val lineRepo = new LineRepository
	    val transRepo = new TransportRepostory
	    val pointsRepo = new PointRepository
	    val vehRepo = new VehiclePositionsRepository
	
	    controllers.Application.context.database.withSession {
	      val line = lineRepo.getByNameAndDirection(lineNameAndDirection(0).trim, lineNameAndDirection(1).trim)
	      val rows = vehRepo.getVehiclesOnLineById(line.id)
	      for (row <- rows) {
	        val point = pointsRepo.getById(row.pointId).get
	        val transport = transRepo.getById(row.vehicleId).get
	        LineInserter.getLineType(line.name) match {
	          case LineInserter.TramType => vehicles += new Tram(transport.id, new Line(line.id, line.name, line.direction,List(),List()), new Point(point.latitude,point.longitude))
	          case _ => vehicles += new Bus(transport.id, new Line(line.id, line.name, line.direction,List(),List()), new Point(point.latitude,point.longitude))
	        }
	      }
	    }
	
	    vehicles
    }
  }

  def vehiclesToJson(vehicles: ArrayBuffer[Transport]) = {
    var json = ""
    var i = 0
    for (v <- vehicles) {
      i = i + 1
      json += Json.obj(
        "id" -> v.id,
        "linia" -> v.associatedLine.name,
        "kierunek" -> v.associatedLine.direction,
        "pojazd" -> v.getClass().getSimpleName,
        "pozycja" -> Json.obj(
          "dlugosc" -> v.position.longtitude,
          "szerokosc" -> v.position.latitude
        )).toString()
      if (i < vehicles.length) {
        json += ",\n"
      }
    }
    "[" + json + "]"
  }

  def getPositionOfAllVehicles() = {
    var vehicles = ArrayBuffer[Transport]()

    val lineRepo = new LineRepository
    val transRepo = new TransportRepostory
    val pointsRepo = new PointRepository
    val vehRepo = new VehiclePositionsRepository

    controllers.Application.context.database.withSession {
      val lines = lineRepo.getAll.toList
      val rows = vehRepo.getAllVehiclesOnLines
      for (row <- rows) {
        val point = pointsRepo.getById(row.pointId).get
        val transport = transRepo.getById(row.vehicleId).get
        val line = lines.find(p => p.id == row.lineId).get
        LineInserter.getLineType(line.name) match {
          case LineInserter.TramType => vehicles += new Tram(transport.id, new Line(line.id, line.name, line.direction,List(),List()), new Point(point.latitude,point.longitude))
          case _ => vehicles += new Bus(transport.id, new Line(line.id, line.name, line.direction,List(),List()), new Point(point.latitude,point.longitude))
        }
      }
    }

    vehicles
  }
  
  def estimateVehiclePositionsOnLine(lineName:String) = {
    val cacheKeyFragment = "vehPos"
    
    var v = Cache.getAs[List[Transport]](cacheKeyFragment + lineName)
    var vehicles = v.getOrElse(null)
    if(null == vehicles) {
    	vehicles = List[Transport]() 
    
	    val el = lineName.split(":")
	    if(el.length > 1) {
	      val name = el(0).trim()
	      val direction = el(1).trim()
	      val line = LineManager.getLine(lineName)
	      try {
	    	  val ase = getFilteredActualSchedules(name, direction, line)  	  
	    	  
	    	  var i = 0
	    	  var lastAddedIteration = -2
	    	  for(entry <- ase) {
	    	    if(null != entry && i > 0 && Math.abs(i - lastAddedIteration) > 2){
		    	    val timeLeft = entry.timeToArriveInMinutes
		    	    val usualTime = calculateDistanceBetweenStopsInMinutes(line, line.stops(i), line.stops(i - 1))
		    	    if(timeLeft <= usualTime && i > 0) {
		    	      val p = calculatePosition(line.stops(i).position, line.stops(i - 1).position, timeLeft, usualTime)
		    	      LineInserter.getLineType(name) match {
			    	      case LineInserter.TramType => {
			    	         vehicles = vehicles :+ new Tram(1, line, p)
			    	      }
			    	      case _ => {
			    	        vehicles = vehicles :+ new Bus(1, line, p)
			    	      }
		    	      }
		    	      lastAddedIteration = i
		    	    }
	    	    }
	    	    i = i + 1
	    	  }	  
	      }catch {
	        case iae:IllegalArgumentException => {
	          iae.printStackTrace()
	          println("Brak linii " + line)
	        }
	      }
	    }
    }
    vehicles
  }    
  
  private def calculatePosition(current:Point, predecessor:Point, timeLeft:Int, usualTime:Int) :Point= {
    val latDif = current.lat - predecessor.lat
    val longDiff = current.long - predecessor.long
    val f = timeLeft.toDouble / usualTime.toDouble
    
    new Point(current.longtitude - f * longDiff, current.latitude - f * latDif)
  }
  
  private def getStopIds(stops:List[Stop]) :List[Int] = {
    var l = List[Int]()
    for(s <- stops) {
      l = l :+ s.id
    }
    l
  }
  
  private def getFilteredActualSchedules(name:String, direction:String, line:Line) = {
    var ids = scala.collection.parallel.mutable.ParMap[Int, ActualScheduleEntry]()
    val schedules = scala.collection.parallel.mutable.ParArray[ActualSchedule]()
    val abc = getStopIds(line.stops)
    for(i <- 0 to line.stops.length - 1) {
      ids.put(abc(i), null) 
    }
    ids.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(ids.size))
    ids.map(k => ids.put(k._1,  getFirstEntryForLine(name, direction, ActualScheduleManager.getStopSchedule(k._1))))
      
    val entries = ArrayBuffer[ActualScheduleEntry]()
    
    for(stop <- line.stops) {
      entries.append(ids.get(stop.id).getOrElse(null))
    }
    entries
  }
  
  private def calculateDistanceBetweenStopsInMinutes(line:Line, current:Stop, predecessor:Stop) :Int = {
    val DEFAULT_DISTANCE = 2
    
    var css = SchedulesManager.getSchedule(line.id, current.id, determineDayType())
    var pss = SchedulesManager.getSchedule(line.id, predecessor.id, determineDayType())

    if(null == pss || null == css) {
      println("Schedules not found")
      DEFAULT_DISTANCE
    } else {
      val closest = css.findClosestDeparture(determineDayType())
      if(null == closest) {
        println("Could'n found closest departure")
        DEFAULT_DISTANCE
      } else {
        val ealier = pss.findEalierDeparture(determineDayType(), closest.time)
        if(null != ealier) {
          Departure.differenceInMinutes(closest, ealier)
        } else {
          println("Could'n found ealier departure")
          DEFAULT_DISTANCE
        }
      }
    }
  }
  
  private def filterSchedulesByType(schedules:List[Schedule], t:Int) = {
    var result = List[Schedule]()
    for(s <- schedules) {
      if(s.scheduleType == t) {
        result = result :+ s
      }
    }
    result
  }
  
  
  private def determineDayType(date:DateTime = DateTime.now) = {
    if(DateTimeConstants.SUNDAY == date.getDayOfWeek()) {
      models.parser.Schedule.HOLIDAY_SCHEDULE
    } else if(DateTimeConstants.SATURDAY == date.getDayOfWeek()) {
      models.parser.Schedule.SATURDAY_SCHEDULE
    } else {
      models.parser.Schedule.NORMAL_SCHEDULE
    }
    
  }
 
  private def getFirstEntryForLine(name:String, direction:String, schedule:ActualSchedule) :ActualScheduleEntry = {
    for(entry <- schedule.entries) {
      if(entry.lineName == name && entry.direction == direction) {
        return entry
      }
    }
    null
  }
}
