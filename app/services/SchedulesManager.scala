package services

import models.repository._
import scala.slick.session.Session
import play.api.libs.json.Json
import play.api.libs.json.JsObject
import play.api.libs.json.JsArray
import play.api.cache.Cache
import play.api.Play.current
import scala.collection.mutable.HashMap
import org.joda.time.{Period, PeriodType, DateTime}
import org.joda.time.format.DateTimeFormat
import org.joda.time
;

/**
 *  Project
 * User: Mateusz
 * Date: 05.12.13
 * Time: 21:27
 */
object SchedulesManager {

  val lineStopRepo = new LineStopRepository
  val scheduleRepo = new ScheduleRepository
  val departureRepo = new DepartureRepository

  def getScheduleForLineAndStop(lineId:String, stopId : String) : String = {
    try {
      val lId = lineId.toInt
      var stopSchedules:JsArray = JsArray()
      controllers.Application.context.database.withSession { implicit session:Session =>
        val stopOnLine = lineStopRepo.getByLineIdAndStopId(lId, stopId.toInt)
          val schedules = scheduleRepo.getByLineStopId(stopOnLine.id)
          val formatter_hour = DateTimeFormat.forPattern("H");
          val formatter_minute = DateTimeFormat.forPattern("mm");
          var s:JsArray = JsArray()
          for(schedule <- schedules) {
            val departures = departureRepo.getByScheduleId(schedule.id)
            var entries:JsArray = JsArray()
            for(departure <- departures) {
              val entry = Json.obj("godzina" -> formatter_hour.print(departure.departureTime.getTime),
                "minuta" -> formatter_minute.print(departure.departureTime.getTime),
                "info" -> departure.departureInfo)
                entries = entries.append(entry)
            }
            s = s.append(Json.obj("typ" -> schedule.scheduleType, "odjazdy" -> entries))
          }
          stopSchedules = stopSchedules.append(Json.obj("stopid" -> stopOnLine.stopId, "rozklady" -> s))
      }
      stopSchedules.toString
    } catch {
      case nfe:NumberFormatException => "[]"
    }
  }
  
   def getSchedule(lineId:Int, stopId:Int, scheduleType:Int) = {
    val cacheKeyPart = "schedules"
    val key = lineId.toString + cacheKeyPart + stopId.toString + ":" + scheduleType
    val res = Cache.getAs[models.parser.Schedule](key)
    val s = res.getOrElse(null)
     val now = new DateTime();
    if(null == s) {
	    controllers.Application.context.database.withSession{implicit session:Session =>
	    	val lineStop = lineStopRepo.getByLineIdAndStopId(lineId, stopId)
	    	val rawSchedules = scheduleRepo.getByLineStopIdAndType(lineStop.id, scheduleType)
	    	val departuresMap = HashMap[Int, List[models.parser.Departure]]()
	    	for(entry <- rawSchedules) {
	    	  val dep = departureRepo.getByScheduleId(entry.id)
	    	  for(d <- dep) {
            val date =  new DateTime(d.departureTime);
            val newDate = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(), date.hourOfDay().get(), date.minuteOfHour().get());
	    	    val newDeparture = new models.parser.Departure(new DateTime(newDate), d.departureInfo)
	    	    var l = departuresMap.get(newDeparture.time.getHourOfDay()).orNull
	    	    if(null == l) {
	    	      l = List[models.parser.Departure]()
	    	    }
	    	    l = l :+ newDeparture
	    	    departuresMap.put(newDeparture.time.getHourOfDay(), l)
	    	  }	    	  	    	  
	    	}
	    	if(rawSchedules.length > 0) {
	    	  val from = new DateTime(rawSchedules(0).activeFrom)
	    	  val to = new DateTime(rawSchedules(0).activeTo)
	    	  val schedule = new models.parser.Schedule(departuresMap, from, to, rawSchedules(0).onDemand,  rawSchedules(0).scheduleType)
	    	  Cache.set(key, schedule, 60 * 60 * 6)
	    	  schedule
	    	} else {
	    	  new models.parser.Schedule(HashMap[Int, List[models.parser.Departure]](), DateTime.now(), DateTime.now, "f", 1)
	    	}
	    }
    } else {
      s
    }
  }
  def getTimesBetweenStops(stopAid : Int, stopBid : Int, typeOfSchedule : Int, lineId : Int, timeAfter : DateTime) : Int = {
    val timesFromA = getSchedule(lineId, stopAid, typeOfSchedule)
    val timesFromB = getSchedule(lineId, stopBid, typeOfSchedule)

    val firstTime = timesFromA.getFirstDepartureFrom(timeAfter)
    val secondTime = timesFromB.getFirstDepartureFrom(firstTime)
    val idle = new Period(timeAfter, firstTime, PeriodType.minutes()).getMinutes
    val travel = new Period(firstTime, secondTime, PeriodType.minutes()).getMinutes
    idle + travel
  }
  def getScheduleAsListOfDepartures(lineId:Int, stopId:Int, schedType : Int, timeAfter : org.joda.time.DateTime) : List[models.parser.Departure] = {
      var departures = List[models.parser.Departure]()
      controllers.Application.context.database.withSession{implicit session:Session =>
        val lineStop = lineStopRepo.getByLineIdAndStopId(lineId, stopId)
        val rawSchedules = scheduleRepo.getByLineStopId(lineStop.id)
        var now = new DateTime();
        for(entry <- rawSchedules) {
          if(entry.scheduleType == schedType)     {
          val dep = departureRepo.getByScheduleId(entry.id)
            for(d <- dep) {
              val date =  new DateTime(d.departureTime);
              val newDate = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(), date.hourOfDay().get(), date.minuteOfHour().get());
              departures = departures :+ new models.parser.Departure(newDate, d.departureInfo)
            }
          }
        }
        departures.filter( x => x.time.isAfter(timeAfter) && x.time.isBefore(timeAfter.plusHours(2))   )
      }
    }

  def getScheduleAsListOfDepartureSaAfterTimeq(lineId:Int, stopId:Int, scheduleType : Int, timeAfter : org.joda.time.DateTime) : org.joda.time.DateTime = {
    var departures = List[models.parser.Departure]()
    controllers.Application.context.database.withSession{implicit session:Session =>
      val lineStop = lineStopRepo.getByLineIdAndStopId(lineId, stopId)
      val rawSchedules = scheduleRepo.getByLineStopId(lineStop.id)
      val now = new DateTime();
      for(entry <- rawSchedules) {
        if(entry.scheduleType == scheduleType)     {
          val dep = departureRepo.getByScheduleId(entry.id)
          for(d <- dep) {
            val date =  new DateTime(d.departureTime);
            val newDate = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(), date.hourOfDay().get(), date.minuteOfHour().get());
            departures = departures :+ new models.parser.Departure(newDate, d.departureInfo)
          }
        }
      }
    try {
      departures.filter( x => x.time.isAfter(timeAfter) && x.time.isBefore(timeAfter.plusHours(4))).head.time
    }  catch {
      case ex : Exception =>
        new time.DateTime()
    }

    }
  }

}
