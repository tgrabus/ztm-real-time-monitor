package services

import models._
import models.parser.{ScheduleEntry, Stop, Line}
import xml._
import collection.mutable.{ArrayBuffer, HashMap}
import org.apache.commons.logging.impl.Log4JLogger
import org.apache.commons.logging.Log
import org.jboss.netty.logging.Log4JLoggerFactory
import scalax.io.Resource

/**
 * Created with IntelliJ IDEA.
 * User: Tomasz
 * Date: 12.04.13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */


object LinesParser {

  def getLines : List[Line] = {
    val content = Helpers.loadSource("http://www.ztm.gda.pl/rozklady/")
    var snippet = Helpers.getSnippet(content.toString(), "<div id=\"content\">", "<div id=\"footer\">")
    snippet = Helpers.removeMarkup(snippet, Array("&nbsp", "&raquo", "<br>", "&#\\d{1,4}[^;]"))
    snippet = snippet.replaceAll("</P", "</p>") //bledy ztm
    snippet = snippet.replaceAll("<b><i>Latarnia Morska</b><i>", "")
    val l = snippet.split("<tr>|</tr>");
    var lines: List[Line] = List[Line]()
    val out = Resource.fromFile("bledy.out")
    for(s <- l) {

      val start = s.indexOf("<a")
      val end = s.indexOf("</a>")
      if(start >= 0 && end >= 0) {
        val rawLink = s.substring(start, end) + "</a>"
        try {
          val a = scala.xml.XML.loadString(rawLink)

          val link = a \ "@href"
          val absoluteAddress = "http://www.ztm.gda.pl/rozklady/"
          
          val src = Helpers.loadSource(absoluteAddress + link)
          val directions = getLineDirections(src)
          val stops = LineStopsParser.getAssociatedLineStops(src)

          try {
	          val line1 = new Line(0, a.text, directions(0), stops(0), List())
	          val line2 = new Line(0, a.text, directions(1), stops(1), List())
	
	          lines = lines :+ line1 :+ line2
          } catch {
            case iob:IndexOutOfBoundsException => {           
              lines = lines :+ new Line(0, a.text, directions(0), stops(0), List())
            }
          }

        } catch {
          case sp : SAXParseException => out.write("Cannot parse:\n" + rawLink + "\n")
        }
      }
    }

    addWaypointsToLines(lines)
    lines
  }

  def extractLink(line:String) :String = {
    val href = "<a href="
    var start = line.indexOf(href + "\"start-")
    if(start > 0 ) {
      //line contains link to page
      //omit html tag and "
      start = start + href.length + 1
      val end = line.indexOf("\"", start)
      line.substring(start, end)
    } else {
      ""
    }
  }

  def extractLineName(line:String):String = {
    val startTag = "<a href="
    val endTag = "</a>"
    var start = line.indexOf(startTag)
    val end = line.indexOf(endTag, start)
    start = line.indexOf(">", start)
    val lineName = line.substring(start + 1, end)
    lineName.replaceAll("<b>|</b>", "")
  }
  
  def getLineDirections(html:String) :List[String] = {
    val stargingTag = "Kierunek:"
    val endingTag = "</b>"
    var start = 0;
    var end = 0;
    var results = List[String]()
    do{
      start = html.indexOf(stargingTag, start + stargingTag.length())
      if(start >= 0) {
    	  end = html.indexOf(endingTag, start)
    	  if(end >= 0) {
    	    val rawName = html.substring(start + stargingTag.length(), end) + " "
    	    results = results :+ Helpers.removeMarkup(rawName.trim(), Array[String]("<b>"))
    	  }
        }      
    }while(start >= 0)
    results
  }

val dataFolderPath = new java.io.File(".").getAbsolutePath.replace(""".""","""""") + "app" + java.io.File.separator + "data" + java.io.File.separator

  def addWaypointsToLines(lines:List[models.parser.Line]) = {
    val data = Helpers.getLinesFromFile(dataFolderPath + "Waypoints.csv")
    val map = new HashMap[String, ArrayBuffer[models.parser.Point]]()
    for (row <- data) {
      val elements = row.split(";")

      val lineName = elements(0)
      val direction = elements(1)
      val lat = elements(2).toFloat
      val long = elements(3).toFloat

      val point = new models.parser.Point(lat, long)
      val key = lineName + "-" + direction
      if(map.contains(key)) {
        map(key) += point
      } else {
        map(key) = new ArrayBuffer[models.parser.Point](){point}
      }
    }

    for(line <- lines) {
      val key = line.name + "-" + line.direction
      if(map.contains(key)) {
        line.waypoints = map(key).toList
      }
    }
    lines
  }
}

object LineStopsParser  {

	def getAssociatedLineStops(htmlSrc: String) : Array[List[Stop]] = {
	try {
		var snippet = Helpers.getSnippet(htmlSrc, "<table width=\"480\" align=\"center\">", "<div id=\"departures-footer\">")
		
		// fragment moze nie zawierac korzenia wiec trzeba go dodac
		snippet = Helpers.addRootElement(snippet)
		snippet = Helpers.removeMarkup(snippet, Array("&nbsp"))
		val root = scala.xml.XML.loadString(snippet) \ "table"
		val tables = root \ "tr" \\ "table"
		
		val firstDirectionStops = getLineStops(tables.head)
		val secondDirectionStops = getLineStops(tables.last)
		
		Array(firstDirectionStops, secondDirectionStops)
	} catch {
	  case s:StringIndexOutOfBoundsException => {
	    println(htmlSrc)
	    Array[List[Stop]]()
	  }
	}
	}

	private def getLineStops(table: Node): List[Stop] = {
			var str:String = ""
					var stops : List[Stop] = Nil
        var schedule = new Array[ScheduleEntry](3);
					table match {
					case table @ <table>{rows @ _*}</table> if (table \ "@class" text) == "route" => {
						for (row <- rows) {
							row match {
							case <tr>{columns @ _*}</tr> if columns.head.label != "th" => {
								var stopName:String = ""
								val a = columns.head \\ "a"
								var stopId:Int = 0
	
								if (a.length != 0){
									val link = a \ "@href"
									stopName = a.text
									stopId = getStopId((a \ "@href").text)
                  try {
                    schedule = getStopSchedule("http://www.ztm.gda.pl/rozklady/" + link, stopName);
                    println("Stop : " + stopName + ". Successfully parsed. Address: "  + link)
                  }
                  catch {
                    case e: Exception => {
                      println("Error ocurred, during processing stop: " + stopName + ". Address: "  + link)
                    }
                  }
									stops = stops :+ new Stop(stopId, stopName.replaceAll(";", ""), null, schedule);
								}
								else {
								  stopName = columns.head.text
								  stops = stops :+ new Stop(-1, stopName.replaceAll(";", ""), null, null);
								}
							}
							case _ => str = "niedziala"
							}
						}
					}
	}

	stops.drop(1)
	}


	def extractStops(link:String) = {
		val content = Helpers.loadSource("http://www.ztm.gda.pl/rozklady/" + link)
				var snippet = Helpers.getSnippet(content.toString(), "<div id=\"departures\">", "<div id=\"footer\">")
	}
	
	def getStopId(link:String) :Int= {
	  val source = Helpers.loadSource("http://www.ztm.gda.pl/rozklady/" + link)
	  try {
		  extractStopId(source)
	  } catch{
	    case iae: IllegalArgumentException => {
	      println("Stop at addres " + link + " cannot be parsed")
	      -1
	    }
	  }
	}
	
	@throws [IllegalArgumentException]
	def extractStopId(html:String) : Int = {
	  val start = "<span title=\"Numer s"
	  val startIdx = html.indexOf(start)
	  if(startIdx >= 0) {
	    val endIdx = html.indexOf("</span>", startIdx)
	    val element = html.substring(startIdx + "<span title=\"Numer slupka\">".length, endIdx)
	    val xml = scala.xml.XML.loadString(element)
	    val strId = xml.text
	    try {
	    	val id =  strId.toInt
	    	id
	    } catch {
	      case nfe : NumberFormatException => {
	        val el = strId.split(',')
	        if(el.length == 2) {
	          el(1).toInt
	        } else {
	        	throw new IllegalArgumentException
	        }
	      }
	    }
	  } else {
	    throw new IllegalArgumentException
	  }
	}


  private def getStopSchedule(link: String, stopName: String): Array[ScheduleEntry] = {
    val content = Helpers.loadSource(link)
    var stop_header = Helpers.getSnippet(content.toString(), "<div id=\"departures-header\">", "<div id=\"SIP\">");
    var ztmid = Helpers.getSnippet(stop_header, "<i>", "</i>");
    ztmid = ztmid.replace("<i>", "");

    var snippet = Helpers.getSnippet(content.toString(), "<div class=\"departures-set\">", "<br class=\"clear\" />")
    snippet = Helpers.addRootElement(snippet)
    snippet = Helpers.removeMarkup(snippet, Array("&nbsp", "&raquo", "<br>"))
    var x = snippet
    var str: String = ""
    var stops: List[Stop] = Nil
    var result = new Array[ScheduleEntry](3)

    result(0) = new ScheduleEntry(stopName, "DniPowszednie")
    result(1) = new ScheduleEntry(stopName, "Soboty")
    result(2) = new ScheduleEntry(stopName, "NiedzieleSwieta")

    var xml = scala.xml.XML.loadString(snippet)

    var schedulesTypes = new Array[Node](3)
    var i: Int = 0

    (xml \ "div").foreach {
      div =>
        schedulesTypes(i) = div
        i += 1
    }
    i = 0
    var lastHour: String = ""
    for (node <- schedulesTypes) {

      if (node != null && (node \ "dif") != null) {
        (node \ "div").foreach {
          div =>
            if ((div \ "@class") != null && (div \ "@class").text == "h") {
              lastHour = div.text
            }
            if ((div \ "@class") != null && (div \ "@class").text == "m") {
              result(i).AddValue(lastHour.toInt, div.text)
            }
        }
      }
      i += 1
    }
    result
  }


}
