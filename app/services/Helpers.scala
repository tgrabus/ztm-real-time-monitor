package services

import java.net.URL
import io.{Codec, Source}
import java.nio.charset.CodingErrorAction
import java.io.FileNotFoundException
import java.lang.IllegalArgumentException
import scala.IllegalArgumentException

/**
 * Created with IntelliJ IDEA.
 * User: Tomasz
 * Date: 18.04.13
 * Time: 17:17
 * To change this template use File | Settings | File Templates.
 */


object Helpers {

  def removeMarkup(content:String, markups: Array[String]): String = {
    var result:String = ""

    if(content != null){
      val regex = markups.mkString("|")
      result = content.replaceAll(regex, "")
    }
    result
  }

  def getSnippet(content:String, start:String, end:String) : String = {
    var result:String = ""

    if(content != null)  {
      val idxf = content.indexOf(start)
      val idxe = content.indexOf(end)
      result = content.substring(idxf, idxe)
    }
    result
  }

  def loadSource(link: String) :String = {
    var content = new StringBuilder

    if (link != null) {
      val stopsPage = new URL(link)
      val source = Source.fromURL(stopsPage, "iso-8859-2")
      source.getLines().foreach(line => content.append(line))
    }

    content toString()
  }

  def addRootElement(snippet: String): String = {
    val root = "<root>".concat(snippet).concat("</root>")
    root
  }


  def getLinesFromFile(path: String) : List[String] = {
    try {
      implicit val codec = Codec("UTF-8")
      codec.onMalformedInput(CodingErrorAction.REPLACE);
      codec.onUnmappableCharacter(CodingErrorAction.REPLACE);
      val src = scala.io.Source.fromFile(path)
      val content = src.mkString
      src.close()
      content.split("\n").toList
    }
    catch {
      case exception:FileNotFoundException => List[String]()
    }
  }

  def indexOfAll(seed:String, text:String):List[Int] =  {
    var result = List[Int]()
    var start = 0
    var offset = text.indexOf(seed, start)
    while (-1 != offset) {
      result = result :+ offset
      start = offset + seed.length
      offset = text.indexOf(seed, start)
    }
    result
  }

  @throws[IllegalArgumentException]
  def extractTable(index:Int, code:String):String = {
    val tableStart = "<table"
    val tableEnd = "</table>"
    val beginnings = Helpers.indexOfAll(tableStart, code)
    val ends = Helpers.indexOfAll(tableEnd, code)
    if(beginnings.length != ends.length) {
      throw new IllegalArgumentException("Code does not contain same number of table opening and closing tags")
    }
    if(index > beginnings.length) {
      //throw new IllegalArgumentException
    }

    var result = ""
    val start = beginnings(index - 1)

    var end = 0

    //find first closing tag
    for(index <- ends) {
      if(index > start && 0 == end) {
        end = index;
      }
    }
    if(beginnings.length > index) {
      //we need to check if next table is inside current table
      if (beginnings(index) < end) {
        //we have jagged array(s)
        var offset = 0
        while(index + offset < beginnings.length && beginnings(index + offset) < ends(index + offset)) {
          offset = offset + 1
        }

        end = ends(ends.indexOf(end) + offset)
      }
      result = code.substring(start,end + tableEnd.length)
    } else {
      result = code.substring(start,end + tableEnd.length)
    }
    result
  }
}
