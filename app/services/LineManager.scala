package services

import collection.mutable.ArrayBuffer
import java.nio.charset.CodingErrorAction
import scala.io.Codec
import collection.mutable
import play.api.libs.json.Json
import models.parser.{Point, Stop, Line}
import models.repository._
import models.schema.Stops
import java.lang.IllegalArgumentException
import models.ZTMContext


object LineManager {
	 import scala.slick.session.{Database, Session}

  @throws[IllegalArgumentException]
  def getLine(name: String):models.parser.Line = {
    val nameAndDirection = name.split(":")
    if(nameAndDirection.length != 2) {
      throw new IllegalArgumentException("Wrong line name")
    }

    val lineRepo = new LineRepository
    val stopRepo = new StopRepository
    val pointRepo = new PointRepository
    val lsRepo = new LineStopRepository
    val lpRepo = new LinePointRepository

    controllers.Application.context.database.withSession{implicit session:Session =>
      val l = lineRepo.getByNameAndDirection(nameAndDirection(0).trim, nameAndDirection(1).trim)

      var stops = List[models.parser.Stop]()
      var waypoints = List[models.parser.Point]()

      for(s <- lsRepo.getByLineId(l.id).toList) {
        val stop = stopRepo.getById(s.stopId).get
        val point = pointRepo.getById(stop.pointId).get
        
        waypoints = waypoints :+ new models.parser.Point(point.latitude, point.longitude)

        val newstop = new models.parser.Stop(stop.id,stop.name, new models.parser.Point(point.latitude, point.longitude), null)
        stops = stops :+ newstop
      }

      new models.parser.Line(l.id, l.name, l.direction, stops, waypoints)
    }
  }

  def getAllLines = {
    val lines = new mutable.HashMap[Int, Line]()

    controllers.Application.context.database.withSession{implicit session:Session =>
      val lpRepo = new LinePointRepository
      val pointsRepo = new PointRepository
      var lineRepo = new LineRepository

      for(line <- lineRepo.getAll.toList) {
        lines(line.id) = new Line(line.id, line.name, line.direction, List[Stop](), List[Point]())
      }

      for(entry <- lpRepo.getAll.toList) {
        val point = pointsRepo.getById(entry.pointId).get
        lines(entry.lineId).waypoints = lines(entry.lineId).waypoints :+ new Point(point.latitude, point.longitude)
      }
    }
    var linesWithWaypoints = List[Line]()

    for (l <- lines.values) {
      if(l.waypoints.length > 0) {
        linesWithWaypoints = linesWithWaypoints :+  l
      }
    }

    linesWithWaypoints
  }

  def getLineNamesAsLabelIdPairs = {
    var hits = "["
    var lines = List[models.schema.Line]()
    controllers.Application.context.database withSession{implicit session:Session =>
      val lineRepo = new LineRepository()
      lines = lineRepo.getAll
    }

    for (line <- lines) {
      hits = hits + convertLineToLabelValuePairs(line) + ","
    }
    if (hits.length > 0 && hits.endsWith(",")) {
      hits.dropRight(1) + "]"
    } else {
      hits + "]"
    }
  }

  def convertLineToLabelValuePairs(line: models.schema.Line) = {
    Json.toJson(Map("label" -> Json.toJson(line.name + " - " + line.direction), "value" -> Json.toJson(line.id)))
  }

  def getLineNames = {
    var lines = List[models.schema.Line]()
    controllers.Application.context.database withSession{ implicit session:Session =>
      val lineRepo = new LineRepository()
      lines = lineRepo.getAll
    }
    val lineNames = new mutable.HashSet[String]()
    for (line <- lines) {
      lineNames.add(line.name + " : " + line.direction)
    }
    Json.toJson(lineNames.toList)
  }

  def getLineNamesByPartOfName(name:String) = {
    var lines = List[models.schema.Line]()
    controllers.Application.context.database withSession{ implicit session:Session =>
      val lineRepo = new LineRepository()
      lines = lineRepo.getByName(name)
    }
    val lineNames = new mutable.HashSet[String]()
    for (line <- lines) {
      lineNames.add(line.name + " : " + line.direction)
    }
    Json.toJson(lineNames.toList)
  }

  def getLines = {
    var lines = List[models.schema.Line]()

    val context = new ZTMContext

    context.database withSession{ implicit session:Session =>
      val lineRepo = new LineRepository()
      lines = lineRepo.getDistinctLines
    }

    val obj = Json.obj("lines" -> lines.map {
      line => Json.obj(
      "name" -> line.name,
      "kind" -> line.lineKind
      )
    })

    Json.toJson(obj)
  }

  def getDirectionsForLine(lineName : String) = {
    var directions = List[String]()

    val context = new ZTMContext

    context.database withSession{ implicit session:Session =>
      val lineRepo = new LineRepository()
      directions = lineRepo.getDirectionsForLine(lineName)
    }

    val obj = Json.obj("directions" -> directions.map {
      direction => direction
    })

    Json.toJson(obj)
  }

  def getLineStopsForDirection(lineName : String, direction:String) = {
    var stops = List[models.schema.Stop]()
    val context = new ZTMContext

    context.database withSession{ implicit session:Session =>
      val lineRepo = new LineRepository()
      stops = lineRepo.getLineStopsForDirection(lineName, direction)
    }

    val obj = Json.obj("stops" -> stops.map {
      stop => Json.obj(
      "id" -> stop.id,
      "name" -> stop.name
      )
    })

    Json.toJson(obj)
  }

  /*def getActualDeparturesFromStop(id : String) = {
    var acts = List[models.schema.ActualDeparture]()
    controllers.Application.context.database withSession{ implicit session:Session =>
      val actRepo = new ActualDepartureRepository

      val lineRepo = new LineRepository()
      acts = actRepo.getActualFromStop(Integer.parseInt(id))

   val obj = Json.obj(
     "id" -> id,
     "lines" -> acts.map {
       w => Json.obj(
         "timeLeft" -> w.departureTime.toString,
         "now" -> w.nowArriving,
         "direction" -> w.direction,
         "line" -> w.lineName
       )
     }
   )

    val json = Json.toJson(obj)
    json
    }
  }*/
}