package services

import play.api.libs.json.Json
import play.api.libs.json.JsArray
import models.parser._

object ActualScheduleParser {

	def parse(id : Int) :ActualSchedule= {
		var entries = List[ActualScheduleEntry]()
		val address = s"http://www.ztm.gda.pl/rozklady/pobierz_SIP.php?n[0]=$id&t=&l=5"
		val src = Helpers.loadSource(address)
		val rows = src.split("</tr>")
		for(row <- rows) {
			if(row.startsWith("<tr><td>")) {
				val data = Helpers.removeMarkup(row, Array("<tr>", "<td>", "&nbsp", "</span>", "<span class=\"juz\">"))
				val elements:Array[String] = data.split("</td>")
				val lineName = elements(0)
				val direction = elements(1)
											
				if(3 == elements.length) {
				  if(";" == elements(2).trim()) {
				    entries = entries :+ new ActualScheduleEntry(lineName, direction, "teraz")
				  } else {
					entries = entries :+ new ActualScheduleEntry(lineName, direction, elements(2))
				  }
				} else if(2 == elements.length) {
				  entries = entries :+ new ActualScheduleEntry(elements(0), elements(1), "teraz")
				}
			}
		}
		new ActualSchedule(entries)
	}
	
	def parse(id : String) :ActualSchedule= {
		try {
			val stopId = id.toInt 
			parse(stopId)
		}
		catch {
		  case nfe : NumberFormatException => new ActualSchedule(List[ActualScheduleEntry]())
		}
	}
  def parseObjects(id : String) : List[ActualScheduleEntry] = {

      val stopId = id.toInt
      var entries = List[ActualScheduleEntry]();
      val address = s"http://www.ztm.gda.pl/rozklady/pobierz_SIP.php?n[0]=$id&t=&l=5"
      val src = Helpers.loadSource(address)
      val rows = src.split("</tr>")
      for(row <- rows) {
        if(row.startsWith("<tr><td>")) {
          val data = Helpers.removeMarkup(row, Array("<tr>", "<td>", "&nbsp", "</span>", "<span class=\"juz\">"))
          val elements:Array[String] = data.split("</td>")

          val entry = new ActualScheduleEntry(elements(0),elements(1),elements(2))
          if(3 == elements.length) {
           entries = entry :: entries
          } else if(2 == elements.length) {
            entries = entry :: entries
          }
        }
      }
      return entries
  }
}