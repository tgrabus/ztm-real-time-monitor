package services
import models._
import collection.mutable.ArrayBuffer
import parser.{Point, Stop, Line}
import play.api.libs.json.Json
import play.api.libs.json.JsObject
import play.api.libs.json.JsObject

/**
 * User: Kuczi
 */
object LineConverter {  
  def getLineAsJson(line : Line) =  {

    var avglat = 0.0;
    var avglong = 0.0;
    var i = 0;
    for(p <- line.waypoints){
      avglat = avglat + p.lat;
      avglong = avglong + p.long;
      i = i + 1;
    }
    avglat = avglat/i;
    avglong = avglong/i;
    val json = Json.toJson( Map( "lineid" -> Json.toJson(line.id.toString),
      "linedirection" -> Json.toJson(line.direction.toString),
      "linename" -> Json.toJson(line.name.toString),
      "avglat" -> Json.toJson(avglat.toString),
      "avglong" -> Json.toJson(avglong.toString),
      "path" -> Json.toJson(line.waypoints), "stops" -> Json.toJson(line.stops)))
    json
  }
}
