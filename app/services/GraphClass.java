package services;
//import scala.collection.JavaConversions._
import java.util.Collections.*;


import models.parser.Departure;
import org.joda.time.DateTime;
import scala.collection.immutable.List;

class Vertex implements Comparable<Vertex>
{
    public int stopId;
    private Edge[] listOfRoutes;
    public int minDistance = Integer.MAX_VALUE;
    public Vertex previous;
    public double lat;
    public double longt;
    public String linia;
    public Edge bestVertexToStop;
    public int minutesToThisVertex;
    public String name;
    public String toString() {
        return name;
    }
    public int compareTo(Vertex other)
    {
        return Integer.compare(minDistance, other.minDistance);
    }

    public Vertex(int stopid, double latitiude, double longtitude, Edge [] krawedzie, String stopName) {
        listOfRoutes = krawedzie;
        stopId = stopid;
        longt = longtitude;
        lat = latitiude;
        name = stopName;
    }


    public void setEdges(Edge[] list) {
        listOfRoutes = list;
    }
    public Edge[] getEdges(int transportationType) {
        //TODO uwzglednic typ transportu
        return listOfRoutes;
    }
}

class Edge
{
    public Vertex source;
    public int lineId;
    public int distance;
    public String lineName;
    public String lineDirection;
    public final Vertex target;
    public int weight;
    public Edge(Vertex source, Vertex argTarget, int argWeight, int lineId, String lineName, String lineDirection)
    {
        target = argTarget;
        weight = argWeight;
        this.source = source;
        this.lineId = lineId;
        distance = argWeight;
        this.lineDirection = lineDirection;
        this.lineName = lineName;
    }

    public int getMinutesToTakeTarget(DateTime globalStartTime, int actMinutes, int scheduleType, int metersByWalk){
        if(this.lineId == -1)      {
            if(distance < metersByWalk) {
                int minutes = this.distance/70;
                return minutes;
            }
            else
                return 2000;
        }

        else {
            DateTime afterTime = globalStartTime.plusMinutes(actMinutes);
            weight = SchedulesManager.getTimesBetweenStops(this.source.stopId, this.target.stopId,
            scheduleType, this.lineId, afterTime );

            if(weight > 60)
                return 2000;
            else return weight;
       }
    }
}

