package services
import models.repository._
import scala.slick.session.Session
import play.api.libs.json.Json
import play.api.libs.json.JsObject
import play.api.libs.json.JsArray
import play.api.cache.Cache
import play.api.Play.current
import scala.collection.mutable.HashMap
import org.joda.time._
import java.sql.Timestamp;
import org.joda.time.format.DateTimeFormat
/**
 * User: Mateusz
 * Date: 22.12.13
 * Time: 17:24
 */
//http://localhost:9000/planRoutes/Jarowa/Otwarta/10:04
//http://localhost:9000/planRoute/37/1353/1336/10:04

object RoutingManager {
      var stopRepo = new StopRepository;
      var lineRepo = new LineRepository;
      var lineStopRepo = new LineStopRepository;
      var scheduleInfo = new ScheduleRepository;
       var pointRepo = new PointRepository;


  def detRoutesByStopName(sourceName : String, destName : String, time : String) : String = {
    var proposeList = List[RoutingPropose]();
    val formatter = DateTimeFormat.forPattern("H:mm");

    print(time)

    val inputFormatter = DateTimeFormat.forPattern("HH:mm");
    val inputTime = inputFormatter.parseDateTime(time);


    val now =  new DateTime();
    val newDate = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(), inputTime.hourOfDay().get(), inputTime.minuteOfHour().get());
    formatter.print(newDate)
    print(newDate.toString(inputFormatter))
    controllers.Application.context.database.withSession { implicit session:Session =>


      val stopsIdSource = stopRepo.getStopsByName(sourceName).get
      val stopFirstPoint = pointRepo.getById(stopsIdSource.head.pointId)
      val nearestSource = stopRepo.getNearestStops(stopFirstPoint.get.latitude, stopFirstPoint.get.longitude)


      val stopsIdDestination = stopRepo.getStopsByName(destName).get
      val stopDestinationPoint = pointRepo.getById(stopsIdDestination.head.pointId)
      val nearestDestination = stopRepo.getNearestStops(stopDestinationPoint.get.latitude, stopDestinationPoint.get.longitude)

    for(sourceStop <-nearestSource)
      for(destStop <-nearestDestination) {
        val list = getRouteByStops(sourceStop.id.toString, destStop.id.toString, newDate)
        proposeList = list ::: proposeList
      }
    }
         val obj = Json.obj("propozycje" -> proposeList.map {
           propose => Json.obj(
            "linia" -> propose.lineName,
            "idlinia" -> propose.lineId,
            "kierunek" -> propose.direction,
            "startId" -> propose.stopStart,
            "endId" -> propose.stopEnd,
            "starttime" -> formatter.print(propose.startTime),
            "endtime" -> formatter.print(propose.endTime),
            "proposetime" -> propose.proposeTime,
            "alat" -> propose.avglat.toString,
            "along" -> propose.avglong.toString,
            "totaltime" ->propose.totalTime,
            "entries" -> propose.listEntries.map {
             entry => Json.obj(
                "time" ->entry.departureTime,
                "lat" -> entry.stopLat,
                "long" -> entry.stopLong,
                "name" ->entry.stopName,
                "id" -> entry.stopId
             )
           }
           )
         })
      obj.toString
  }

  def getRouteByDetailedSettings(lineId : String, stopA : String, stopB : String, time : String) : String =
  {

    val inputFormatter = DateTimeFormat.forPattern("HH:mm");
    val inputTime = inputFormatter.parseDateTime(time);


    val now =  new DateTime();
    val newDate = new DateTime(now.year().get(), now.monthOfYear().get(), now.dayOfMonth().get(), inputTime.hourOfDay().get(), inputTime.minuteOfHour().get());


    val info = getListOfStopsOnLineAfterStop(lineId.toInt, stopA.toInt, stopB.toInt, newDate)
    val list = info.listEntries
    val obj = Json.obj(
      "alat" -> info.avglat.toString,
      "along" -> info.avglong.toString,
      "entries" -> list.map {
      entry => Json.obj(
        "time" -> entry.departureTime,
        "lat" -> entry.stopLat,
        "long" -> entry.stopLong,
        "name" -> entry.stopName,
        "id" -> entry.stopId
      )
    })
    obj.toString
  }
  def getRouteByStops(stopA : String, stopB : String, time : org.joda.time.DateTime) : List[RoutingPropose] = {
    var proposeList = List[RoutingPropose]();
    val formatter = DateTimeFormat.forPattern("H:mm");
    controllers.Application.context.database.withSession { implicit session:Session =>
      val lines = getLinesRoutingByStops(stopA, stopB);
      for(lin <- lines)
      {
        val line = lineRepo.getById(lin);
        val info = getListOfStopsOnLineAfterStop(line.get.id, stopA.toInt, stopB.toInt, time)
        if(info != null){
          val list = info.listEntries
          if(list.length > 0 )
            proposeList = proposeList :+  new RoutingPropose(list, line.get.name,
              line.get.id.toString, line.get.direction,
              stopA.toString,
              stopB.toString,
              formatter.print(time),
              info.startTime,
              info.endTime,
              info.totalTime.toString,
              info.avglat,
              info.avglong)
        }
      }
    }
    proposeList
  }

  def getListOfStopsOnLineAfterStop(lineId : Int, startStopId : Int, lastStopId : Int,  ts : org.joda.time.DateTime) :
  RoutingInfo = {
    var entryList = List[RoutingInfoEntry]();
    var staring = new org.joda.time.DateTime();
    var ending = new org.joda.time.DateTime();
    val formatter = DateTimeFormat.forPattern("H:mm");
    var timeStart = ts
    var points = 0
    var alat = 0.0;
    var along = 0.0;
    var dur  = 0;
    controllers.Application.context.database.withSession { implicit session:Session =>
        val thisStop = lineStopRepo.getByLineIdAndStopId(lineId, startStopId);
        val lastStop = lineStopRepo.getByLineIdAndStopId(lineId, lastStopId);
        val stopsAfter = lineStopRepo.getByLineId(lineId).filter(x => (x.id >= thisStop.id && x.id <= lastStop.id) );
        var ss = 0;
        var dd = 0;

        for(s <-  stopsAfter) {
            var lastDep = new DateTime();
            val schedulesToStop = SchedulesManager.getScheduleAsListOfDepartures(lineId, s.stopId, 1, timeStart);
            dd = 0;
            if(schedulesToStop.length > 0) {
                for (dep <- schedulesToStop){
                  val stopInfo = stopRepo.getById(s.stopId).get;
                  val pointInfo = pointRepo.getById(stopInfo.pointId).get
                  if(ss == 0 && dd == 0){
                    entryList = entryList :+ new RoutingInfoEntry(s.stopId.toString,
                      stopInfo.name, pointInfo.latitude.toString,
                      pointInfo.longitude.toString, formatter.print(dep.time))
                      staring = dep.time;
                      ending = dep.time;
                      points = points + 1;
                      alat = alat + pointInfo.latitude;
                      along = along + pointInfo.longitude;
                    dd=dd+1;
                  } else {
                        if(dep.time.isAfter(lastDep)){
                          dd=dd+1
                        }
                    if(dd == 1){
                      lastDep = dep.time;
                      entryList = entryList :+ new RoutingInfoEntry(s.stopId.toString,
                        stopInfo.name, pointInfo.latitude.toString,
                        pointInfo.longitude.toString, formatter.print(dep.time))
                        ending = dep.time;
                        points = points + 1;
                        alat = alat + pointInfo.latitude;
                        along = along + pointInfo.longitude;
                    }
                  }
              }
            ss = ss+ 1;
          }
          timeStart = ending
      }
      val total = new Interval(staring, ending)
      dur = total.toDuration().toPeriod().getMinutes()
    }
    new RoutingInfo(entryList, staring, ending, dur, alat/points, along/points)
  }

  def getLinesRoutingByStops(stopA : String, stopB: String) : List[Int] = {
    var list = List[Int]();
    controllers.Application.context.database.withSession { implicit session:Session =>
      val lines_1 = lineStopRepo.getByStopId(stopA.toInt).map( m => m.lineId );
      val lines_2 = lineStopRepo.getByStopId(stopB.toInt).map( m => m.lineId );
      val lines = lines_2.intersect(lines_1);
      list = for (f <- lines ) yield f;
    }
    list
  }


  def getNearestStops(lat : Float, long : Float) = {
    controllers.Application.context.database.withSession { implicit session:Session =>
       stopRepo.getNearestStops(lat, long)
    }
  }



}
class RoutingInfoEntry(val stopId:String,
                       val stopName:String,
                       val stopLat:String,
                       val stopLong : String,
                       var departureTime : String)  {

}

class RoutingInfo (val listEntries : List[RoutingInfoEntry],
                   val startTime : org.joda.time.DateTime,
                   val endTime : org.joda.time.DateTime,
                   val totalTime : Int,
                   val avglat : Double,
                   val avglong : Double

                    ) {

}

class RoutingPropose(val listEntries : List[RoutingInfoEntry],
                     val lineName : String,
                     val lineId : String,
                     val direction : String,
                     val stopStart : String,
                     val stopEnd : String,
                     val proposeTime : String,
                     val startTime : org.joda.time.DateTime,
                     val endTime : org.joda.time.DateTime,
                     val totalTime : String,
                     val avglat : Double,
                     val avglong : Double) {

}
