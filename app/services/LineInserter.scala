package services

import scala.slick.driver.PostgresDriver.simple._

/**
 * User: Kuczi
 */
object LineInserter {
  def isTram(lineNumber: Int) = {
    if (lineNumber > 0 && lineNumber < 100) {
      true;
    } else {
      false
    }
  }

  val TramType = 1
  val BusType = 2
  val NightBusType = 3
  val WaterTramType = 4

  def getLineType(lineName:String) = {
    var lineNumber = 0
    try {
      lineNumber = lineName.toInt
    } catch {
      case nfe: NumberFormatException => lineNumber = -1;
    }
    var vehicleType = 0
    if(isTram(lineNumber)) {
      vehicleType = TramType
    } else{
      if('F' == lineName(0).toUpper) {
        vehicleType = WaterTramType
      } else if ('N' == lineName(0).toUpper) {
        vehicleType = NightBusType
      } else {
        vehicleType = BusType
      }
    }
    vehicleType
  }
}
