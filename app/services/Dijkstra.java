package services;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

/**
 * User: Mateusz
 * Date: 26.01.14
 * Time: 19:20
 */
public class Dijkstra {

    private static void computePaths(Vertex source, DateTime startTime, int scheduleType, int transportationType, int metersByWalk)
    {
        source.minDistance = 0;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
        vertexQueue.add(source);

        while (!vertexQueue.isEmpty()) {
            Vertex u = vertexQueue.poll();
            for (Edge e : u.getEdges(transportationType))
            {
                Vertex v = e.target;
                int distanceThroughU = u.minDistance + e.getMinutesToTakeTarget(startTime, u.minDistance, scheduleType, metersByWalk);
                if (distanceThroughU < v.minDistance) {
                    vertexQueue.remove(v);
                    v.minDistance = distanceThroughU ;
                    v.previous = u;
                    v.bestVertexToStop = e;
                    v.minutesToThisVertex = distanceThroughU;
                    v.linia = e.lineName;
                    vertexQueue.add(v);
                }
            }
        }
    }
    public static List<Vertex> searchPath(Vertex a, Vertex b, DateTime startTime, int scheduleType, int transportationType, int metersByWalk) {
        computePaths(a, startTime, scheduleType, transportationType, metersByWalk);
        List<Vertex> path = getShortestPathTo(b);
        path.get(0).linia = path.get(1).linia;
        path.get(0).bestVertexToStop = path.get(1).bestVertexToStop;
        path.get(0).minutesToThisVertex = path.get(1).minutesToThisVertex;
        return path;
    }
    private static List<Vertex> getShortestPathTo(Vertex target)
    {
        List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            path.add(vertex);
        Collections.reverse(path);
        return path;
    }
}
