import models.ZTMContext
import org.specs2.mutable.Specification

/**
 * User: Tomasz
 * Date: 23.06.13
 */
class ZTMContextTests extends Specification {
  /*val context = new ZTMContext

  "getLineSchedule" should {
    "return null while passing improper line id" in {
      val result = context.getLineSchedule(-1)
      result must beNull
    }.pendingUntilFixed

    "return tuple consists of line, stops and departures list" in {
      val tuple = context.getLineSchedule(1);
      val line = tuple._1
      val stops = tuple._2
      val departuresList = tuple._3

      stops.length must beGreaterThan(0)
      departuresList.length must beGreaterThan(0)
      line must not beNull

    }.pendingUntilFixed

    "return tuple where stop list length is equal to departures list length" in {
      val tuple = context.getLineSchedule(1);
      val stops = tuple._2
      val departuresList = tuple._3

      stops.length must beEqualTo(departuresList.length)
    }.pendingUntilFixed
  }

  "searchRoute" should {
    "return null while passing empty source string" in {
      val result = context.searchRoute("", "Uphagena", 5555)
      result must beNull
    }.pendingUntilFixed

    "return null while passing empty target string" in {
      val result = context.searchRoute("Wrzeszcz PKP", "", 5555)
      result must beNull
    }.pendingUntilFixed

    "return tuple consists of line and departures list" in {
      val tuple = context.searchRoute("Wrzeszcz PKP", "Oliwa PKP", 5555)
      val line = tuple._1
      val stops = tuple._2

      line.length must beGreaterThan(0)
      stops.length must beGreaterThan(0)
    }.pendingUntilFixed
  }

    "getStopUpcomingDeparturesList" should {
          "return null while stop not exists" in {
          val tuple = context.getStopUpcomingDeparturesList("sdvweweas", null)
          tuple must beNull
          }.pendingUntilFixed

          "return full list -> stop, lines, departures" in {
              val tuple = context.getStopUpcomingDeparturesList("sdvweweas", null)

               val stop = tuple._1
               val lines = tuple._2
               val departuresList = tuple._3

               lines.length must beGreaterThan(0)
               departuresList.length must beGreaterThan(0)
               stop must not beNull
          }.pendingUntilFixed
    }*/

}
