import org.specs2.mutable.Specification
import org.joda.time._
import models.parser.ActualScheduleEntry


class ActualScheduleEntryTests extends Specification{
	"timeToArriveInMinutes" should {
	  val d = new DateTime(1,1,1,14,59)
	  
	  "return number greater than 0 if arriva will take place in next hour" in {    
	    val entry = new ActualScheduleEntry("", "", "16:01")
	    if(entry.timeToArriveInMinutes(d) > 0) {
	      success
	    } else {
	      failure
	    }
	  }
	}
}