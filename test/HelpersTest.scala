import java.net.MalformedURLException
import org.specs2.mutable.Specification
import services.Helpers


/**
 * Created with IntelliJ IDEA.
 * User: Tomasz
 * Date: 18.04.13
 * Time: 17:55
 * To change this template use File | Settings | File Templates.
 */


class HelpersTests extends Specification {

  "loadSource" should {

    "return nonempty string" in {
      val link:String = "http://www.ztm.gda.pl/"
      val source = Helpers.loadSource(link)

      (source must be empty).not
    }

    "return string containing html tags" in {
      val link:String = "http://www.ztm.gda.pl/"
      val source :String = Helpers.loadSource(link)

      source must beMatching("<html.+>") and contain("</html>")
    }

    "return empty string while passed null parameter" in {
      val link:String = null
      val source = Helpers.loadSource(link)

      source must be empty
    }

    "throw exception while passed illegal url parameter" in {
      val link:String = "illegal"

      Helpers.loadSource(link) must throwA[MalformedURLException]
    }
  }

  "addRootElement should return string surrounded 'root' tags" ! {
    var source:String = "<a></a><b><c></c></b>"
    val result = Helpers.addRootElement(source)

    result must contain("<root>") and contain("</root>")
  }

  "getLinesFromFile should return empty list when file does not exits" in {
    var result = Helpers.getLinesFromFile("####")
    if(result.length == 0) success
    else failure
  }

  "indexOfAll" should {
    "return empty list while string does not contain specified string" in {
      val text = "abcabcabc"
      val seed = "d"
      val result = Helpers.indexOfAll(seed, text)
      if(result.isEmpty) success
      else failure
    }

    "return positions of all occurrences" in {
      var text = ""
      val count = 5
      val seed = "sd"
      for(index <- 1 to count) {
        text = text + "spam" + seed + "spam"
      }
      val result = Helpers.indexOfAll(seed, text)
      if(result.length == count) success
      else {
        failure("Found " + result.length + "occurrences instead of " + count)
      }
    }
  }

  "extractTable" should {
    "throw IllegalArgumentException when code does not contain same number of table opening and closing tags" in {
      val html = "<table><table></table>"
      try {
        val result = Helpers.extractTable(1, html)
        failure
      } catch {
        case iae:IllegalArgumentException => success
        case _ => failure
      }
    }

    "return string starting with \"<table\"" in {
      val html = "<table>1<table>2<table>3</table></table></table>"
      try {
        val result = Helpers.extractTable(2, html)
        if(result.startsWith("<table")) success
        else failure
      } catch {
        case _ => failure
      }
    }

    "return string ending with \"</table>\"" in {
      val html = "<table>1<table>2<table>3</table></table></table><table>4</table>"
      try {
        val result = Helpers.extractTable(2, html)
        if(result.endsWith("</table>")) success
        else failure
      } catch {
        case _ => failure
      }
    }

    "return right table" in {
      val html = "<table>1</table><table>2</table><table>3</table><table>4</table>"
      try {
        val result = Helpers.extractTable(2, html)
        if(result.contains("2")) success
        else failure
      } catch {
        case _ => failure
      }
    }

    "return jagged table" in {
      val html = "<table>1<table>2<table>3</table></table></table><table>4</table>"
      try {
        val result = Helpers.extractTable(2, html)
        if(result.contains("2") && result.contains("3") && !result.contains("4")) success
        else failure
      } catch {
        case _ => failure
      }
    }
  }
}
