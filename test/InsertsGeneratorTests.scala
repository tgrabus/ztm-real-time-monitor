import models.parser.Line
import org.specs2.mutable.Specification
import services.LineInserter._

/**
 * User: Kuczi
 */
class InsertsGeneratorTests extends Specification{
   "getLineType" should {
     "return 1 when line name is lesser then 100" in {
       if(getLineType("99") != 1) failure
       else success
     }

     "return 2 when line name is greater/equal then 100" in {
       if(getLineType("100") != 2) failure
       else success
     }

     "return 3 if line name starts with N" in {
       if(getLineType("N15") != 3) failure
       else success
     }

     "return 4 if its water tram line" in {
       if(getLineType("F15") != 4) failure
       else success
     }
   }
}
