/**
 * User: Kuczi
 */
import org.specs2.mutable._
import services.StopsParser

class StopsParserTests extends Specification {
  /*"The StopParser " should {
    "return non-empty list" in {
      StopsParser.getStops.length must_!=(0)
    }
    "Stop name should not contain '<'" in {
      for (stop <- StopsParser.getStops)
        if(stop.name.contains("<")) failure
      success
    }
    "Stop name should not contain '>'" in {
      for (stop <- StopsParser.getStops)
        if(stop.name.contains(">")) failure
      success
    }
  }*/
  "parseStop" should {
    val stopId = 1
    val stopName = "Nazwa"
    val long = 53.123
    val lat = 18.321
    val separator = ";"
    val entry = stopId + separator + stopName + separator + lat + separator + long
    
    "return stop with id from first column" in {
      val s = StopsParser.parseStop(entry)
      if(s.id == stopId) {
        success
      } else {
        failure
      }
    }
    
     "return stop with name from second column" in {
      val s = StopsParser.parseStop(entry)
      if(s.name == stopName) {
        success
      } else {
        failure
      }
     }
      
     "return stop with longitude from thrid column" in {
      val s = StopsParser.parseStop(entry)
      if(s.position.long == long) {
        success
      } else {
        failure
      }
     }
     
     "return stop with latitude from second column" in {
      val s = StopsParser.parseStop(entry)
      if(s.position.lat == lat) {
        success
      } else {
        failure
      }
     }
     
     
    "throw exception when id is not number" in {
      try {
    	  val s = StopsParser.parseStop("aa" + separator + stopName + separator + lat + separator + long)
    	  failure
      }
      catch {
        case iae:IllegalArgumentException => success
      }
     }
     
     "throw exception when longitude is not number" in {
      try {
    	  val s = StopsParser.parseStop(stopId + separator + stopName + separator + lat + separator + "aaa")
    	  failure
      }
      catch {
        case iae:IllegalArgumentException => success
      }
     }
  
  
     "throw exception when latitude is not number" in {
      try {
    	  val s = StopsParser.parseStop(stopId + separator + stopName + separator + "aaa" + separator + long)
    	  failure
      }
      catch {
        case iae:IllegalArgumentException => success
      }
     }
  }
}
