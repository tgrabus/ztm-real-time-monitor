import org.specs2.mutable.Specification
import play.api.libs.json.Json
import play.api.libs.json.JsArray

class ActualScheduleParserTests extends Specification{
  val validStopId = "2076"
  val invalidStopId = "dasfsadfa"
  
	"ActualScheduleParser" should {
	  /*"return JSON array in case of valid id" in {
	    val res = services.ActualScheduleParser.parse(validStopId);
	    val a = Json.parse(res)
	    if(a.isInstanceOf[JsArray]) {
	      success
	    } else {
	      failure
	    }
	  }*/
	  
	  "return ActualSchedule without entries" in {
	    val res = services.ActualScheduleParser.parse(invalidStopId);
	    if(0 == res.entries.length) {
	      success
	    } else {
	      failure
	    }
	  }
	  
	}
}