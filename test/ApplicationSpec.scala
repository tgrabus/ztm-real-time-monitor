package test

import org.specs2.mutable._

import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
class ApplicationSpec extends Specification {
  
  "Application" should {
    
    "send 404 on a bad request" in {
      running(FakeApplication()) {
        route(FakeRequest(GET, "/boum")) must beNone        
      }
    }
  }
    
    /*"render the index page" in {
      running(FakeApplication()) {
        val home = route(FakeRequest(GET, "/")).get
        
        status(home) must equalTo(OK)
        contentType(home) must beSome.which(_ == "text/html")
        contentAsString(home) must contain ("Your new application is ready.")
      }
    }*/

    "respond to the getLineNames Action " should {
      running(new FakeApplication()) {
        val result = controllers.Application.getLineNames()(FakeRequest())

      "be successful" in {
          status(result) must equalTo(OK)
        }

      "be a json" in {
        contentType(result) must beSome("application/json")
      }
    }
  }

  "respond to the getLinesWithDirections Action " should {
    running(new FakeApplication()) {
      val result = controllers.Application.getLinesWithDirections()(FakeRequest())

      "be successful" in {
        status(result) must equalTo(OK)
      }

      "be a json" in {
        contentType(result) must beSome("application/json")
      }
    }
  }

  "respond to the getStopNames Action " should {
    running(new FakeApplication()) {
      val result = controllers.Application.getStopNames()(FakeRequest())

      "be successful" in {
        status(result) must equalTo(OK)
      }

      "be a json" in {
        contentType(result) must beSome("application/json")
      }
    }
  }
}