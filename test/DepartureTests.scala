import org.specs2.mutable.Specification
import org.joda.time._
import models.parser.ActualScheduleEntry
import models.parser.Departure

class DepartureTests extends Specification {
	"differenceInMinutes for departures in same hour" should {
	  val min = 30
	  val diff = 5
	  val first = new Departure(new DateTime(1,1,1, 12, min), "")
	  val second = new Departure(new DateTime(1, 1, 1, 12, min - diff), "")
	  
	  "return valid result if first departure is later then second" in {  
	    if(diff == Departure.differenceInMinutes(first, second)) {
	      success
	    } else {
	      failure
	    }
	  }
	  
	  "return valid result if second departure is later then fist" in {
	    if(diff == Departure.differenceInMinutes(second, first)) {
	      success
	    } else {
	      failure
	    }
	  }
	}
	
	"differenceInMinutes for departures in different hours" should {
	  
	  "return valid result if first departure is later" in {
	     val first = new Departure(new DateTime(1,1,1, 13, 5), "")
	     val second = new Departure(new DateTime(1, 1, 1, 12, 55), "")
	     if(10 == Departure.differenceInMinutes(first, second)) {
	       success
	     } else {
	       failure
	     }
	  }
	  
	  "return valid result if first departure is ealier" in {
	     val first = new Departure(new DateTime(1,1,1, 12, 55), "")
	     val second = new Departure(new DateTime(1, 1, 1, 13, 5), "")
	     val dist = Departure.differenceInMinutes(first, second)
	     if(10 == dist) {
	       success
	     } else {
	       failure(dist.toString)
	     }
	  }
	}
}