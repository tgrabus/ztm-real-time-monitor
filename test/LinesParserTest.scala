import org.specs2.mutable.Specification
import services.{LineStopsParser, LinesParser}

/**
 * Created with IntelliJ IDEA.
 * User: Tomasz
 * Date: 18.04.13
 * Time: 18:14
 * To change this template use File | Settings | File Templates.
 */
class LinesParserTest extends Specification {
  "getLines" should {
    /*val lines = LinesParser.getLines

    "return nonempty list of lines" in {
      (lines must be empty).not
    }

    "return lines with associated stops" in {
      for(line <- lines) {
        if(line.stops.length < 1) failure
      }

      success
    }

    "return lines with associated stops" in {
      for(line <- lines) {
        if(line.waypoints.length < 1) failure
      }

      success
    }.pendingUntilFixed
  }

  "getAssociatedLineStops" should {
    val directions = LineStopsParser.getAssociatedLineStops("start-002_20120922-1.html")

    "return array of two directions" in {
      directions.length must be_==(2)
    }

    "return lists filled stops" in {
      for(direction <- directions) {
        if(direction.length < 1) failure
      }

      success
    }*/
  }

  "extractLink" should {
    "return string witout double quote" in {
      val testTag = "<a href=\"start-002_20120922-1.html\" class=\"block\">"
      val result = LinesParser.extractLink(testTag)
      if(result.contains("\"")) failure
      else success
    }
  }
  
  "extractStopId" should {
    "return positive integer for valid html" in {
      val id = 1234
      val code = "<span title=\"Numer slupka\"><i>" + id + "</i></span>"
      if(id == LineStopsParser.extractStopId(code)) {
        success
      } else {
        failure
      } 
    }
    
    "handle format like 123,45 and return value after coma" in {
      val f = 123
      val s = 45
      val code = "<span title=\"Numer slupka\"><i>" + f +"," + s + "</i></span>"
      if(s == LineStopsParser.extractStopId(code)) {
        success
      } else {
        failure
      }
    }
    
    "throw IllegalArgumentException in other cases" in {
      val code = "<span title=\"Numer slupka\"><i>1dsa1</i></span>"
      try {
    	  LineStopsParser.extractStopId(code)
    	  failure
      } catch {
        case iae : IllegalArgumentException => success
      }
    }
  }
}
