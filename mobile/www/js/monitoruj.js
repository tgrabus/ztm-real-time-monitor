$(document).on('pageinit', function() {
    document.addEventListener("deviceready", onDeviceReady, false);
    onDeviceReady();
});

var ctaLayer;
var url = 'http://ztm-monitor.herokuapp.com/trasaLinii/4353453/199';

function onDeviceReady() {
    $(window).unbind();
    $(window).bind('pageshow resize orientationchange', function(e){
        max_height();
    });
    max_height();
    var googleMap = new GoogleMap("map");
	getVehiclesOnLine();
    interval = setInterval(function() {getVehiclesOnLine();}, 20000);				
    var callback = googleMap.Init;
    google.load("maps", "3", {"callback": callback, other_params: "sensor=true"});
}
function GoogleMap(_id) {
    var map;
    var id = _id;
    this.Init = function() {   	
        if(map == null) {
            var latlng = new google.maps.LatLng(52, 20);
            var myOptions = {
                zoom: 10,
                center: latlng,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: false,
				scaleControl: false,
				disableDefaultUI: true,
				mapTypeControl: false, // kontrolka trybu mapy
				mapTypeControlOptions:
				{
					style: google.maps.MapTypeControlStyle.DROPDOWN_MENU // w stylu rozwijanego menu
				},
				streetViewControl: false
            };
            map = new google.maps.Map(document.getElementById(_id), myOptions);
            ctaLayer = new google.maps.KmlLayer(url);
            ctaLayer.setMap(map);
        }
    }
    this.GetMap = function() {
        return map;
    }
}
function max_height(){
    var h = 0;
    var f = 0;
    var w = $(window).height();
    var c = $('div[data-role="content"]');
    var c_h = c.height();
    var c_oh = c.outerHeight(true);
    var c_new = w - h - f - c_oh + c_h;
    var total = h + f + c_oh;
    if(c_h<c.get(0).scrollHeight){
        c.height(c.get(0).scrollHeight);
    }else{
        c.height(c_new);
    }
}

var markers = []
var monitoredLine

function getVehiclesOnLine() {
    var url = "http://ztm-monitor.herokuapp.com/pojazdyNaLinii/199"
    $.ajax({
        url: url,
        success: function (result) { updateMap(result) },
        dataType: 'json'
    });

}

function updateMap(data) {
    removeOldMarkups()
    addNewMarkups(data)
}

function removeOldMarkups() {
    for(index in markers) {
        markers[index].setMap(null)
    }
}

function addNewMarkups(data) {
    markers = []
    for(var i = 0; i < data.length; i++) {
        var pos = new google.maps.LatLng(data[i].pozycja.dlugosc, data[i].pozycja.szerokosc)
        var icon = selectMarkerIcon(data[i].pojazd)
        var label = "Pojazd " + data[i].id;
        var marker = new google.maps.Marker({
            position: pos,
            title: label,
            icon: icon});
        marker.setMap(mapa)
        markers.push(marker)
    }
}

function selectMarkerIcon(vehicleType) {
    if(vehicleType == "Tram") {
        return "http://ztm-monitor.herokuapp.com/assets/images/train1.png";
    }  else {
        return "http://ztm-monitor.herokuapp.com/assets/images/bus1.png";
    }
}

function deleteOverlays() {
  removeOldMarkups();
  markers = [];
}
