$(document).on('pageinit', '#mapPage', function() {
    document.addEventListener("deviceready", onDeviceReady, false);
    //onDeviceReady();
});

function onDeviceReady() {
       
    //$('#map').css('height',getRealContentHeight());

    var googleMap = new GoogleMap("map");
    var callback = googleMap.Init;
    google.load("maps", "3", {"callback": callback, other_params: "sensor=true"});
}


function GoogleMap(_id) {
    var map;
    var id = _id;

    this.Init = function() {   	
        if(map == null) {
            var latlng = new google.maps.LatLng(52, 20);
            var myOptions = {
                zoom: 10,
                center: latlng,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: false,
				scaleControl: false,
				disableDefaultUI: true,
				mapTypeControl: false,
				mapTypeControlOptions:
				{
					style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
				},
				streetViewControl: false
            };

            map = new google.maps.Map(document.getElementById(_id), myOptions);
            
            
            
            showLineTrack();
            
            
            //var userPos = new UserPosition(map);
            //userPos.TrackUserPosition();
        }
    };

    this.GetMap = function() {
        return map;
    };
    
    function showLineTrack() {
    	var json = JSON.parse(NavigationObject.params);

		var key = json.params[0].key;
		var linia = json.params[0].value;
		var kierunek = json.params[1].value;

		if(linia && kierunek) {
			
			var $header = $(".ui-header .ui-title", "#mapaPage");
			
			$header.html("Trasa linii " + linia);
			
			monitor(linia + ":" + kierunek);
		}
    }
    
    function monitor(nazwaLinii) {

        //clearRouteToStop();
        //clearMap()
        //removeStopsMarkers()
        //deleteOverlays();
    	
        var url = ManagerLinks.serviceUrl + '/trasaLinii/' + nazwaLinii;

        $.ajax({
            success : function (data) {
            	google.maps.event.trigger(map, "resize");
            	
                setPath(data.path, data.avglat, data.avglong);
                setStopsMarkers(data.stops);
            },
            error: function (xhr, ajaxOptions, thrownError) {
            	alert('eeerrrorrrr');
            },
            dataType: 'json',
            url: url,
            type: 'GET'
        });
    };
    
    function setPath(points, cent_lat, cent_long) {
    	var path = [];
    	for(var i = 0; i < points.length; i++) {
            var pos = new google.maps.LatLng(points[i].lat, points[i].long)
            path.push(pos);
        }
        var panning = new google.maps.LatLng(cent_lat,cent_long);
    	var polyLine = new google.maps.Polyline({
    		path: path,
    		geodesic: true,
    		strokeColor: '#1161a3',
    	    strokeOpacity: 1.0,
    	    strokeWeight: 5
    	});
    	polyLine.setMap(map)
        map.panTo(panning);
    };

    function setStopsMarkers(stops) {
    	var stopMarkers = []
        var markers = new Array();
    	
    	for(var i = 0; i < stops.length; i++) {
            var pos = new google.maps.LatLng(stops[i].position.lat, stops[i].position.long);
            var icon;
            if(0 == i) {
            	icon = {
    		    url: "http://153.19.52.107/assets/images/flag2_map.png",
    		    scaledSize: new google.maps.Size(32, 32)
    		  };
            } else if(stops.length - 1 == i) {
            	icon = {
        		    url: "http://153.19.52.107/assets/images/flag3_map.png",
        		    scaledSize: new google.maps.Size(32, 32)
        		  };
            } else {
            	icon = {
    			    url: "http://153.19.52.107/assets/images/flag_map.png",
    			    scaledSize: new google.maps.Size(32, 32)
    			  };
            }
            var label = stops[i].name
            markers[i] = new google.maps.Marker({
                position: pos,
                title: label,
                icon: icon});

            markers[i].setMap(map);
            stopMarkers.push(markers[i]);

            google.maps.event.addListener(markers[i], 'click', function() {
            	var infowindow = new google.maps.InfoWindow({
                    content: this.title
                });
            	
            	infowindow.open(map, this);
            });
        }
    };
}

function UserPosition(_map) {
    var map = _map;
    var marker;
    var infowindow;
    var watchId;
    this.TrackUserPosition = function() {
			google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
            var geoOptions = {
                maximumAge: 5000,
                timeout: 5000,
                enableHighAccuracy: true
            }
            watchId = navigator.geolocation.watchPosition(geoSuccess, geoError, geoOptions);
        });
    }
    var geoError = function(error) {
        alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
    }
    var geoSuccess = function(position) {
        var info =
            ('Latitude: '         + position.coords.latitude          + '<br>' +
                'Longitude: '         + position.coords.longitude         + '<br>' +
                'Altitude: '          + position.coords.altitude          + '<br>' +
                'Accuracy: '          + position.coords.accuracy          + '<br>' +
                'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '<br>' +
                'Heading: '           + position.coords.heading           + '<br>' +
                'Speed: '             + position.coords.speed             + '<br>' +
                'Timestamp: '         + new Date(position.timestamp));
        var point = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        map.setCenter(point);
        map.setZoom(15);

        if(!marker){
            marker = new google.maps.Marker({
                position: point,
                animation: google.maps.Animation.BOUNCE,
                map: map
            });
        }
        else {
            marker.setPosition(point);
        }

        if(!infowindow){
            infowindow = new google.maps.InfoWindow({
                content: info
            });
        }
        else {
            infowindow.setContent(info);
        }
        
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });
        
        google.maps.event.trigger(map, "resize");
    }

}

function getRealContentHeight() {
    var header = $.mobile.activePage.find("div[data-role='header']:visible");
    var footer = $.mobile.activePage.find("div[data-role='footer']:visible");
    var content = $.mobile.activePage.find("div[data-role='content']:visible:visible");
    var viewport_height = $(window).height();

    var content_height = viewport_height - header.outerHeight(); - footer.outerHeight();
    if((content.outerHeight() - header.outerHeight() - footer.outerHeight()) <= viewport_height) {
        content_height -= (content.outerHeight() - content.height());
    } 
    return content_height;
}

function createBoundedWrapper(object, method) {
	  return function() {
	    return method.apply(object, arguments);
	  };
}