//mapa na stronie
var mapa
//interwał do aktualizacji pozycji pojazdów na mapie
var interval
//warstwa z przystankami i trasą linii
var ctaLayer

function mapaStart()
{
    var wspolrzedne = new google.maps.LatLng(54.356556,18.647919);
    var opcjeMapy = {
      zoom: 13,
      center: wspolrzedne,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: false,
      scaleControl: false,
      zoomControl: false
    };
    mapa = new google.maps.Map(document.getElementById("mapka"), opcjeMapy);
}

$(function() {
var availableTags = [
    "Traugutta-Sobieskiego",
    "Wrzeszcz PKP",
    "Uphagena",
    "Firoga",
    "Szybowcowa",
    "Kurpińskiego",
    "Jaśkowa Dolina",
    "Do Studzienki"
];
var lines = [
    "115",
    "119",
    "232",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "11",
    "12",
    "116",
    "210"
];
$( "#from" ).autocomplete({
    source: availableTags
});
$( "#to" ).autocomplete({
    source: availableTags
});

$( "#linia" ).autocomplete({
    source: lines,
    select: function( event, ui ) {
        alert( ui );
        return false;
}

});

$('#from').keyup(function() {
    $('#tab-linia').show(100);
    var przystanek = $(this).val();
    var url = '/tablicaPrzystanku/' + przystanek;
    var url2 = '/MockupTest/' + przystanek;
    $.ajax({
        success : function (data) {
          $('#tab-linia').html("");

          var len = data.length;
          var i = 0;
          for(i =0; i<len; i++)
          {
              $('#tab-linia').append("<div>" + data[i]._lineNumber + " " + data[i]._timeToArrive + " " + data[i]._direction + "</div>");
          }
        },
        dataType: 'json',
        url: url2,
        type: 'GET'
    });

});


$('#search').click(function() {
    $('#tab-linia').show(100);
    var przystanek = $('#from').val();
    var url = '/tablicaPrzystanku/' + przystanek;
    var url2 = '/MockupTest/' + przystanek;
    $.ajax({
        success : function (data) {
            $('#tab-linia').html("");
            var len = data.length;
            var i = 0;
            for(i =0; i<len; i++)
            {
                $('#tab-linia').append("<div>" + data[i]._lineNumber + " " + data[i]._timeToArrive + " " + data[i]._direction + "</div>");
            }
        },
        dataType: 'json',
        url: url2,
        type: 'GET'
    });
});

$('#linia').keyup(function(event) {
    $('#rozklad-linia-result').show(100);
    var lineid = $(this).val();
    var url = '/rozkladJazdy/' + lineid;
    $.ajax({
        success : function (data, textStatus) {
          $('#rozklad-linia-result').html(data);
        },
        dataType: 'html',
        url: url,
        type: 'GET'
    });

});

$('#pokazMonitorowanaLinie').click(function() {
    //usuwanie starej linii
    $("#informacjeObledzie").text("");
    //przerwanie akutalizacji pozycji pojazdów
    if(typeof interval !== 'undefined') {
        clearInterval(interval);
    }
    deleteOverlays();
    var nazwaLinii = $('#monitorowanaLinia').val();
    var url = 'http://ztm-monitor.herokuapp.com/trasaLinii/' + nazwaLinii;
    var request = new XMLHttpRequest;
    request.open('GET', url, true);
    request.send();
    request.onreadystatechange = function(){
        if(request.status==200 ){
            if(request.readyState == 4) {
                if(typeof ctaLayer !== 'undefined') {
                    ctaLayer.setMap(null);
                }
                ctaLayer = new google.maps.KmlLayer(url);
                ctaLayer.setMap(mapa);
                monitoredLine = nazwaLinii;
                getVehiclesOnLine();
                interval = setInterval(function() {getVehiclesOnLine();}, 20000);
            }
        }else{
            $("#informacjeObledzie").text("Linia " + nazwaLinii + " nie istnieje!");
        }
    };
});

$(function() {
$( document ).tooltip();
});

$("#przesiadkiNiedozwolone").hide();
$("#tramwajeNiedozwolone").hide();
$("#autobusyNiedozwolone").hide();

$("#autobusyDozwolone").click(function() {
    $("#autobusyDozwolone").hide();
    $("#autobusyNiedozwolone").show();
    $("#bus").val(0);
});

$("#tramwajeDozwolone").click(function() {
    $("#tramwajeDozwolone").hide();
    $("#tramwajeNiedozwolone").show();
    $("#tram").val(0);
});

$("#autobusyNiedozwolone").click(function() {
    $("#autobusyDozwolone").show();
    $("#autobusyNiedozwolone").hide();
    $("#bus").val(1);
});

$("#tramwajeNiedozwolone").click(function() {
    $("#tramwajeDozwolone").show();
    $("#tramwajeNiedozwolone").hide();
    $("#tram").val(1);
});

$("#przesiadkiDozwolone").click(function() {
    $("#przesiadkiDozwolone").hide();
    $("#przesiadkiNiedozwolone").show();
    $("#transfer").val(0);
});

$("#przesiadkiNiedozwolone").click(function() {
    $("#przesiadkiDozwolone").show();
    $("#przesiadkiNiedozwolone").hide();
    $("#transfer").val(1);
});

$( "input[type=submit]" ).button().click(function( event ) { event.preventDefault(); });

});