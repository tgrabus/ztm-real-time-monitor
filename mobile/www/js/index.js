﻿/***** TOOLS ******/
var ManagerLinks = {
	serviceUrl: "http://153.19.52.107",	
	
	getStops: "/apimobile/stops",
	getLinesNames: "/apimobile/lines",
	getLineDirections: "/apimobile/directions/:name/",
	getLineStopsForDirection: "/apimobile/linestops/:name/:direction/",
	getActualSchedulesForStop: "/getActualDeparturesOnStop/:id"
};

var NavigationLinks = {
	trasaPageId: "#trasaPage",
	trasaPageFile: "index.html",
	wybierzPrzystanekPageId: "#wybierzPrzystanekDialogPage",
	wybierzPrzystanekPageFile: "wybierz_przystanek.html",
	ustawOpcjeDialogPageId: "#ustawOpcjeDialogPage",
	liniePageId: "#liniePage",
	liniePageFile: "linie.html",
	kierunkiPageId: "#kierunkiPage",
	kierunkiPageFile: "kierunki.html",
	liniaKierunekPrzystankiPageId: "#liniaKierunekPrzystankiPage",
	liniaKierunekPrzystankiPageFile: "linia_kierunek_przystanki.html",
	mapaPageId: "#mapPage",
	mapaPageFile: "map.html",
	rozkladyAktualnePageId: "#rozkladyAktualnePage",
	rozkladyAktualnePageFile: "rozkladyAktualne.html",
	przystankiPageId: "#przystankiPage",
	przystankiPageFile: "przystanki.html"
};

var NavigationObject = {
	fromPage: "",
	toPage: "",
	control: "",
	params: ""
};

function FillLinkParameter(link, param, value) {
	return link.replace(param, value);
}

function ShowLoader() {	
	setTimeout(function(){
        $.mobile.loading('show');
    }, 1); 
}

function HideLoader() {
	setTimeout(function(){
        $.mobile.loading('hide');
    }, 1); 
}

function Navigate() {
	//alert(NavigationObject.toPage);
	
	$.mobile.changePage(NavigationObject.toPage, { transition: "slide", reverse: true, changeHash: true });
}

/******************/

/**** MANAGERS ****/
function ChooseBusStopViewManager(pageId) {
	
	this.AddListViewToPageContentAsync = addListViewToPageContentAsync;
	this.ListenOnListViewItemClick = listenOnListViewItemClick;
	this.ListenOnListViewItemClick2 = listenOnListViewItemClick2;
	
	function addListViewToPageContentAsync() {
		ShowLoader();
		
		var busStopManager = new BusStopManager();
	    busStopManager.GetBusStopsData(onGetBusStopsDataCallback);
	}
	
	function onGetBusStopsDataCallback(data) {	
		var $content = $(".ui-content .list-wrapper", pageId);
		$content.empty();

		var $listView = $('<ul/>', {
		    "data-role": "listview",
		    "data-filter": "true",
		    "data-filter-placeholder": "Wyszukaj przystanek"
		});
		
		//data.stops.sort();
		
		addRecursivelyItemsToListView(0, $content, $listView, data.stops);
	}
	
	function addRecursivelyItemsToListView(i, $content, $listView, data) {	
		
		$li = $('<li><h3>' + data[i].name + '</h3><p>' + data[i].id + '</p></li>');
		$li.data('stopId', data[i].id);
		
		$listView.append($li);
		
		setTimeout(function () {
			if (++i < data.length) {				
				addRecursivelyItemsToListView(i, $content, $listView, data);
		    } else {
		    	$listView.appendTo($content).listview();
		    	HideLoader();
		    }
		}, 1);
	};
	
	function listenOnListViewItemClick() {
		$(document).on("click", pageId + " li", function(event) {

			var selectedBusStop = $(this).text();
			
			NavigationObject.fromPage = NavigationLinks.wybierzPrzystanekPageFile;
			NavigationObject.toPage = NavigationLinks.trasaPageFile;
			NavigationObject.params = '{"params": [{"key":"selectedBusStop","value":"' + selectedBusStop + '"}]}';
			
			Navigate();
		});
	};
	
	function listenOnListViewItemClick2() {
		$(document).on("click", pageId + " li", function(event) {

			var selectedBusStop = $(this).data('stopId');
			
			NavigationObject.fromPage = NavigationLinks.przystankiPageId;
			NavigationObject.toPage = NavigationLinks.rozkladyAktualnePageFile;
			NavigationObject.params = '{"params": [{"key":"selectedBusStop","value":"' + selectedBusStop + '"}]}';
			
			Navigate();
		});
	};
}

function TripViewManager(pageId) {
	
	this.UpdateChooseStopButtonText = updateChooseStopButtonText;	
	this.InitializeListeners = initializeListeners;
	
	function initializeListeners() {
		listenOnChooseBusStopButtonClick();
		
		listenOnDataDepartureButtonClick();
		listenOnDataDepartureInputChanged();
		listenOnTimeDepartureButtonClick();
		listenOnTimeDepartureInputChanged();
	}
	
	function markClickedChooseBusStopButton($button) {
		$button.attr('data-clicked', 'yes');
	};
	
	function unmarkAllChooseBusStopButtons() {
		$('#trasaPage .buttons').removeAttr('data-clicked');
	};
	
	function getMarkedChooseBusStopButton() {
		return $('#trasaPage .buttons[data-clicked="yes"]').first();
	}
	
	function updateChooseStopButtonText(value) {
		var $button = getMarkedChooseBusStopButton();
		$button.find('.buttonText').text(value);
	}
	
	function listenOnChooseBusStopButtonClick() {
		$('#wybierzPrzystankiGrid .buttons').click(function() {
			var $button = $(this);
			
			//NavigationObject.control = $button;
			unmarkAllChooseBusStopButtons();
			markClickedChooseBusStopButton($button);
		});
	};
	
	function listenOnDataDepartureButtonClick() {		
		$('#dataOdjazduLink').on('click', function() {
			  $('#dataOdjazduInput').datebox('open');
		});
	};
	
	function listenOnDataDepartureInputChanged() {
		$('#dataOdjazduInput').bind('change', function() {
			$('#dataOdjazduText').text($(this).val());
			
		});
		
		$('#dataOdjazduInput').bind('datebox', function (e, passed) { 
			if ( passed.method === 'close' ) {
				$('#dataOdjazduText').text($(this).datebox('callFormat'));
			}
		});
	};
	
	function listenOnTimeDepartureButtonClick() {		
		$('#czasOdjazduLink').on('click', function() {
			  $('#czasOdjazduInput').datebox('open');
		});
	};

	function listenOnTimeDepartureInputChanged() {
		$('#czasOdjazduInput').bind('change', function() {
			$('#czasOdjazduText').text($(this).val());
		});
		
		$('#czasOdjazduInput').bind('datebox', function (e, passed) { 
			if ( passed.method === 'close' ) {
				$('#czasOdjazduText').text($(this).datebox('callFormat'));
			}
		});
	};
}

function LineViewManager(pageId) {
	
	this.AddListViewToPageContentAsync = addListViewToPageContentAsync;
	this.ListenOnListViewItemClick = listenOnListViewItemClick;
	
	function addListViewToPageContentAsync() {
		ShowLoader();
		
		var lineManager = new LineManager();
		lineManager.GetLinesData(onGetLinesDataCallback);
	}
	
	function onGetLinesDataCallback(data) {	
		var $content = $(".ui-content .list-wrapper", pageId);
		$content.empty();

		var $listView = $('<ul/>', {
		    "data-role": "listview",
		    "data-filter": "true",
		    "data-filter-placeholder": "Wyszukaj linię"
		});
		
		data.lines.sort(natcmp);
		
		addRecursivelyItemsToListView(0, $content, $listView, data.lines);
	}
	
	function addRecursivelyItemsToListView(i, $content, $listView, data) {	
		$listView.append('<li>' + data[i].name + '</li>');
		
		setTimeout(function () {
			if (++i < data.length) {				
				addRecursivelyItemsToListView(i, $content, $listView, data);
		    } else {
		    	$listView.appendTo($content).listview();
		    	HideLoader();
		    }
		}, 1);
	};
	
	function listenOnListViewItemClick() {
		$(document).off("click", pageId + " li");
		$(document).on("click", pageId + " li", function(event) {

			var selectedLine = $(this).text();
			
			NavigationObject.fromPage = NavigationLinks.liniePageId;
			NavigationObject.toPage = NavigationLinks.kierunkiPageFile;
			NavigationObject.params = '{"params": [{"key":"selectedLine","value":"' + selectedLine + '"}]}';
			
			Navigate();
		});
	};
	
	function strcmp(a, b) {
		  if (a.name < b.name)
		     return -1;
		  if (a.name > b.name)
		    return 1;
		  return 0;
	};
	
	function natcmp(a, b) {
	    var x = [], y = [];

	    a.name.replace(/(\d+)|(\D+)/g, function($0, $1, $2) { x.push([$1 || 0, $2]) })
	    b.name.replace(/(\d+)|(\D+)/g, function($0, $1, $2) { y.push([$1 || 0, $2]) })

	    while(x.length && y.length) {
	        var xx = x.shift();
	        var yy = y.shift();
	        var nn = (xx[0] - yy[0]) || strcmp(xx[1], yy[1]);
	        if(nn) return nn;
	    }

	    if(x.length) return -1;
	    if(y.length) return +1;

	    return 0;
	}
}

function LineDirectionsViewManager(pageId) {
	
	this.AddListViewToPageContentAsync = addListViewToPageContentAsync;
	this.ListenOnListViewItemClick = listenOnListViewItemClick;
	
	var line;
	
	function addListViewToPageContentAsync(lineName) {
		ShowLoader();
		
		line = lineName;
		
		var lineManager = new LineManager();
		lineManager.GetDirectionsForLine(lineName, onGetLinesDataCallback);
	}
	
	function onGetLinesDataCallback(data) {	
		var $content = $(".ui-content", pageId);
		$content.empty();

		var $listView = $('<ul/>', {
		    "data-role": "listview"
		});
		
		data.directions.sort();
		
		addRecursivelyItemsToListView(0, $content, $listView, data.directions);
	}
	
	function addRecursivelyItemsToListView(i, $content, $listView, data) {	
		$listView.append('<li>' + data[i] + '</li>');
		
		setTimeout(function () {
			if (++i < data.length) {				
				addRecursivelyItemsToListView(i, $content, $listView, data);
		    } else {
		    	$listView.appendTo($content).listview();
		    	HideLoader();
		    }
		}, 1);
	};
	
	function listenOnListViewItemClick() {
		$(document).off("click", pageId + " li");
		$(document).on("click", pageId + " li", function(event) {

			var selectedDirection = $(this).text();
			
			NavigationObject.fromPage = NavigationLinks.kierunkiPageId;
			NavigationObject.toPage = NavigationLinks.liniaKierunekPrzystankiPageFile;
			NavigationObject.params = '{"params": [{"key":"selectedLine","value":"' + line + '"}, {"key":"selectedDirection","value":"' + selectedDirection + '"}]}';
			
			Navigate();
		});
	};
}

function LineStopsForDirectionsViewManager(pageId) {
	
	this.AddListViewToPageContentAsync = addListViewToPageContentAsync;
	this.ListenOnListViewItemClick = listenOnListViewItemClick;
	this.UpdateHeader = updateHeader;
	
	var lastDirection;
	
	function addListViewToPageContentAsync(lineName, directionName) {
		ShowLoader();
		lastDirection = directionName;
		
		var lineManager = new LineManager();
		lineManager.GetLineStopsForDirection(lineName, directionName, onGetLinesDataCallback);
	}
	
	function onGetLinesDataCallback(data) {	
		var $content = $(".ui-content", pageId);
		$content.empty();

		var $listView = $('<ul/>', {
		    "data-role": "listview"
		});
		
		$listView.append('<li data-role="list-divider">Kierunek: ' + lastDirection + '</li>');
			
		addRecursivelyItemsToListView(0, $content, $listView, data.stops);
	}
	
	function addRecursivelyItemsToListView(i, $content, $listView, data) {	
		//$listView.append('<li>' + data[i] + '</li>');
		
		$li = $('<li><h3>' + data[i].name + '</h3><p>' + data[i].id + '</p></li>');
		$li.data('stopId', data[i].id);
		
		$listView.append($li);
		
		setTimeout(function () {
			if (++i < data.length) {				
				addRecursivelyItemsToListView(i, $content, $listView, data);
		    } else {
		    	$listView.appendTo($content).listview();
		    	HideLoader();
		    }
		}, 1);
	};
	
	function updateHeader(linia) {
		var $header = $(".ui-header .ui-title", pageId);
		
		$header.html("Trasa linii " + linia);
	}
	
	function listenOnListViewItemClick() {
		$(document).off("click", pageId + " li");
		$(document).on("click", pageId + " li", function(event) {

			var selectedBusStop = $(this).data('stopId');
			
			NavigationObject.fromPage = NavigationLinks.liniaKierunekPrzystankiPageId;
			NavigationObject.toPage = NavigationLinks.rozkladyAktualnePageFile;
			NavigationObject.params = '{"params": [{"key":"selectedBusStop","value":"' + selectedBusStop + '"}]}';
			
			Navigate();
		});
	};
}

function ActualSchedulesForStopViewManager(pageId) {
	
	this.AddListViewToPageContentAsync = addListViewToPageContentAsync;
	this.ListenOnListViewItemClick = listenOnListViewItemClick;
	this.UpdateHeader = updateHeader;
	
	var lastDirection;
	
	function addListViewToPageContentAsync(stopId, directionName) {
		ShowLoader();
		
		var stopManager = new BusStopManager();
		stopManager.GetActualSchedulesForStop(stopId, onGetSchedulesDataCallback);
	}
	
	function onGetSchedulesDataCallback(data) {	
		var $content = $(".ui-content", pageId);
		$content.empty();

		var $listView = $('<ul/>', {
		    "data-role": "listview"
		});
		
		if(jQuery.isEmptyObject(data)) {
			$listView.append('<li><h3>Brak aktualnie najbliższych odjazdów.</h3></li>');
			$listView.appendTo($content).listview();
			HideLoader();
		}
		
		addRecursivelyItemsToListView(0, $content, $listView, data);
	}
	
	function addRecursivelyItemsToListView(i, $content, $listView, data) {	
		
		$listView.append('<li><h3>' + data[i].line + ' -> ' + data[i].timeLeft + '</h3><p>Kierunek ' + data[i].direction + '</p></li>');
		
		setTimeout(function () {
			if (++i < data.length) {				
				addRecursivelyItemsToListView(i, $content, $listView, data);
		    } else {
		    	$listView.appendTo($content).listview();
		    	HideLoader();
		    }
		}, 1);
	};
	
	function updateHeader(stop) {
		var $header = $(".ui-header .ui-title", pageId);
		
		$header.html(stop);
	}
	
	function listenOnListViewItemClick() {
		
	};
}


function BusStopManager() {
	
	this.GetBusStopsData = getBusStopsData;
	this.GetActualSchedulesForStop = getActualSchedulesForStop;

    function getBusStopsData(callback) {  	
    	var dataFromStorage = _cacheManager.GetData(_cacheIdentifier);
		
		if(dataFromStorage == null) {
			//console.log('load from web');
			downloadBusStopsAsync(callback);
		}
		else {
			//console.log('load from storage');
			callback(dataFromStorage);
		}
    }
	
	function downloadBusStopsAsync(callback) {		
		var url = ManagerLinks.serviceUrl + ManagerLinks.getStops;

		$.ajax({		
			success : function (data) {				 
				_cacheManager.StoreData(_cacheIdentifier, data);
				callback(data);
			},
			error : function (a, b, c) {
				alert(a + ' ' + b + ' ' + c);
				console.log('cannot download bus stops');
			},
			dataType : 'json',
			url : url,
			type : 'GET'
		});
	};
	
	function getActualSchedulesForStop(stopId, callback) {
		
		var dataFromStorage = _cacheManager.GetData("schedulesfor" + stopId);
		
		if(dataFromStorage == null) {
			//console.log('load from web');
			getActualSchedulesForStopAsync(stopId, callback);
		}
		else {
			//console.log('load from storage');
			callback(dataFromStorage);
		}
	};
	
	function getActualSchedulesForStopAsync(stopId, callback) {
		
		var prepareLink = ManagerLinks.getActualSchedulesForStop;
		prepareLink = FillLinkParameter(prepareLink, ':id', stopId);
		var url = ManagerLinks.serviceUrl + prepareLink;

		$.ajax({		
			success : function (data) {				 
				_cacheManager.StoreData("schedulesfor" + stopId, data);
				callback(data);
			},
			error : function (a, b, c) {
				alert(a + ' ' + b + ' ' + c);
				//alert('cannot download actual schedules');
				console.log('cannot download actual schedules');
			},
			dataType : 'json',
			url : url,
			type : 'GET'
		});
	};
	
	var _that = this;
	var _cacheManager = new CacheManager();
	var _cacheIdentifier = "busStops"
}

function LineManager() {
	
	this.GetLinesData = getLinesData;
	this.GetDirectionsForLine = getDirectionsForLine;
	this.GetLineStopsForDirection = getLineStopsForDirection;

    function getLinesData(callback) {  	
    	var dataFromStorage = _cacheManager.GetData(_cacheLines);
		
		if(dataFromStorage == null) {
			//console.log('load from web');
			downloadLinesAsync(callback);
		}
		else {
			//console.log('load from storage');
			callback(dataFromStorage);
		}
    };
	
	function downloadLinesAsync(callback) {		
		var url = ManagerLinks.serviceUrl + ManagerLinks.getLinesNames;

		$.ajax({		
			success : function (data) {				 
				_cacheManager.StoreData(_cacheLines, data);
				callback(data);
			},
			error : function (a, b, c) {
				//alert('cannot download lines');
				console.log('cannot download lines');
			},
			dataType : 'json',
			url : url,
			type : 'GET'
		});
	};
	
	function getDirectionsForLine(lineName, callback) {  	
    	var dataFromStorage = _cacheManager.GetData("directionsfor" + lineName);
		
		if(dataFromStorage == null) {
			//console.log('load from web');
			getDirectionsForLineAsync(lineName, callback);
		}
		else {
			//console.log('load from storage');
			callback(dataFromStorage);
		}
    };
	
	function getDirectionsForLineAsync(lineName, callback) {

		var prepareLink = ManagerLinks.getLineDirections;
		prepareLink = FillLinkParameter(prepareLink, ':name', lineName);		
		var url = ManagerLinks.serviceUrl + prepareLink;

		$.ajax({		
			success : function (data) {				 
				_cacheManager.StoreData("directionsfor" + lineName, data);
				callback(data);
			},
			error : function (a, b, c) {
				alert(a + ' ' + b + ' ' + c);
				//alert('cannot download directions');
				console.log('cannot download directions');
			},
			dataType : 'json',
			url : url,
			type : 'GET'
		});
	};
	
	function getLineStopsForDirection(lineName, directionName, callback) {  	
    	var dataFromStorage = _cacheManager.GetData("stopsfor" + lineName + directionName);
		
		if(dataFromStorage == null) {
			//console.log('load from web');
			getLineStopsForDirectionAsync(lineName, directionName, callback);
		}
		else {
			//console.log('load from storage');
			callback(dataFromStorage);
		}
    };
	
	function getLineStopsForDirectionAsync(lineName, directionName, callback) {

		var prepareLink = ManagerLinks.getLineStopsForDirection;
		prepareLink = FillLinkParameter(prepareLink, ':name', lineName);
		prepareLink = FillLinkParameter(prepareLink, ':direction', directionName);
		var url = ManagerLinks.serviceUrl + prepareLink;

		$.ajax({		
			success : function (data) {				 
				_cacheManager.StoreData("stopsfor" + lineName + directionName, data);
				callback(data);
			},
			error : function (a, b, c) {
				alert(a + ' ' + b + ' ' + c);
				//alert('cannot download directions');
				console.log('cannot download directions');
			},
			dataType : 'json',
			url : url,
			type : 'GET'
		});
	};
	
	var _that = this;
	var _cacheManager = new CacheManager();
	var _cacheLines = "lines"
}

function CacheManager() {
	this.StoreData = storeData;
	this.GetData = tryGetData;
	this.Clear = clearStorage;
	this.RemoveData = removeData;
	
	function tryGetData(key)
	{
		var stringValue = window.localStorage[key];
		
		if(stringValue == null) {
			return null;
		}
		
		var jsonValue = JSON.parse(stringValue);	
		return jsonValue;	
	}
	 
	function storeData(key, value)
	{
		var stringValue = JSON.stringify(value);
		
		window.localStorage[key] = stringValue;
	}
	
	function clearStorage()
	{
		window.localStorage[key] = value;
	}
	
	function removeData(key)
	{
		window.localStorage[key] = value;
	}
}

/******************/

/**** TRASA SCRIPTS ****/

$(document).on('pageinit', '#trasaPage', function(){       	
	var tripViewManager = new TripViewManager("#trasaPage");
	
	tripViewManager.InitializeListeners();
	
	
});

$(document).on('pagebeforeshow', '#trasaPage', function(){    
	
	if(NavigationObject.toPage == NavigationLinks.trasaPageFile &&
	   NavigationObject.fromPage == NavigationLinks.wybierzPrzystanekPageFile) 
	{			
		var json = JSON.parse(NavigationObject.params);
		
		var key = json.params[0].key;
		var value = json.params[0].value;
		
		if(key && value) {
			var tripViewManager = new TripViewManager("#trasaPage");
			tripViewManager.UpdateChooseStopButtonText(value);
		}
	}
	else {
		
	}
	
	NavigationObject.toPage = "";
	NavigationObject.fromPage = "";
	NavigationObject.params = "";
});

$(document).on('pagebeforehide', '#trasaPage', function(event, data){
	
	var nextPageId = "#" + data.nextPage.attr('id');
	var link1 = NavigationLinks.ustawOpcjeDialogPageId;
	var link2 = NavigationLinks.wybierzPrzystanekPageId;
	
	if(	nextPageId 	==  link1 ||
		nextPageId 	== 	link2 ){
		return;
	}
	
	$('.buttonText', "#wybierzPrzystankiGrid").first().text('Wybierz źródło');
	$('.buttonText', "#wybierzPrzystankiGrid").last().text('Wybierz cel');
	$('#dataOdjazduText').text("");
	$('#czasOdjazduText').text("");
	
});

/***********************/

/**** WYBIERZ PRZYSTANEK SCRIPTS ****/

$(document).on('pageinit', '#wybierzPrzystanekDialogPage', function(){       	
	var chooseBusStopViewManager = new ChooseBusStopViewManager("#wybierzPrzystanekDialogPage");
	
	chooseBusStopViewManager.AddListViewToPageContentAsync();
    
    chooseBusStopViewManager.ListenOnListViewItemClick();
	
	
});

$(document).on('pagebeforeshow', '#wybierzPrzystanekDialogPage', function() {     
	
});

/************************************/

/**** PRZYSTANKI SCRIPTS ****/

$(document).on('pageinit', '#przystankiPage', function(){  
	
	var chooseBusStopViewManager = new ChooseBusStopViewManager("#przystankiPage");
	
	chooseBusStopViewManager.AddListViewToPageContentAsync();
	
	chooseBusStopViewManager.ListenOnListViewItemClick2();

});

$(document).on('pagebeforeshow', '#przystankiPage', function() {     
	
});

/***********************/

/**** LINIE SCRIPTS ****/

$(document).on('pageinit', '#liniePage', function(){  
	
	var lineViewManager = new LineViewManager("#liniePage");
	
	lineViewManager.AddListViewToPageContentAsync();
	
	lineViewManager.ListenOnListViewItemClick();

});

$(document).on('pagebeforeshow', '#liniePage', function() {     
	
});

/***********************/

/**** KIERUNKI SCRIPTS ****/

$(document).on('pageinit', '#kierunkiPage', function(){  
	
});

$(document).on('pagebeforeshow', '#kierunkiPage', function() { 
	
	if(NavigationObject.toPage == NavigationLinks.kierunkiPageFile &&
	   NavigationObject.fromPage == NavigationLinks.liniePageId) 
	{			
		var json = JSON.parse(NavigationObject.params);

		var key = json.params[0].key;
		var value = json.params[0].value;

		if(key && value) {
			var lineViewManager = new LineDirectionsViewManager("#kierunkiPage");
			
			lineViewManager.AddListViewToPageContentAsync(value);
			lineViewManager.ListenOnListViewItemClick();
		}
	}

	NavigationObject.toPage = "";
	NavigationObject.fromPage = "";
	NavigationObject.params = "";
});

/***********************/

/**** LINIA KIERUNEK PRZYSTANKI SCRIPTS ****/

$(document).on('pageinit', '#liniaKierunekPrzystankiPage', function(){  
	
	
	
});

$(document).on('pagebeforeshow', '#liniaKierunekPrzystankiPage', function() { 
	
	if(NavigationObject.toPage == NavigationLinks.liniaKierunekPrzystankiPageFile &&
	   NavigationObject.fromPage == NavigationLinks.kierunkiPageId) 
	{			
		var json = JSON.parse(NavigationObject.params);

		var key = json.params[0].key;
		var linia = json.params[0].value;
		var kierunek = json.params[1].value;

		if(linia && kierunek) {
			var lineStopsForDirectionsViewManager = new LineStopsForDirectionsViewManager("#liniaKierunekPrzystankiPage");
			
			lineStopsForDirectionsViewManager.AddListViewToPageContentAsync(linia, kierunek);
			
			lineStopsForDirectionsViewManager.ListenOnListViewItemClick();
			
			lineStopsForDirectionsViewManager.UpdateHeader(linia);
			
			$("#idzDoMapyLink").click(function() {
				NavigationObject.fromPage = NavigationLinks.liniaKierunekPrzystankiPageId;
				NavigationObject.toPage = NavigationLinks.mapaPageFile;
				NavigationObject.params = '{"params": [{"key":"linia","value":"' + linia + '"}, {"key":"kierunek","value":"' + kierunek + '"}]}';
				
				Navigate();
			});
		}
	}

	NavigationObject.toPage = "";
	NavigationObject.fromPage = "";
	NavigationObject.params = "";
});

/***********************/

/**** ROZKLADY AKTUALNE SCRIPTS ****/

$(document).on('pageinit', '#rozkladyAktualnePage', function(){  
	
});

$(document).on('pagebeforeshow', '#rozkladyAktualnePage', function() { 
	
	if((NavigationObject.toPage == NavigationLinks.rozkladyAktualnePageFile &&
	    NavigationObject.fromPage == NavigationLinks.przystankiPageId) ||
	   (NavigationObject.toPage == NavigationLinks.rozkladyAktualnePageFile &&
		NavigationObject.fromPage == NavigationLinks.liniaKierunekPrzystankiPageId)) 
	{			
		var json = JSON.parse(NavigationObject.params);

		var key = json.params[0].key;
		var value = json.params[0].value;

		if(key && value) {
			var actualSchedulesForStopViewManager = new ActualSchedulesForStopViewManager("#rozkladyAktualnePage");
			
			actualSchedulesForStopViewManager.AddListViewToPageContentAsync(value);
		}
	}

	NavigationObject.toPage = "";
	NavigationObject.fromPage = "";
	NavigationObject.params = "";
});

/***********************/