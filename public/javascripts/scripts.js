//mapa na stronie
var mapa
//interwał do aktualizacji pozycji pojazdów na mapie
var interval
//warstwa z przystankami i trasą linii
var ctaLayer
var akademik = new google.maps.LatLng(54.369059,18.607544);
var iamherePosition = new google.maps.LatLng(54.356556,18.647919);
var markerIamHere;

var stops = [];
var lines = [];
var linesWithoutDirections = [];

var lastAjaxCallForPath = null;

function mapaStart()
{
    var wspolrzedne = new google.maps.LatLng(54.356556,18.647919);
    var opcjeMapy = {
      zoom: 13,
      center: wspolrzedne,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: false,
      scaleControl: false,
      zoomControl: false,
	  panControl: false,
      mapTypeControl: false,
      overviewMapControl: false
    };
    mapa = new google.maps.Map(document.getElementById("mapka"), opcjeMapy);

    markerIamHere = new google.maps.Marker({
        map:mapa,
        draggable:true,
        animation: google.maps.Animation.DROP,
        position: wspolrzedne
    });

    google.maps.event.addListener(markerIamHere, 'dragend', dragedMarker);
}
function dragedMarker()
{
    mapa.panTo(markerIamHere.getPosition());
    iamherePosition = markerIamHere.getPosition();
}


function stopLastCall() {
	if(null != lastAjaxCallForPath) {
		lastAjaxCallForPath.abort();
	}
}

function monitor() {

    if(typeof interval !== 'undefined') {
        clearInterval(interval);
    }
    clearRouteToStop();
    clearMap()
    removeStopsMarkers()
    deleteOverlays();
    var nazwaLinii = $('#monitorowanaLinia').val();
    var url = '/trasaLinii/' + nazwaLinii;
    stopLastCall();
    lastAjaxCallForPath = $.ajax({
        success : function (data) {
            setPath(data.path, data.avglat, data.avglong);
            setStopsMarkers(data.stops, data.lineid);
            monitoredLine = nazwaLinii;
            getVehiclesOnLine();
            interval = setInterval(function() {getVehiclesOnLine();}, 30000);
        },
        error: function (xhr, ajaxOptions, thrownError) {
        	deleteOverlays();
        },
        dataType: 'json',
        url: url,
        type: 'GET'
    });
}


$(document).ready(function () {
    //initialize data for autocomplete widget
    var stopSource = "/getStopNames/";
    $.ajax({
        success : function (data, textStatus) {
            data.push.apply(stops, data);
        },
        dataType: 'json',
        url: stopSource,
        type: 'GET'
    });

    /*var lineSource = "/getLinesWithDirections/";
    $.ajax({
        success : function (data, textStatus) {
            lines.push.apply(lines, data);
        },
        dataType: 'json',
        url: lineSource,
        type: 'GET'
    });*/

    var url = "/getLineNames/"
    $.ajax({
        success : function (data, textStatus) {
        	var node = $("#monitorowanaLinia");
        	data.sort();
        	node.append("<option value=\"\"></option>")
        	for(var i = 0; i < data.length; i++) {
        		node.append("<option value=\"" + data[i] + "\">" + data[i] + "</option>")
        	}
        	$(".chosen-select").chosen({no_results_text: "Nie ma takiej linii!"});
        	$(".chosen-single").css("width", "280px");
        	$(".chosen-drop").css("width", "280px");

            $('.chosen-select').on('change', function(evt, params) {
                monitor();
            });
        	
        	$(window).keypress(function(event){
            	if(13 == event.keyCode) {
            		if($(".no-results").length > 0) {

            		} else {
            			var txt = $(".highlighted").text();
            			if(txt.length > 0) {
	            			$('#monitorowanaLinia').val(txt);
	            			monitor();
            			}
            		}
            		event.preventDefault();
            	}
            	
            });
      	
        },
        dataType: 'json',
        url: url,
        type: 'GET'
    });
});

$(function() {

$( "#from" ).autocomplete({
    delay: 200,
    minLength: 2,
    maxItemsToShow:10,
    source: stops,
    select: function(event, ui) {
        //showArrivingVehicles(ui.item.value)
    }
});

$( "#to" ).autocomplete({
    delay: 200,
    minLength: 2,
    maxItemsToShow:10,
    source: stops
});


    $('#trambox').click(function() {
        var stopA = $('#from').val();
        var stopB = $('#to').val();
        var data = $('#datepicker').val();
        var time = $('#timepicker').val();
        if(stopA.length == 0) {
            alert("Uzupełnij przystanek początkowy!")
            return 0;
        }
        if(stopB.length == 0) {
            alert("Uzupełnij przystanek końcowy!")
            return 0;
        }
        if(time.length == 0) {
            alert("Uzupełnij przystanek czas!")
            return 0;
        }
        drawDirectionOnMapFromAdvanced(stopA, stopB, data, time)
    });
$('#search').click(function() {
    var stopA = $('#from').val();
    var stopB = $('#to').val();
    var data = $('#datepicker').val();
    var time = $('#timepicker').val();
    if(stopA.length == 0) {
        alert("Uzupełnij przystanek początkowy!")
        return 0;
    }
    if(stopB.length == 0) {
        alert("Uzupełnij przystanek końcowy!")
        return 0;
    }
    if(time.length == 0) {
        alert("Uzupełnij przystanek czas!")
        return 0;
    }

    getPropositions(stopA, stopB, time, data)
});

$('#linia').keyup(function(event) {
    if($('#linia').val().length == 0) {
        $('#rozklad-linia-result').html("");
    }
});

$('#pokazMonitorowanaLinie').click(function() {
	monitor();
});


$(function() {
$( document ).tooltip();
    $('.blink').blink();
});



$( "input[type=submit]" ).button().click(function( event ) { event.preventDefault(); });

});
