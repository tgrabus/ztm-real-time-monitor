
function RestEngine(url, handler)
{
    this.Url = url;

    this.Call = Call;

    this.SetDataType = SetDataType;
    this.AddArgument = AddArgument;
    this.RemoveArgument = RemoveArgument;

    this.DataType = 'jsonp';
    this.Arguments = new Array();

    function Call()
    {
        var url2 = this.Url;

        if (!this.Arguments.isEmpty())
        {
            url2 = url2 + '?';

            for (var key in this.Arguments)
            {
                if (this.Arguments.hasOwnProperty(key))
                {
                    url2 = url2 + key + '=' + this.Arguments[key] + '&';
                }
            }

            url2 = url2.substring(0, url2.length - 1);

        }

        $.ajax({
            url: url2,
            type: 'GET',
            dataType: this.DataType,
            success: function (data, textStatus,jqXhr) { handler(data, textStatus, jqXhr) },
            error: function (jqXhr, textStatus, errorThrown) { ErrorHandler(jqXhr, textStatus, errorThrown) },
            crossDomain: true
        });

    }

    function ErrorHandler(jqXhr, textStatus, errorThrown)
    {
         alert('Error occured during call: ' + this.Url + 'Error message: ' + errorThrown);
    }

    function SetDataType(dataType)
    {
        this.DataType = dataType;
    }

    function AddArgument(key, value)
    {
        this.Arguments[key] = value;
    }

    function RemoveArgument(key)
    {
        this.Arguments[key] = undefined;
    }

    Object.prototype.isEmpty = function ()
    {
        for (var prop in this) if (this.hasOwnProperty(prop)) return false;
        return true;
    };
}


