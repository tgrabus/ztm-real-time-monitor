var markers = []
var monitoredLine
var stopMarkers
var polyLine
var path

function getVehiclesOnLine() {
    var url = "/pojazdyNaLinii/" + monitoredLine
    $.ajax({
        url: url,
        success: function (result) { updateMap(result) },
        dataType: 'json',
        crossDomain: false
    });
}

function updateMap(data) {
    removeOldMarkups()
    addNewMarkups(data)
}

function removeOldMarkups() {
    for(index in markers) {
        markers[index].setMap(null)
    }
}

function removeStopsMarkers() {
    for(index in stopMarkers) {
    	stopMarkers[index].setMap(null)
    }
}

function addNewMarkups(data) {
    markers = []
    for(var i = 0; i < data.length; i++) {
        var pos = new google.maps.LatLng(data[i].pozycja.dlugosc, data[i].pozycja.szerokosc)
        var icon = { url : selectMarkerIcon(data[i].pojazd), scaledSize: new google.maps.Size(24, 24) }
        var label = "Pojazd " + data[i].id;
        var marker = new google.maps.Marker({
            position: pos,
            title: label,
            icon: icon});
        marker.setMap(mapa)
        markers.push(marker)
    }
}

function selectMarkerIcon(vehicleType) {
    if(vehicleType == "Tram") {
        return "http://153.19.52.107/assets/images/tram_map.png";
    }  else {
        return "http://153.19.52.107/assets/images/bus_map.png";
    }
}

function deleteOverlays() {
  if(typeof polyLine !== "undefined") {
    polyLine.setMap(null);
  }
  removeOldMarkups();
  markers = [];
}


function getAllVehicles() {
    var url = "/wszystkiePojazdy"
    $.ajax({
        url: url,
        success: function (result) { updateMap(result) },
        dataType: 'json',
        crossDomain: false
    });
}

function setPath(points, cent_lat, cent_long) {
	path = [];
	for(var i = 0; i < points.length; i++) {
        var pos = new google.maps.LatLng(points[i].lat, points[i].long)
        path.push(pos);
    }
    var panning = new google.maps.LatLng(cent_lat,cent_long);
	polyLine = new google.maps.Polyline({
		path: path,
		geodesic: true,
		strokeColor: '#1161a3',
	    strokeOpacity: 1.0,
	    strokeWeight: 5
	});
	polyLine.setMap(mapa)
    mapa.panTo(panning);
}

function setStopsMarkers(stops, lineid) {
	stopMarkers = []
    var markers = new Array();
    var lineId = lineid;
	for(var i = 0; i < stops.length; i++) {
        var pos = new google.maps.LatLng(stops[i].position.lat, stops[i].position.long);
        var icon;
        if(0 == i) {
        	icon = {
		    url: "http://153.19.52.107/assets/images/flag2_map.png",
		    scaledSize: new google.maps.Size(32, 32)
		  };
        } else if(stops.length - 1 == i) {
        	icon = {
    		    url: "http://153.19.52.107/assets/images/flag3_map.png",
    		    scaledSize: new google.maps.Size(32, 32)
    		  };
        } else {
        	icon = {
			    url: "http://153.19.52.107/assets/images/flag_map.png",
			    scaledSize: new google.maps.Size(32, 32)
			  };
        }
        var label = stops[i].name
        markers[i] = new google.maps.Marker({
            position: pos,
            title: label,
            icon: icon});

        google.maps.event.addListener(markers[i], 'click', showStopDialog(markers[i].title, stops[i].id, lineId));
        markers[i].setMap(mapa);
        stopMarkers.push(markers[i]);
    }
}

function setDialogWithOnlineData(name, stopNumber, lineId)
{
    if(stopNumber.length == 0) return;

    var url = '/getActualDeparturesOnStop/' + stopNumber;
    $.ajax({
        success : function (data) {
            $('#table1').html("")
            $( "#tablica-nazwa" ).html("Przystanek <strong>" + name + "</strong>")

            var x = data.lines;
            var len = data.length;
            var i = 0;
            for(i =0; i<len; i++) {
                var idran = "xx" + Math.floor((Math.random()*10000)+1);
                var zaile;
                if(data[i].timeLeft == "teraz") {
                    zaile = "<strong><span style=\"color:red\" id=\"" + idran + "\">Za moment!</span></strong>";
                }
                else
                    zaile = data[i].timeLeft
                $('#table1').append("<tr><td>" + data[i].line + " -> </td><td>" + data[i].direction + "</td><td>" + zaile + "</td></tr>");
                $("#"+ idran ).blink();
            }
        },
        dataType: 'json',
        url: url,
        type: 'GET'
    });
    var url2 = '/getScheduleOnStop/' + lineId + '/' + stopNumber;
    $.ajax({
        success : function (data) {
            var stopid = data[0].stopid
            var rozklady = data[0].rozklady
            showRozklady(rozklady, 'rozklady-jazdy')
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + " " + xhr.status + " blad");
        },
        dataType: 'json',
        url: url2,
        type: 'GET'
    });
}
function showRozklady(rozklady, divid){
    var rozkladyl = rozklady.length;
    var i = 0;
    $('#rozklady-jazdy').html("")
    var html = "<div id=\"tabs\"><ul>";
	if(rozklady[0].odjazdy.length > 0) {
		i++;
		html+="<li><a href=\"#tabs-" + i + "\">Dni Robocze</a></li>";
	}
	if(rozklady[1].odjazdy.length > 0) {
		i++;
		html+="<li><a href=\"#tabs-" + i + "\">Soboty</a></li>";
	}
	if(rozklady[2].odjazdy.length > 0) {
		i++;
		html+="<li><a href=\"#tabs-" + i + "\">Niedziele i święta</a></li>";
	}
	
    html+="</ul>";
	var j = 0;
    for(i = 0; i < rozkladyl; i++){

        var godziny = rozklady[i].odjazdy;
        var odjazdow = godziny.length;
        var typrozkladu = "Nieznany";
		
        if(i == 0)
            typrozkladu = "Dni Robocze";
        if(i == 1)
            typrozkladu = "Soboty";
        if(i == 2)
            typrozkladu = "Niedziele i święta";
        if(odjazdow > 0) {
			j++;
            $('#rozklady-jazdy').append("<div><strong>Rozklad " + typrozkladu + "</strong>")
            html+="<div id=\"tabs-"+ j + "\">";
            var h = 0;
            var lasthour = -1;
            for(h=0; h<odjazdow; h++){

                if(lasthour<godziny[h].godzina){
                    $('#rozklady-jazdy').append("<strong>" + godziny[h].godzina + ": </strong>")
                    html+= "<strong>" + godziny[h].godzina + ": </strong>";
                }


                var odjazd =  godziny[h].minuta;
                var inf = godziny[h].info;
                var infstr = "";
                if(inf.length > 0)
                    infstr = "<i>" + inf + "</i>";
                if(h!= odjazdow - 1)
                    odjazd = odjazd + infstr +  ", ";



                var d = new Date();
                var n = d.getHours();
                if(n == godziny[h].godzina)
                    odjazd = "<strong>" + odjazd + "</strong>";

                $('#rozklady-jazdy').append(odjazd)
                html+=odjazd;

                if(h+ 1 < odjazdow){
                    if(godziny[h+1].godzina>godziny[h].godzina)  {
                            $('#rozklady-jazdy').append("</br>")
                            html+="</br>";
                        }
                }
                lasthour =  godziny[h].godzina
            }
            $('#rozklady-jazdy').append("</div>")
            html+="</div>";
        }

    }
    $('#rozklady-jazdy').html(html)
    $("#tabs").tabs();
}
function showStopDialog(name, number, lineId){
    return function(){
        $( "#stop-window" ).dialog("open")
        setDialogWithOnlineData(name, number, lineId)
    }
}
