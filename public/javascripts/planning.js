/**
 * Created with IntelliJ IDEA.
 * User: Mateusz
 * Date: 14.01.14
 * Time: 10:21
 * To change this template use File | Settings | File Templates.
 */


function getPropositions(from, to, time, data) {
    var url = "planRoutes/" + from +  "/" + to + "/" + time;
    $('#planning-results-window').dialog('open')
    $('#preloader').show(300)
    var html = ""
    $('#planning-results').html("Trwa wyszukiwanie połączeń...");
    $.ajax({
        success : function (data) {
                var propozycje = data.propozycje
                var ilosc =  propozycje.length
                if(ilosc > 0) {
                    var i = 0;
                    html += "<div id=\"accor-result\">"
                    for(i = 0; i < ilosc; i++){
                        html += "<h3>"
                        var nazwa_lini = propozycje[i].linia
                        var kierunek = propozycje[i].kierunek
                        var startt = propozycje[i].starttime
                        var endt = propozycje[i].endtime
                        var total = propozycje[i].totaltime

                        var idlinia = propozycje[i].idlinia
                        var idstart = propozycje[i].startId
                        var idend = propozycje[i].endId
                        var proposetime = propozycje[i].proposetime
                        //var idPole2 = "xxx" + Math.floor((Math.random()*10000)+1);
                        var idPole = "pp" + Math.floor((Math.random()*10000)+1);
                        var link = "<strong><a href=\"\" data-idp=\""+idPole+"\" class=\"showonmap\" data-lin=\""+ idlinia +"\" data-sta=\""+ idstart +"\" data-end=\""+ idend +"\" data-tim=\""+ proposetime +"\">Pokaż na mapce</a></strong>"
                        html += nazwa_lini
                        html += " "
                        html += kierunek
                        html += " ("
                        html += startt
                        html += " - "
                        html += endt
                        html += ") "
                        html += total
                        html += " min "
                        html += link
                        html += "</h3>"
                        html += "<div>"
                        var odjazdy = propozycje[i].entries
                        var odjazdy_count = odjazdy.length




                        html += "<p id=\""+idPole+"\"></p>"
                        var j = 0;
                        for(j = 0; j < odjazdy_count; j++){
                            html += "<p><strong>"
                            html += odjazdy[j].time
                            html += ":</strong> "
                            html += odjazdy[j].name
                            html += "</p>"
                        }
                        html += "</div>"



                    }
                    html += "</div>"
                    $('#preloader').hide(300)
                    $('#planning-results').html(html);
                    $('#accor-result').accordion({heightStyle : 'content'});
                    $(".showonmap").click(function(){
                        var li = $(this).attr('data-lin')
                        var st = $(this).attr('data-sta')
                        var en = $(this).attr('data-end')
                        var ti = $(this).attr('data-tim')
                        var ip = $(this).attr('data-idp')
                        drawDirectionOnMap(li,st,en,ti, ip)

                    });

                } else {
                    $('#preloader').hide(300)
                    $('#planning-results-window').dialog('close')
                    alert("Brak połączeń!")
                }


        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + " " + xhr.status + " błąd pobierania planowania!");
        },
        dataType: 'json',
        url: url,
        type: 'GET'
    });
}
var dir_markers;
var dir_path;
var dir_polyLine;

function clearMap() {
    for(index in dir_markers) {
        dir_markers[index].setMap(null)
    }
    if(typeof dir_polyLine !== "undefined") {
        dir_polyLine.setMap(null);
    }
    dir_markers = []
    dir_path = []
}

function drawDirectionOnMapFromAdvanced(startStop, endStop, date, time){
    var url = "test/"+startStop+"/" + endStop + "/"+ date + "-" + time + "/1/600"

    $.ajax({
        success : function (data) {
            clearMap();
            removeStopsMarkers()
            deleteOverlays();
            var x = data.entries;
            var a = data.alat;
            var b = data.along;
            var len = x.length;
            var i = 0;
            dir_markers = []
            dir_path = []
            var markers = new Array();

            var dest = new google.maps.LatLng(x[0].lat, x[0].long);
            //calcRoute(iamherePosition, dest, poleid);


            for(var i = 0; i < len; i++) {
                var pos = new google.maps.LatLng(x[i].lat, x[i].long);
                dir_path.push(pos);
                var icon;
                icon = {
                    url: "http://153.19.52.107/assets/images/flag2_map.png",
                    scaledSize: new google.maps.Size(32, 32)
                };
                var label = x[i].time + " " + x[i].name + " " + x[i].line + " " + x[i].linedir
                markers[i] = new google.maps.Marker({
                    position: pos,
                    title: label,
                    icon: icon});


                markers[i].setMap(mapa);
                dir_markers.push(markers[i]);
            }

            var panning = new google.maps.LatLng(a,b);
            dir_polyLine = new google.maps.Polyline({
                path: dir_path,
                geodesic: true,
                strokeColor: '#1161a3',
                strokeOpacity: 1.0,
                strokeWeight: 5
            });
            dir_polyLine.setMap(mapa)
            mapa.panTo(panning);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + " " + xhr.status + " błąd pobierania planowania!");
        },
        dataType: 'json',
        url: url,
        type: 'GET'
    });
}

function drawDirectionOnMap(lineid, startid, endid, time, poleid){
    var url = "planRoute/"+lineid+"/" + startid + "/"+endid + "/" + time

    $.ajax({
        success : function (data) {
            clearMap();
            removeStopsMarkers()
            deleteOverlays();
            var x = data.entries;
            var a = data.alat;
            var b = data.along;
            var len = x.length;
            var i = 0;
            dir_markers = []
            dir_path = []
            var markers = new Array();

            var dest = new google.maps.LatLng(x[0].lat, x[0].long);
            calcRoute(iamherePosition, dest, poleid);


            for(var i = 0; i < len; i++) {
                var pos = new google.maps.LatLng(x[i].lat, x[i].long);
                dir_path.push(pos);
                var icon;
                icon = {
                    url: "http://153.19.52.107/assets/images/flag2_map.png",
                    scaledSize: new google.maps.Size(32, 32)
                };
                var label = x[i].time + " " + x[i].name
                markers[i] = new google.maps.Marker({
                    position: pos,
                    title: label,
                    icon: icon});


                markers[i].setMap(mapa);
                dir_markers.push(markers[i]);
            }

            var panning = new google.maps.LatLng(a,b);
            dir_polyLine = new google.maps.Polyline({
                path: dir_path,
                geodesic: true,
                strokeColor: '#1161a3',
                strokeOpacity: 1.0,
                strokeWeight: 5
            });
            dir_polyLine.setMap(mapa)
            mapa.panTo(panning);
        },
        dataType: 'json',
        url: url,
        type: 'GET'
    });
}
var to_Stop_polylines;
function clearRouteToStop(){
    for(index in to_Stop_polylines) {
        to_Stop_polylines[index].setMap(null)
    }
    to_Stop_polylines = []
}
function calcRoute(origin, destination, divid) {
    var directionsService = new google.maps.DirectionsService();
    var request = {
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.WALKING
    };
    directionsService.route(request, function(response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            var route = response.routes[0];
            var seconds = 0;
            for (var i = 0; i < route.legs.length; i++) {
                var len =  route.legs[i].distance.text;
                seconds+=route.legs[i].duration.value;

                var leg_steps = route.legs[i].steps
                var steps_count =  leg_steps.length;
                clearRouteToStop();
                for (var j = 0; j < steps_count; j++) {
                    var x_polyLine = new google.maps.Polyline({
                        path: leg_steps[j].path,
                        geodesic: true,
                        strokeColor: '#006600',
                        strokeColor: '#006600',
                        strokeOpacity: 1.0,
                        strokeWeight: 4
                    });
                    to_Stop_polylines.push(x_polyLine)
                    x_polyLine.setMap(mapa)
                }
            }
            var minutes = Math.round(seconds/60,2)
            $('#'+divid).html(minutes + " minut spaceru do przystanku.");
        }
    });
}

