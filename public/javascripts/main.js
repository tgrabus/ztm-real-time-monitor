$(function() {
    $("#datepicker").datepicker({ dateFormat: "dd-mm-yy", firstDay: 1 });
    $('#timepicker').timepicker({ timeFormat: "H:i", step: 10 });



    $( "#ac" ).accordion({
        heightStyle: "content",
        autoHeight: false,
        clearStyle: true
    } );
    $( "#stop-window-accordion" ).accordion({heightStyle : 'content'} );
    $( "#stop-window" ).dialog({
        resizable: true,
        width : 500,
        autoOpen : false,
        buttons: {
            "Zamknij": function() {
                $( this ).dialog( "close" );
           }
        }
    });
    $( "#planning-results-window" ).dialog({
        resizable: true,
        width : 600,
        autoOpen : false,
        buttons: {
            "Zamknij": function() {
                $( this ).dialog( "close" );
            }
        }
    });

	var w = $(window).width() - $("#leftbar").width();
	$("#mapka").css('width', w);
});
