# --- First database schema

# --- !Ups

CREATE TABLE Departure ( 
	departureHour integer NOT NULL,
	departureMinute smallint NOT NULL,
	departureInfo varchar(100),
	dayType smallint NOT NULL,
	shedule integer NOT NULL
);

CREATE TABLE lines ( 
	id_line integer NOT NULL,
	name varchar(100) NOT NULL,
	direction varchar(100) NOT NULL,
	type smallint NOT NULL
);

CREATE TABLE lines_points ( 
	counter integer NOT NULL,
	isstop boolean NOT NULL,
	point integer NOT NULL,
	line integer NOT NULL
);

CREATE TABLE lines_stops ( 
	id_line_stop integer NOT NULL,
	counter integer NOT NULL,
	ondemand boolean,
	line integer NOT NULL,
	stop integer NOT NULL
);

CREATE TABLE points ( 
	id_point integer NOT NULL,
	latitude real,
	longtitude real
);

CREATE TABLE schedule ( 
	id_shedule integer NOT NULL,
	active_from timestamp NOT NULL,
	active_to timestamp NOT NULL
);

CREATE TABLE stops ( 
	id_line integer NOT NULL,
	name varchar(50) NOT NULL,
	point integer NOT NULL
);

CREATE TABLE transports ( 
	ztm_number varchar(20) NOT NULL,
	type number NOT NULL,
	manufacturer varchar(50),
	model varchar(50),
	seats integer,
	capacity integer
);

CREATE TABLE users ( 
	id_user number NOT NULL,
	login varchar(30) NOT NULL,
	passwd varchar(50) NOT NULL,
	settings varchar(100)
);


ALTER TABLE lines ADD CONSTRAINT PK_lines 
	PRIMARY KEY (id_line);


ALTER TABLE lines_stops ADD CONSTRAINT PK_lines_stops 
	PRIMARY KEY (id_line_stop);


ALTER TABLE points ADD CONSTRAINT PK_points 
	PRIMARY KEY (id_point);


ALTER TABLE schedule ADD CONSTRAINT PK_schedule 
	PRIMARY KEY (id_shedule);


ALTER TABLE stops ADD CONSTRAINT PK_stops 
	PRIMARY KEY (id_line);


ALTER TABLE transports ADD CONSTRAINT PK_transports 
	PRIMARY KEY (ztm_number);


ALTER TABLE users ADD CONSTRAINT PK_users 
	PRIMARY KEY (id_user);




ALTER TABLE Departure ADD CONSTRAINT FK_Departure_schedule 
	FOREIGN KEY (shedule) REFERENCES schedule (id_shedule);

ALTER TABLE lines_stops ADD CONSTRAINT FK_lines_stops_lines 
	FOREIGN KEY (line) REFERENCES lines (id_line);

ALTER TABLE lines_stops ADD CONSTRAINT FK_lines_stops_schedule 
	FOREIGN KEY (id_line_stop) REFERENCES schedule (id_shedule);

ALTER TABLE lines_stops ADD CONSTRAINT FK_lines_stops_stops 
	FOREIGN KEY (line) REFERENCES stops (id_line);

ALTER TABLE stops ADD CONSTRAINT FK_stops_points 
	FOREIGN KEY (point) REFERENCES points (id_point);

	
CREATE TABLE vehicle_positions (
	tstamp time not null,
	vehicle_id integer not null,
	line integer not null,
	lat real not null,
	long real not null
);

# --- !Downs

drop table vehicle_positions;
drop table users;
drop table transports;
drop table stops;
drop table schedule;
drop table points;
drop table lines_stops;
drop table lines_points;
drop table lines;
drop table Departure;